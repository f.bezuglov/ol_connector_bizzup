<?php

/**
 * Функции для подключения к Битрикс24 и вызова API
 *
 */

namespace application\lib;

use application\models\Authentications;
use application\models\Configs;
use application\models\Logs;
use Exception;

class Bitrix24
{
    const TIMEOUT_SOCKET = 15;
    const BATCH_COUNT    = 50; // количество строк для отправки на обработку в batch
    private $lastCall = 0;

    public $method, $arDataRest;
    private          $isRetrySend    = false;
    public           $next           = 0;
    public           $isError        = false;
    public           $sqlMessage, $numError, $batch;
    protected        $webHook, $accessToken, $member_id;
    private          $timeProcessing = 0;
    public           $timeBegin, $timeEnd;

    public function __construct($member_id)
    {
        if (empty($_SESSION['auth'][$member_id])) {
            exit('Критическая ошибка: отсутствуют аутентификация к апи битрикс');
        }
        $this->member_id = $member_id;
        $this->accessToken = $_SESSION['auth'][$member_id]['access_token'];
        $this->timeBegin = microtime(true);
    }

    private function callCurl()
    {
        ini_set('max_execution_time', 900);
        set_time_limit(2400);

        // Ожидание полсекунды, так как битрикс не любит много запросов
        $this->synchronize();

        ///////////////////////////////////////////////////////////////////////////////////////
        // * Начиная с REST-модуля 22.0.0 в облачной версии Битрикс24 ко всем ответам REST-запросов в массиве времени
        // * operating добавлен дополнительный ключ с дополнительной информацией о времени выполнения запроса.
        // * Ключ сообщает вам о времени выполнения запроса к методу внутри портала.
        // * Данные о времени выполнения запросов к методу суммируются. Потом проверяется.
        // * Если время запроса всех методов превышает 480 секунд за последние 10 минут, метод блокируется на 10 минут.

        // если прошло 10 минут, то сбрасываем счетчики времени
        if ((600 - (microtime(true) - $this->timeBegin)) <= 0) {
            $this->timeProcessing = 0;
            $this->timeBegin      = microtime(true);
        } // иначе если сумма времени всех запросов превышает 480 секунд то ждем
        elseif ($this->timeProcessing > 460) {
            usleep((600 - (microtime(true) - $this->timeBegin)) * 1000000);
            $this->timeProcessing = 0;
            $this->timeBegin      = microtime(true);

        }

        $this->isError = false;
        $this->sqlMessage = null;

        if (!empty($this->webHook)) {
            $url        = $this->webHook . $this->method . '.json';
            $postFields = $this->arDataRest;
        } else {
            $clientEndpoint     = 'https://' . $_SESSION['auth'][$this->member_id]['domain'] . '/rest/';
            $url                = $clientEndpoint . $this->method . '.json';
            $postFields         = $this->arDataRest;
            $postFields['auth'] = $this->accessToken;
        }


        try {
            $obCurl = curl_init();
            curl_setopt($obCurl, CURLOPT_URL, $url);
            curl_setopt($obCurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($obCurl, CURLOPT_POSTREDIR, 10);
            curl_setopt($obCurl, CURLOPT_USERAGENT, 'confirm-visit');
            if ($postFields) {
                curl_setopt($obCurl, CURLOPT_POST, true);
                curl_setopt($obCurl, CURLOPT_POSTFIELDS, http_build_query($postFields));
            }
            curl_setopt($obCurl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($obCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($obCurl, CURLOPT_TIMEOUT, self::TIMEOUT_SOCKET * 60);
            curl_setopt($obCurl, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_SOCKET);
            curl_setopt($obCurl, CURLE_OPERATION_TIMEOUTED, self::TIMEOUT_SOCKET * 60);
            curl_setopt($obCurl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

            $resultRaw = curl_exec($obCurl);

            if (curl_errno($obCurl)) {
                if (!$this->isRetrySend) {
                    sleep(1);
                    $this->isRetrySend = true;
                    return $this->callCurl();
                }
                $this->isError = true;
                $this->sqlMessage = 'curl curl_error. Ответ с битрикса:  ' . curl_error($obCurl);
                $info          = curl_getinfo($obCurl);
                return false;
            }

            $result = json_decode($resultRaw ?? '', true);

            /*var_dump($result);
            exit;*/

            if (json_last_error() !== JSON_ERROR_NONE) {
                if (!$this->isRetrySend) {
                    sleep(1);
                    $this->isRetrySend = true;
                    return $this->callCurl();
                }
                $this->isError = true;
                $this->sqlMessage = 'curl json_error. Ответ с битрикса: ' . $resultRaw;
                return false;
            }

            curl_close($obCurl);

            if (!empty($result['error']) && $result['error'] == 'expired_token') {
                if (!$this->getNewAuth()) {
                    return false;
                };
                return $this->callCurl();
            }

            if (!empty($result['error']) || !isset($result['result'])) {
                $this->isError = true;
                $sqlMessage       = !empty($result['error_description']) ? $result['error_description'] : $result['error'];
                $sqlMessage       = !empty($sqlMessage) ? $sqlMessage : 'Неизвестная ошибка';
                $this->sqlMessage = 'curl error. Ответ с битрикса: ' . $sqlMessage;
                return false;
            }
            $this->isRetrySend    = false;
            $this->timeProcessing += (float)$result['time']['processing'];

            return $result;

        } catch (\Throwable $e) {
            $this->isError  = true;
            $this->sqlMessage  = 'curl throwable: ' . $e->getMessage();
            $this->numError = $e->getCode();
            return false;
        }
    }

    public function getNewAuth()
    {
        try {
            if (empty($this->accessToken)) {
                throw new Exception('Пустая авторизация. Переустановите приложение.');
            }

            $client_id = $client_secret = '';
            if (!empty($_SESSION['auth'][$this->member_id]['client_id']) && !empty($_SESSION['auth'][$this->member_id]['client_secret'])) {
                $client_id     = $_SESSION['auth'][$this->member_id]['client_id'];
                $client_secret = $_SESSION['auth'][$this->member_id]['client_secret'];
            } else {
                if (file_exists(__DIR__ . '/../config/bitrix24.php')) {
                    $config = require __DIR__ . '/../config/bitrix24.php';
                    if (empty($config['client_id']) || empty($config['client_secret'])) {
                        throw new Exception('Проверьте константы битрикс24 для продления ключей авторизации в файле ' . __DIR__ . '/../config/bitrix24.php');
                    }
                    $client_id     = $config['client_id'];
                    $client_secret = $config['client_secret'];
                } else {
                    throw new Exception('Отсутствует файл c константами битрикс24 для продления ключей авторизации');
                }
            }

            if (empty($_SESSION['auth'][$this->member_id]['refresh_token'])) {
                throw new Exception('Отсутствует refresh_token в сессии.');
            }
            $params = [
                'client_id'     => $client_id,
                'grant_type'    => 'refresh_token',
                'client_secret' => $client_secret,
                'refresh_token' => $_SESSION['auth'][$this->member_id]['refresh_token'],
            ];

            $target_url = 'https://oauth.bitrix.info/oauth/token/';
            $post       = http_build_query($params);

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $target_url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

            $result = curl_exec($curl);

            if ($result === false) {
                $this->isError    = true;
                $this->sqlMessage = 'Ошибка продления ключей авторизации битрикс24: ' . curl_error($curl);
                Logs::insertRowLogs('error', get_class($this), $this->sqlMessage);
                return false;
            }

            curl_close($curl);
            $result = json_decode($result, true);

            if (!empty($result['error'])) {
                throw new Exception($result['error'] . (!empty($result['error_description']) ? (': ' . $result['error_description']) : ''));
            }
            if (empty($result['access_token']) || empty($result['refresh_token']) || empty($result['expires_in'])) {
                throw new Exception('Ошибка при обновлении токена');
            }

            $_SESSION['auth'][$this->member_id]['access_token'] = $this->accessToken = $result['access_token'];
            $_SESSION['auth'][$this->member_id]['refresh_token'] = $result['refresh_token'];
            if (!$this->saveAuthentications($result['access_token'], $result['refresh_token'], $result['expires_in'])) {
                return false;
            };

            return true;

        } catch (Exception $ex) {
            $this->isError = true;
            $this->sqlMessage = $ex->getMessage();
            return false;
        }

    }

    public function saveAuthentications($accessToken, $refreshToken, $expiresIn)
    {
        $authenticationsModel                = new Authentications();
        $authenticationsModel->member_id     = $this->member_id;
        $authenticationsModel->access_token  = $accessToken;
        $authenticationsModel->refresh_token = $refreshToken;
        $authenticationsModel->expires_in    = $expiresIn;
        if (!$authenticationsModel->updateRowTokens()) {
            $this->isError = true;
            $this->sqlMessage = $authenticationsModel->sqlMessage;
            return false;
        }
        return true;
    }

    /*
     * Ожидание полсекунды, так как битрикс не любит много запросов
     *
     */
    public function synchronize()
    {
        if (isset($_SESSION['auth'][$this->member_id]['bitrix_synchronize'])) {
            $prev_run = $_SESSION['auth'][$this->member_id]['bitrix_synchronize'];
            $curr_run = microtime(true);
            if (($curr_run - $prev_run) < 0.5) {
                $sleep = 0.5 - ($curr_run - $prev_run);
                usleep($sleep * 1000000);
            }
        }
        $_SESSION['auth'][$this->member_id]['bitrix_synchronize'] = microtime(true);
    }

    public function call($method, $arDataRest = null)
    {
        $this->method     = $method;
        $this->arDataRest = $arDataRest;
        $result           = $this->callCurl();
        if ($this->isError) {
            Logs::insertRowLogs('error', get_class($this), $this->sqlMessage, debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            $result['result'] = [];
        }
        $this->next = $result['next'] ?? 0;
        return $result;
    }

    public function callBatch($arData, $halt = 0)
    {
        $arResult = [];
        if (is_array($arData)) {
            $arDataRest = [];
            $i          = 0;
            foreach ($arData as $key => $data) {

                if (!empty($data['method'])) {
                    $i++;
                    if (static::BATCH_COUNT >= $i) {
                        $arDataRest['cmd'][$key] = $data['method'];
                        if (!empty($data['params'])) {
                            $arDataRest['cmd'][$key] .= '?' . http_build_query($data['params']);
                        }
                    }
                }
            }
            if (!empty($arDataRest)) {
                $arDataRest['halt'] = $halt;
                $this->method       = 'batch';
                $this->arDataRest   = $arDataRest;


                $arResult = $this->callCurl();
            }
        }
        if (isset($arResult['result']['result_error']) && is_array($arResult['result']['result_error']) && count($arResult['result']['result_error']) > 0) {
            foreach ($arResult['result']['result_error'] as $key => $error) {
                $this->sqlMessage = (!empty($this->sqlMessage) ? ($this->sqlMessage . ', ') : '') . $key . ': ' . $error['error_description'];
            }
            $this->isError = true;
            Logs::insertRowLogs('error', get_class($this), 'Ответ с битрикса: ' . $this->sqlMessage, debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
        }
        if (!isset($arResult['result']['result']) || !is_array($arResult['result']['result'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($this->sqlMessage) ? $this->sqlMessage : 'Пустой ответ с битрикса. Выход из обработки.';
            Logs::insertRowLogs('error', get_class($this), $this->sqlMessage, debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            $arResult['result']['result'] = [];
        }

        return $arResult;
    }

    public function install()
    {
        $batch[0]['method'] = 'imbot.register';
        $batch[0]['params'] = [
            'CODE'             => 'BOT_CHATFILTER',
            'TYPE'             => 'O',
            'EVENT_HANDLER'    => __APP_HANDLER__,
            'EVENT_BOT_DELETE' => __APP_HANDLER__,
            'PROPERTIES'       => [ // Личные данные чат-бота (обяз.)
                                    'NAME'              => 'Бот Павел', // Имя чат-бота (обязательное одно из полей NAME или LAST_NAME)
                                    'LAST_NAME'         => '', // Фамилия чат-бота (обязательное одно из полей NAME или LAST_NAME)
                                    'COLOR'             => 'GREEN', // Цвет чат-бота для мобильного приложения RED, GREEN, MINT, LIGHT_BLUE, DARK_BLUE, PURPLE, AQUA, PINK, LIME, BROWN,  AZURE, KHAKI, SAND, MARENGO, GRAY, GRAPHITE
                                    'EMAIL'             => 'support@bizzup.ru', // E-mail для связи. НЕЛЬЗЯ использовать e-mail, дублирующий e-mail реальных пользователей
                                    'PERSONAL_BIRTHDAY' => '2021-12-21', // День рождения в формате YYYY-mm-dd
                                    'WORK_POSITION'     => 'Чат-бот', // Занимаемая должность, используется как описание чат-бота
                                    'PERSONAL_WWW'      => 'https://bizzup.ru', // Ссылка на сайт
                                    'PERSONAL_GENDER'   => 'M', // Пол чат-бота, допустимые значения M -  мужской, F - женский, пусто, если не требуется указывать
                                    'PERSONAL_PHOTO'    => base64_encode(__URL_PREFIX__ . '/images/bot-img.jpg') // Аватар чат-бота - base64
            ]
        ];

        $result = $this->callBatch($batch);

        if (!empty($result['result']['result_error'][0]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][0]['error_description']) ? $result['result']['result_error'][0]['error_description'] : $result['result']['result_error'][0]['error'];
            return false;
        }

        if (empty($result['result']['result'][0]) || !is_numeric($result['result']['result'][0])) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный ответ с битрикса';
            return false;
        }

        return $result['result']['result'][0];
    }

    /**
     * @param Configs $config
     * @return array|bool|mixed
     */
    public function uninstall($config)
    {
        $result = [];

        if (!empty($config->tmpl_contact_id) && $config->tmpl_contact_id > 0) {
            $batch[0]['method'] = 'bizproc.workflow.template.delete';
            $batch[0]['params'] = [
                'ID' => $config->tmpl_contact_id
            ];
        }
        if (!empty($config->tmpl_lead_id) && $config->tmpl_lead_id > 0) {
            $batch[1]['method'] = 'bizproc.workflow.template.delete';
            $batch[1]['params'] = [
                'ID' => $config->tmpl_lead_id
            ];
        }
        if (empty($batch)) {
            return $result;
        }

        $result = $this->callBatch($batch);

        if (!empty($result['result']['result_error'][0]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][0]['error_description']) ? $result['result']['result_error'][0]['error_description'] : $result['result']['result_error'][0]['error'];
            return false;
        }

        if (!empty($result['result']['result_error'][1]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][1]['error_description']) ? $result['result']['result_error'][1]['error_description'] : $result['result']['result_error'][1]['error'];
            return false;
        }

        return $result;
    }

    public function getTemplates()
    {
        $result[0] = [];
        $result[1] = [];

        $batch[0]['method'] = 'bizproc.workflow.template.list';
        $batch[0]['params'] = [
            'select' => ['ID', 'NAME'],
            'filter' => ['MODULE_ID' => 'crm', 'ENTITY' => 'CCrmDocumentLead']
        ];
        $batch[1]['method'] = 'bizproc.workflow.template.list';
        $batch[1]['params'] = [
            'select' => ['ID', 'NAME'],
            'filter' => ['MODULE_ID' => 'crm', 'ENTITY' => 'CCrmDocumentContact']
        ];

        $answer = $this->callBatch($batch);

        if (empty($answer['result']['result_error'][0]['error'])
            && !empty($answer['result']['result'][0])
            && is_array($answer['result']['result'][0])) {
            foreach ($answer['result']['result'][0] as $key => $lead) {
                $result[0][$key]['id']   = $lead['ID'] ?? 0;
                $result[0][$key]['name'] = $lead['NAME'] ?? 0;
            }
        } else {
            $result[0][0]['id']   = 0;
            $result[0][0]['name'] = $result['result']['result_error'][0]['error'] ?? '';
        }
        if (empty($answer['result']['result_error'][1]['error'])
            && !empty($answer['result']['result'][1])
            && is_array($answer['result']['result'][1])) {
            foreach ($answer['result']['result'][1] as $key => $lead) {
                $result[1][$key]['id']   = $lead['ID'] ?? 0;
                $result[1][$key]['name'] = $lead['NAME'] ?? 0;
            }
        } else {
            $result[1][0]['id']   = 0;
            $result[1][0]['name'] = $result['result']['result_error'][1]['error'] ?? '';
        }

        return $result;
    }

    public function installTemplates()
    {
        try {

            $result['lead']    = [];
            $result['contact'] = [];

            $batch[0]['method'] = 'bizproc.workflow.template.add';
            $batch[0]['params'] = [
                'DOCUMENT_TYPE' => ['crm', 'CCrmDocumentLead', 'LEAD'],
                'NAME'          => 'Сервисный чат-бот WAZZUP',
                'DESCRIPTION'   => '',
                'AUTO_EXECUTE'  => 0,
                'TEMPLATE_DATA' => base64_encode(file_get_contents(__WORKFLOW_TEMPLATES__ . '/tmpl-lead.bpt'))
            ];
            $batch[1]['method'] = 'bizproc.workflow.template.add';
            $batch[1]['params'] = [
                'DOCUMENT_TYPE' => ['crm', 'CCrmDocumentContact', 'CONTACT'],
                'NAME'          => 'Сервисный чат-бот WAZZUP',
                'DESCRIPTION'   => '',
                'AUTO_EXECUTE'  => 0,
                'TEMPLATE_DATA' => base64_encode(file_get_contents(__WORKFLOW_TEMPLATES__ . '/tmpl-contact.bpt'))
            ];

            $answer = $this->callBatch($batch);

            if (empty($answer['result']['result_error'][0]['error'])
                && !empty($answer['result']['result'][0])
                && is_numeric($answer['result']['result'][0])) {
                $result['lead']['id']   = $answer['result']['result'][0];
                $result['lead']['name'] = 'Сервисный чат-бот WAZZUP';
            } else {
                if (!empty($answer['result']['result_error'][0])) {

                    $messageArr = [];
                    if (!empty($answer['result']['result_error'][0]['error'])) {
                        $messageArr[] = $answer['result']['result_error'][0]['error'];
                    }
                    if (!empty($answer['result']['result_error'][0]['error_description'])) {
                        $messageArr[] = $answer['result']['result_error'][0]['error_description'];
                    }
                    $message = implode(': ', $messageArr);

                } else {
                    $message = 'Неизвестная ошибка';
                }
                throw new Exception($message);
            }

            if (empty($answer['result']['result_error'][1]['error'])
                && !empty($answer['result']['result'][1])
                && is_numeric($answer['result']['result'][1])) {
                $result['contact']['id']   = $answer['result']['result'][1];
                $result['contact']['name'] = 'Сервисный чат-бот WAZZUP';
            } else {

                if (!empty($answer['result']['result_error'][1])) {

                    $messageArr = [];
                    if (!empty($answer['result']['result_error'][1]['error'])) {
                        $messageArr[] = $answer['result']['result_error'][1]['error'];
                    }
                    if (!empty($answer['result']['result_error'][1]['error_description'])) {
                        $messageArr[] = $answer['result']['result_error'][1]['error_description'];
                    }
                    $message = implode(': ', $messageArr);

                } else {
                    $message = 'Неизвестная ошибка';
                }
                throw new Exception($message);
            }

            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function workflowStartForContact($tmpl_contact_id, $workflow_contact_id, $message, $chat_id)
    {
        $method = 'bizproc.workflow.start';
        $params = [
            'TEMPLATE_ID' => $tmpl_contact_id,
            'DOCUMENT_ID' => ['crm', 'CCrmDocumentContact', $workflow_contact_id],
            'PARAMETERS'  => ['TEXT' => $message, 'CHAT_ID' => $chat_id]
        ];

        return $this->call($method, $params);
    }

    public function workflowStartForLead($tmpl_lead_id, $workflow_lead_id, $message, $chat_id)
    {
        $method = 'bizproc.workflow.start';
        $params = [
            'TEMPLATE_ID' => $tmpl_lead_id,
            'DOCUMENT_ID' => ['crm', 'CCrmDocumentLead', $workflow_lead_id],
            'PARAMETERS'  => ['TEXT' => $message, 'CHAT_ID' => $chat_id]
        ];

        return $this->call($method, $params);
    }


    public function transferDialogOpenLine($chat_id)
    {
        $method = 'imopenlines.bot.session.operator';
        $params = [
            'CHAT_ID' => $chat_id
        ];

        return $this->call($method, $params);
    }

    public function transferDialogOperator($chat_id, $workflow_type, $workflow_contact_id, $workflow_lead_id)
    {
        $method = $params = [];
        // получим id ответственного $assigned_by
        if (!empty($workflow_type) && $workflow_type == 'CONTACT' && !empty($workflow_contact_id)) {
            $method = 'crm.contact.get';
            $params = [
                'ID' => $workflow_contact_id
            ];
        }
        if (!empty($workflow_type) && $workflow_type == 'LEAD' && !empty($workflow_lead_id)) {
            $method = 'crm.lead.get';
            $params = [
                'ID' => $workflow_lead_id
            ];
        }
        if (empty($method) || empty($params)) {
            $this->isError = true;
            $this->sqlMessage = 'Отсутствуют данные для получения ответственного';
            return false;
        }

        $answer = $this->call($method, $params);
        if ($this->isError){
            return $answer;
        }
        $assigned_by = $answer['result']['ASSIGNED_BY_ID'] ?? 0;
        if (empty($assigned_by)) {
            $this->isError = true;
            $this->sqlMessage = 'Не найден ответственный';
            return false;
        }

        $method = 'imopenlines.bot.session.transfer';
        $params = [
            'CHAT_ID' => $chat_id,
            'USER_ID' => $assigned_by,
            'LEAVE' => 'N'
        ];

        return $this->call($method, $params);
    }


    public function answerForMissedCall($chat_id, $message) {
        $method = 'imopenlines.bot.session.message.send';
        $params = [
            'CHAT_ID' => $chat_id,
            'MESSAGE' => $message
        ];

        return $this->call($method, $params);
    }


    public function closeDialog($chat_id) {
        $method = 'imopenlines.bot.session.finish';
        $params = [
            'CHAT_ID' => $chat_id,
        ];

        return $this->call($method, $params);
    }


}

