<?php

/* @var $model application\models\Configs
 *
 */

if (!is_string($model)) : ?>
    <div class="container-fluid" id="container">
        <div id="flex-container">
            <div class="raw-item" id="raw">
                <img class="img-left raw-item" id="raw" src="<?php echo __URL_PREFIX__; ?>/images/img-header.png">
            </div>
            <div class="flex-item" id="flex">
                <p style="font-size: 25px; font-weight: bold;">Сервисный чат-бот WAZZUP</p>
                <p>Чат-бот на основе бизнес-процессов Битрикс24. Разработан для сервиса WAZZUP.</p>
            </div>
        </div>
        <div class="row">
            <nav style="width: 100%;">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="true">Настройка</a>
                    <a class="nav-item nav-link" id="connector-tab" data-toggle="tab" href="#connector" role="tab" aria-controls="connector" aria-selected="false">Инструкции</a>
                    <a class="nav-item nav-link" id="support-tab" data-toggle="tab" href="#support" role="tab" aria-controls="support" aria-selected="false">Поддержка</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent" style="width: 100%;">
                <div class="tab-pane fade show active" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                    <div class="col-sm-12">
                        <?php require_once __DIR__ . '/../configs/_form.php'; ?>
                        <input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>/bitrix"/>
                    </div>
                </div>
                <div class="tab-pane fade" id="connector" role="tabpanel" aria-labelledby="connector-tab">
                    <div class="col-sm-12">
                        <?php echo @file_get_contents('https://docs.google.com/document/d/1Khtwic0At-eMpmHeQO2vwqStMlkZJU1HKj_caLJhgWU/export?format=txt'); ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="support" role="tabpanel" aria-labelledby="support-tab">
                    <div class="col-sm-12">
                        <p>
                            Техническая поддержка приложения осуществляется через
                            <a href="https://www.bitrix24.net/oauth/select/?preset=im&IM_DIALOG=networkLineseccd342407061fa70fe5f4246d8ee0d9" target="_blank">чат технической поддержки компании BIZZUP</a>.
                        </p>
                        <p>
                            В остальных случаях техническая поддержка осуществляется на платной основе либо через
                            <a href="https://www.bitrix24.net/oauth/select/?preset=im&IM_DIALOG=networkLineseccd342407061fa70fe5f4246d8ee0d9" target="_blank">чат технической поддержки</a>, либо по электронной почте
                            <a style="color: black;" href="mailto:support@bizzup.ru">support@bizzup.ru</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <p class="text_footer">Разработчик "BIZZUP"
                <a style="color: black;" href="tel:+73435230670">+7(3435)230-670</a>,
                <u><a href="mailto:support@bizzup.ru" style="color: black;" target="_blank">support@bizzup.ru</a></u>,
                <u><a href="https://bizzup.ru/?utm_source=market" target="_blank">https://bizzup.ru</a></u>
            </p>
        </div>
    </div>
<?php else :
    echo $model;
endif;
?>
<script>
    (function (w, d, u) {
        var s = d.createElement('script');
        s.async = true;
        s.src = u + '?' + (Date.now() / 60000 | 0);
        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document, 'https://cdn-ru.bitrix24.ru/b4612775/crm/site_button/loader_3_u1g13h.js');
</script>