<?php
/* @var application\models\ChatsMessages $model */
use application\models\ChatsMessages;

$viewTableProperties = ChatsMessages::getViewTableProperties();
?>
<table class="table table-striped table-bordered">
    <tbody>
    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
        <tr><th><?php echo $propertyParam['label'] ?? $propertyName; ?></th><td><?php echo $model->$propertyName; ?></td></tr>
    <?php endforeach; ?>
    </tbody>
</table>




