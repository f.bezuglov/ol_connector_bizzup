<?php
/**
 * @var application\models\joins\ChatsMessagesJoins[] $models
 * @var \application\models\BotActions[][] $botActionsModels
 */

use application\models\joins\ChatsMessagesJoins;
$viewTableProperties = ChatsMessagesJoins::getViewTableProperties();

?>

<?php if (empty($models)): ?>
<div class="expand-row-div mb-2" style="display: none">
    Сообщений нет
</div>
<?php elseif (is_string($models)): ?>
<div class="expand-row-div mb-2" style="display: none">
    <?php echo $models; ?>
</div>
<?php else: ?>
    <div class="card card-primary card-outline expand-row-div" style="display: none">
        <div class="card-header">
            <div class="card-title" style="cursor: move;">
                <i class="fa fa-edit"></i>&nbsp;Messages
            </div>
        </div>
        <div class="card-body" style="padding: 0">
            <table class="table table-striped table-bordered text-center">
                <thead>
                <tr>
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                        <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <th class="align-middle" style="<?php echo !empty($propertyParam['style']) ? $propertyParam['style'] : 'width:auto;'; ?>"><?php echo $propertyParam['label'] ?? $propertyName; ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($models as $key => $model) : ?>
                    <tr class="align-middle data-rows" data-id="<?php echo $model->id; ?>">
                        <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                            <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                            <td><?php echo $model->getStr($propertyName); ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php foreach ($botActionsModels[$key] as $botActionsModel) : ?>
                        <tr class="data-bot-rows" data-id="<?php echo $botActionsModel->id; ?>">
                            <td class="text-left" colspan="<?php echo count($viewTableProperties); ?>">
                                <i>Bot action (<span class="has-<?php echo $botActionsModel->result; ?>"><?php echo $botActionsModel->result; ?></span>) : <?php echo $botActionsModel->action; ?></i>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>



