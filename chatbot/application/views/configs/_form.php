<?php
/* @var application\models\joins\ConfigsJoins $model
 * @var array $curLabels
 * @var array $lead_templates
 * @var array $contact_templates
 */

use application\models\Configs;

$labels         = Configs::getViewTableProperties();

?>
<div class="mb-4">
    <button id="configs-reset" class="btn btn-outline-secondary" type="button" title="Установить все настройки по умолчанию">Настроить по умолчанию</button>
</div>
<form id="form-config" class="form-row" method="post">
    <div class="col-sm-5 mr-3 pjax-container">
        <div class="form-group">
            <label for="towards_send_dialog" class="control-label"><?php echo $labels['towards_send_dialog']['label']; ?></label>
            <select class="form-control" name="towards_send_dialog" id="towards_send_dialog" style="width: 100%">
                <option value="1" <?php echo (isset($model->towards_send_dialog) && $model->towards_send_dialog == 1) ? 'selected=""' : ''; ?>>В открытую линию</option>
                <option value="2" <?php echo (isset($model->towards_send_dialog) && $model->towards_send_dialog == 2) ? 'selected=""' : ''; ?>>Ответственному</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="tmpl_lead_id" class="control-label"><?php echo $labels['tmpl_lead_id']['label']; ?></label>
            <select class="form-control" name="tmpl_lead_id" id="tmpl_lead_id" style="width: 100%">
                <option value="0" <?php echo empty($model->tmpl_lead_id) ? 'selected=""' : ''; ?>>Не использовать</option>
                <?php foreach ($lead_templates as $lead_template) : ?>
                    <option value="<?php echo $lead_template['id']; ?>" <?php echo (!empty($model->tmpl_lead_id) && $model->tmpl_lead_id == $lead_template['id']) ? 'selected=""' : ''; ?>><?php echo $lead_template['name'] . (!empty($lead_template['id']) ? (' | ID = ' . $lead_template['id']) : ''); ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group">
            <label for="tmpl_contact_id" class="control-label"><?php echo $labels['tmpl_contact_id']['label']; ?></label>
            <select class="form-control" name="tmpl_contact_id" id="tmpl_contact_id" style="width: 100%">
                <option value="0" <?php echo empty($model->tmpl_contact_id) ? 'selected=""' : ''; ?>>Не использовать</option>
                <?php foreach ($contact_templates as $contact_template) : ?>
                    <option value="<?php echo $contact_template['id']; ?>" <?php echo (!empty($model->tmpl_contact_id) && $model->tmpl_contact_id == $contact_template['id']) ? 'selected=""' : ''; ?>><?php echo $contact_template['name'] . (!empty($contact_template['id']) ? (' | ID = ' . $contact_template['id']) : ''); ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group">
            <button id="install-templates" type="button" class="btn btn-lg btn-success btn-sm ladda-button" data-style="expand-right">Добавить новые шаблоны</button>
        </div>
    </div>
    <div class="col-sm-12 pjax-container">
        <div class="form-group required">
            <label><?php echo $labels['is_ignore_str']['label']; ?></label>
            <div class="table-responsive" id="is-ignore-str-grid-container">
                <table class="table table-dynamic m-0">
                    <tbody class="table-dynamic-tbody">
                    <?php foreach ($model->is_ignore_str as $key => $isIgnoreStr) : ?>
                    <tr id="table-dynamic-row-<?php echo $key; ?>" class="table-dynamic-row" data-row-index="<?php echo $key; ?>">
                        <td style="width: 90%">
                            <input name="is_ignore_str[]" type="text" class="form-control" placeholder="Введите значение ..." value="<?php echo $isIgnoreStr ?? ''; ?>">
                            <div class="help-block"></div>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn-table-dynamic-delete-item btn btn-danger btn-sm" title="Убрать">
                                <i class="fa fa-minus"></i>
                            </button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="table-dynamic-tfoot">
                        <td>
                            <div class="help-block"></div>
                        </td>
                        <td class="text-center">
                            <button id="btn-is-ignore-str-add-item" type="button" class="btn btn-success btn-sm" title="Добавить">
                                <span class="fa fa-plus"></span>
                            </button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12 pjax-container">
        <div class="form-group required">
            <label><?php echo $labels['is_system_str']['label']; ?></label>
            <div class="form-group form-check">
                <input name="is_system_autoclose" type="checkbox" class="form-check-input" id="is-system-autoclose" value="1"<?php echo !empty($model->is_system_autoclose) ? ' checked="checked"' : ''; ?>>
                <label class="form-check-label" for="is-system-autoclose"><?php echo $labels['is_system_autoclose']['label']; ?></label>
            </div>
            <div class="table-responsive" id="is-system-str-grid-container">
                <table class="table table-dynamic m-0">
                    <tbody class="table-dynamic-tbody">
                    <?php foreach ($model->is_system_str as $key => $isSystemStr) : ?>
                        <tr id="table-dynamic-row-<?php echo $key; ?>" class="table-dynamic-row" data-row-index="<?php echo $key; ?>">
                            <td style="width: 90%">
                                <input name="is_system_str[]" type="text" class="form-control" placeholder="Введите значение ..." value="<?php echo $isSystemStr ?? ''; ?>">
                                <div class="help-block"></div>
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn-table-dynamic-delete-item btn btn-danger btn-sm" title="Убрать">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="table-dynamic-tfoot">
                        <td>
                            <div class="help-block"></div>
                        </td>
                        <td class="text-center">
                            <button id="btn-is-system-str-add-item" type="button" class="btn btn-success btn-sm" title="Добавить">
                                <span class="fa fa-plus"></span>
                            </button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12 pjax-container">
        <div class="form-group required">
            <label><?php echo $labels['is_operator_str']['label']; ?></label>
            <div class="table-responsive" id="is-operator-str-grid-container">
                <table class="table table-dynamic m-0">
                    <tbody class="table-dynamic-tbody">
                    <?php foreach ($model->is_operator_str as $key => $isOperatorStr) : ?>
                        <tr id="table-dynamic-row-<?php echo $key; ?>" class="table-dynamic-row" data-row-index="<?php echo $key; ?>">
                            <td style="width: 90%">
                                <input name="is_operator_str[]" type="text" class="form-control" placeholder="Введите значение ..." value="<?php echo $isOperatorStr ?? ''; ?>">
                                <div class="help-block"></div>
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn-table-dynamic-delete-item btn btn-danger btn-sm" title="Убрать">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="table-dynamic-tfoot">
                        <td>
                            <div class="help-block"></div>
                        </td>
                        <td class="text-center">
                            <button id="btn-is-operator-str-add-item" type="button" class="btn btn-success btn-sm" title="Добавить">
                                <span class="fa fa-plus"></span>
                            </button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12 pjax-container">
        <div class="form-group required">
            <label><?php echo $labels['is_missed_call_str']['label']; ?></label>
            <div class="table-responsive" id="is-missed-call-str-grid-container">
                <table class="table table-dynamic m-0">
                    <tbody class="table-dynamic-tbody">
                    <?php foreach ($model->is_missed_call_str as $key => $isMissedCallStr) : ?>
                        <tr id="table-dynamic-row-<?php echo $key; ?>" class="table-dynamic-row" data-row-index="<?php echo $key; ?>">
                            <td style="width: 90%">
                                <input name="is_missed_call_str[]" type="text" class="form-control" placeholder="Введите значение ..." value="<?php echo $isMissedCallStr ?? ''; ?>">
                                <div class="help-block"></div>
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn-table-dynamic-delete-item btn btn-danger btn-sm" title="Убрать">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="table-dynamic-tfoot">
                        <td>
                            <div class="help-block"></div>
                        </td>
                        <td class="text-center">
                            <button id="btn-is-missed-call-str-add-item" type="button" class="btn btn-success btn-sm" title="Добавить">
                                <span class="fa fa-plus"></span>
                            </button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12 pjax-container">
        <div class="form-group">
            <label for="is_missed_call_answer_str"><?php echo $labels['is_missed_call_answer_str']['label']; ?></label>
            <input name="is_missed_call_answer_str" type="text" class="form-control" id="is_missed_call_answer_str" placeholder="Введите значение ..." value="<?php echo $model->is_missed_call_answer_str ?? ''; ?>">
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-sm-12 mt-25">
        <button type="submit" class="btn btn-lg <?php echo $model->isNewRecord ? 'btn-success' : 'btn-primary'; ?>"><?php echo $model->isNewRecord ? 'Добавить' : 'Обновить'; ?></button>
    </div>
    <input type="hidden" name="id" value="<?php echo $model->isNewRecord ? '0' : $model->id; ?>"/>
    <input type="hidden" name="member_id" value="<?php echo $model->member_id; ?>"/>
    <input type="hidden" name="action" value="<?php echo $model->isNewRecord ? 'create' : 'update'; ?>"/>
    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
</form>
