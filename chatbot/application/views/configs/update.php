<?php

/** @var $model application\models\Configs
 *
 */

$this->title                             = 'Изменить настройки';
$this->params['breadcrumbs'][0]['url']   = __URL_PREFIX__ . '/clients/index';
$this->params['breadcrumbs'][0]['label'] = 'Клиенты';
$this->params['breadcrumbs'][1]          = $this->title;

if (!is_string($model)) : ?>
    <div class="row mb-4">
        <div class="col-sm-12">
            <h6>Клиент: <?php echo $model->member_name ?? 0; ?></h6>
        </div>
    </div>
    <div id="settings">
        <?php
        require_once __DIR__ . '/_form.php'; ?>
        <input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>/configs"/>
    </div>
<?php
else :
    echo $model;
endif;