<?php

/* @var application\models\Configs $model
 * @var application\models\Authentications $authenticationsModel
 * @var application\models\Users[] $users
 */

$curLabels = $model->attributeLabels();

$this->title                   = 'Параметры';
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => __URL_PREFIX__ . '/clients/index'];
$this->params['breadcrumbs'][] = $this->title;

if (!is_string($model)) : ?>
    <div class="row mb-4">
        <div class="col-sm-12">
            <h6>Клиент: <?php echo $authenticationsModel->member_name; ?></h6>
        </div>
    </div>
<?php else :
    echo $model;
endif;

?>
<form class="form-row pb-4" action="params?member_id=<?php echo $model->member_id; ?>" method="post">
    <div class="col-md-12 pr-4">
        <div class="card card-default">
            <h5 class="card-title card-header text-center">Ответственный за уведомления пациентам</h5>
            <div class="card-body">
                <div class="form-group">
                    <label for="user_responsible_phone"><?php echo $curLabels['user_responsible_phone']; ?></label>
                    <input name="user_responsible_phone" type="text" class="form-control" id="user_responsible_phone" placeholder="Введите телефон ответственного" value="<?php echo $model->user_responsible_phone ?? ''; ?>">
                    <div class="help-block"></div>
                </div>
                <div class="form-group">
                    <label for="user_responsible_fio"><?php echo $curLabels['user_responsible_fio']; ?></label>
                    <input name="user_responsible_fio" type="text" class="form-control" id="user_responsible_fio" placeholder="Введите имя ответственного" value="<?php echo $model->user_responsible_fio ?? ''; ?>">
                    <div class="help-block"></div>
                </div>
                <div class="text-navy">
                    <small><i>* Необходимо указать человека, кто может включать/отключать уведомления клиентам на портале. Отправляются сообщения для подтверждения массовой загрузки информации из МИС.</i></small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Изменить</button>
    </div>
    <input type="hidden" name="id" value="<?php echo $model->id; ?>"/>
    <input type="hidden" name="member_id" value="<?php echo $model->member_id; ?>"/>
    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
</form>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>"/>
