<?php
/* @var application\models\BotActions $model */
use application\models\BotActions;

$viewTableProperties = BotActions::getViewTableProperties();
?>
<table class="table table-striped table-bordered">
    <tbody>
    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
        <tr><th><?php echo $propertyParam['label'] ?? $propertyName; ?></th><td><?php echo $model->$propertyName; ?></td></tr>
    <?php endforeach; ?>
    </tbody>
</table>




