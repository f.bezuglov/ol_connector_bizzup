<?php
/* @var $model application\models\forms\RequestUserConfirmActionForm
 */
?>
<div class="login-box">
    <div class="login-logo">
        <a href="http://app.bizzup.ru<?php echo __URL_PREFIX__; ?>"><b>ColorExpert</b></a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Запрос сброса пароля</p>
            <form method="post" id="request-password-reset">
                <div class="form-group">
                    <label for="email" class="required">Электронная почта</label>
                    <input type="text" id="email" class="form-control" name="email" aria-required="true">
                    <div class="help-block"></div>
                </div>
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>