<div class="login-box">
    <div class="login-logo">
        <a href="http://app.bizzup.ru/mres/admin"><b>ChatBot</b></a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Авторизация</p>
            <form method="post" id="login">
                <div class="mb-3">
                    <div class="input-group">
                        <input class="form-control" type="text" name="login" placeholder="Введите электронную почту">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="help-block"></div>
                </div>
                <div class="mb-3">
                    <div class="input-group">
                        <input class="form-control" type="password" name="password" placeholder="Введите пароль">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="help-block"></div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="rememberMe" value="1" checked="">
                            <label for="remember">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-primary btn-block">Войти</button>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-12 mt-2">
                    <a href="/request-password-reset">Забыли пароль?</a>
                </div>
            </div>
        </div>
    </div>
</div>
