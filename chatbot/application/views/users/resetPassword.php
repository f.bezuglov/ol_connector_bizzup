<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\ResetPasswordForm */

$this->title = Yii::t('users', 'RESET_PASSWORD');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>TenderQuest</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo $this->title ?></p>
        <div class="site-reset-password">

            <div class="row">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'enableAjaxValidation' => true]); ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('users', 'SAVE'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
