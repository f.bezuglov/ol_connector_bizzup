<?php
/* @var $model application\models\Users
 * @var $selectsValues array
 */

$this->title                   = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
$labels                        = $model->attributeLabels();
?>

<div class="row">
    <div class="col-xs-12 col-md-7">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Личные данные</h3>
            </div>
            <div class="card-body">
                <form action="<?php echo __URL_PREFIX__; ?>/profile" method="post" id="form-profile">
                    <div class="form-group">
                        <label for="name"><?php echo $labels['name']; ?></label>
                        <input id="name" class="form-control" type="text" name="name" value="<?php echo $model->name; ?>" aria-required="true" aria-invalid="false">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <label for="sex"><?php echo $labels['sex']; ?></label>
                        <select id="sex" class="form-control select2" style="width: 100%;" name="sex" aria-invalid="false">
                            <?php foreach ($selectsValues['sexes'] as $sexId => $sexName) : ?>
                                <option value="<?php echo $sexId; ?>"<?php echo $sexId == $model->sex ? ' selected="selected"' : ''; ?>><?php echo $sexName; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <label for="user-photo"><?php echo $labels['photo']; ?></label>
                        <div class="cropper-widget">
                            <input type="hidden" id="user-photo" class="photo-field" name="photo" value="<?php echo $model->photo; ?>">
                            <img class="thumbnail" src="<?php echo !empty($model->photo) ? $model->photo : (__URL_PREFIX__ . '/images/nophoto.png'); ?>" alt="" style="max-height: 300px; max-width: 300px" data-no-photo="<?php echo __URL_PREFIX__; ?>/images/nophoto.png">
                            <div class="cropper-buttons">
                                <button type="button" class="btn btn-sm btn-danger delete-photo" aria-label="Удалить фото">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Удалить фото        </button>
                                <button type="button" class="btn btn-sm btn-success crop-photo hidden" aria-label="Обрезать фото">
                                    <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span> Обрезать фото        </button>
                                <button type="button" class="btn btn-sm btn-info upload-new-photo hidden" aria-label="Загрузить другое фото">
                                    <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Загрузить другое фото        </button>
                            </div>

                            <div class="new-photo-area" style="height: 300px; width: 300px;">
                                <div class="cropper-label" style="z-index: 16777271;">
                                    <span>Для загрузки новой фотографии, кликните здесь или перетащите файл сюда</span>
                                </div>
                            </div>
                            <div class="progress hidden" style="width: 300px;">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" style="width: 0%">
                                    <span class="sr-only"></span>
                                </div>
                            </div>
                        </div>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                    <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">
                    <input type="hidden" name="form_name" value="profile">
                </form>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Сменить пароль</h3>
            </div>
            <div class="card-body">
                <form action="<?php echo __URL_PREFIX__; ?>/profile" method="post" id="form-password">
                    <div class="form-group">
                        <label for="old_password"><?php echo $labels['old_password']; ?></label>
                        <input type="password" id="old_password" class="form-control" name="old_password" aria-required="true" aria-invalid="true">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <label for="new_password"><?php echo $labels['new_password']; ?></label>
                        <input type="password" id="new_password" class="form-control" name="new_password" aria-required="true" aria-invalid="true">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <label for="new_password_repeat"><?php echo $labels['new_password_repeat']; ?></label>
                        <input type="password" id="new_password_repeat" class="form-control" name="new_password_repeat" aria-required="true" aria-invalid="true">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                    <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">
                    <input type="hidden" name="form_name" value="password">
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Сменить e-mail</h3>
            </div>
            <div class="card-body">
                <form action="<?php echo __URL_PREFIX__; ?>/profile" method="post" id="form-email">
                    <div class="form-group">
                        <label for="email"><?php echo $labels['new_email']; ?></label>
                        <input type="email" id="email" class="form-control" name="email" aria-required="true" aria-invalid="true">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                    <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">
                    <input type="hidden" name="form_name" value="email">
                </form>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>"/>