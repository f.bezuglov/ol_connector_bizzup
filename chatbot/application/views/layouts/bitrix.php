<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo __URL_PREFIX__; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/fonts/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/bitrix.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid" id="container">
    <?php echo $content; ?>
</div>
<script src="<?php echo __URL_PREFIX__; ?>/js/jquery.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/pjax/pjax.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/configs.js"></script>
</body>
</html>
