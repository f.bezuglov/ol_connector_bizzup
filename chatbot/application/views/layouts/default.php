<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CHATBOT | <?php echo $this->title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="<?php echo __URL_PREFIX__; ?>/favicon.ico">
    <link href="<?php echo __URL_PREFIX__; ?>/fonts/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/ionicons.min.css" rel="stylesheet">
    <?php if (!empty($get_needed_css) && is_array($get_needed_css)) {
        foreach ($get_needed_css as $file) {
            echo '<link href="' . __URL_PREFIX__ . $file . '" rel="stylesheet">';
        }
    } ?>
    <link href="<?php echo __URL_PREFIX__; ?>/css/adminlte.css" rel="stylesheet">
    <link href="<?php echo __URL_PREFIX__; ?>/css/admin.css" rel="stylesheet">
</head>
<body class="sidebar-mini <?php echo !empty($_SESSION['sidebar-toggle']) ? ' sidebar-collapse' : ''; ?>">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand border-bottom-0 navbar-dark navbar-primary">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link sidebar-toggle" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <!-- SEARCH FORM -->
            <form class="form-inline ml-auto">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Поиск" aria-label="Поиск">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </ul>
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item user-menu dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                    <?php if (empty($_SESSION['curUser']['photo'])) : ?>
                        <img src="<?php echo __URL_PREFIX__; ?>/images/users/<?php echo \application\models\Users::getDefaultPhoto(); ?>.png" class="user-image img-circle elevation-2" alt="Фото пользователя">
                    <?php else : ?>
                        <img src="<?php echo $_SESSION['curUser']['photo']; ?>" class="user-image img-circle elevation-2" alt="Фото пользователя">
                    <?php endif; ?>
                    <span class="hidden-xs"><?php echo $_SESSION['curUser']['name'] ?? ''; ?></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-md-right">
                    <li class="user-header">
                        <?php if (empty($_SESSION['curUser']['photo'])) : ?>
                            <img src="<?php echo __URL_PREFIX__; ?>/images/users/<?php echo \application\models\Users::getDefaultPhoto(); ?>.png" class="user-image img-circle elevation-2" alt="Фото пользователя">
                        <?php else : ?>
                            <img src="<?php echo $_SESSION['curUser']['photo']; ?>" class="user-image img-circle elevation-2" alt="Фото пользователя">
                        <?php endif; ?>
                        <p><?php echo $_SESSION['curUser']['name'] ?? ''; ?></p>
                    </li>
                    <li class="user-footer">
                        <div class="float-left">
                            <a class="btn btn-default btn-flat" href="<?php echo __URL_PREFIX__; ?>/profile" data-method="post">Профиль</a>                                </div>
                        <div class="float-right">
                            <a class="btn btn-default btn-flat" href="<?php echo __URL_PREFIX__; ?>/logout" data-method="post">Выйти</a>                                </div>
                    </li>

                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <a href="<?php echo __URL_PREFIX__; ?>/index" class="brand-link text-center">
            <span class="brand-text font-weight-light">CHATBOT</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="<?php echo __URL_PREFIX__; ?>/" class="nav-link<?php echo ($this->route['controller'] == 'main') ? ' active' : ''; ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Аналитика
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo __URL_PREFIX__; ?>/clients/index" class="nav-link<?php echo ($this->route['controller'] == 'clients') ? ' active' : ''; ?>">
                            <i class="nav-icon fas fa-handshake"></i>
                            <p>
                                Клиенты
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo __URL_PREFIX__; ?>/logs/index" class="nav-link<?php echo ($this->route['controller'] == 'logs') ? ' active' : ''; ?>">
                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                Логи
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"><?php echo $this->title; ?></h1>
                    </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <?php if (!empty($this->params['breadcrumbs'])) : ?>
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo __URL_PREFIX__; ?>/index">Главная</a>
                                    </li>
                                    <?php foreach ($this->params['breadcrumbs'] as $breadcrumbs) : ?>
                                        <?php if (is_string($breadcrumbs)) : ?>
                                            <li class="breadcrumb-item active"><?php echo $breadcrumbs; ?></li>
                                        <?php elseif (is_array($breadcrumbs)) : ?>
                                            <li class="breadcrumb-item">
                                                <a href="<?php echo $breadcrumbs['url'] ?? ''; ?>"><?php echo $breadcrumbs['label'] ?? ''; ?></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ol>
                        </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="container-fluid">
                <?php if (!empty($_SESSION['saveMessage']) && is_array($_SESSION['saveMessage'])) : ?>
                <?php foreach ($_SESSION['saveMessage'] as $saveMessage) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert-<?php echo $saveMessage['type'] ?? ''; ?> alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="icon fa fa-check"></i><?php echo $saveMessage['message'] ?? ''; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php unset($_SESSION['saveMessage']); ?>
                <?php endif; ?>
                <?php echo $content; ?>
                <script>

                </script>
            </div>
        </div>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; 2022 <a href="https://bizzup.ru">bizzup.ru</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1
        </div>
    </footer>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-lg" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Large Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-xl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<script src="<?php echo __URL_PREFIX__; ?>/js/jquery.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/adminlte.min.js"></script>
<?php if (!empty($get_needed_js) && is_array($get_needed_js)) {
    foreach ($get_needed_js as $file) {
        echo '<script src="' . __URL_PREFIX__ . $file . '"></script>';
    }
} ?>
<script>
    $(document).ready(function ()
    {
        $('.sidebar-toggle').on('click', function () {
            $.ajax({
                url: '<?php echo __URL_PREFIX__; ?>/main/sidebar-toggle',
            });
        });
        /*$(".alert").fadeTo(2000, 1000).slideUp(1000, function(){
            $(".alert").slideUp(1000);
        });*/
    });
</script>
</body>
</html>
