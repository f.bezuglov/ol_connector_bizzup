<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CHATBOT | Вход</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo __URL_PREFIX__; ?>/favicon.ico">
    <link rel="stylesheet" href="<?php echo __URL_PREFIX__; ?>/fonts/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo __URL_PREFIX__; ?>/css/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo __URL_PREFIX__; ?>/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo __URL_PREFIX__; ?>/css/admin.css">
</head>
<body class="hold-transition login-page text-sm">
<div>
    <?php if (!empty($_SESSION['saveMessage'])) : ?>
        <div class="mt-5 alert-<?php echo $_SESSION['saveMessage'][0]['type']; ?> alert">
            <i class="icon fa fa-times"></i><?php echo $_SESSION['saveMessage'][0]['message']; ?>
        </div>
        <?php unset($_SESSION['saveMessage']); ?>
    <?php endif; ?>
    <?php echo $content; ?>
</div>
<script src="<?php echo __URL_PREFIX__; ?>/js/jquery.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/adminlte.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo __URL_PREFIX__; ?>/js/login.js"></script>
</body>
</html>
