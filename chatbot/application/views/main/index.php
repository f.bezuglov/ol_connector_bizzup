<?php
/*
 */

$this->title                   = 'Аналитика';

?>

<div class="row">
    <div class="col-md-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <p>Пользователи</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo __URL_PREFIX__; ?>/admin/index" class="small-box-footer">
                Управление <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <p>База данных</p>
            </div>
            <div class="icon">
                <i class="ion ion-refresh"></i>
            </div>
            <a href="<?php echo __URL_PREFIX__; ?>/db-backups/index" class="small-box-footer">
                Экспорт <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>