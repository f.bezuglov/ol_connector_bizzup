<?php

/* @var $model application\models\Authentications
 * @var $selectsValues array
 */

use application\models\Authentications;

$curLabels = Authentications::getViewTableProperties();
?>
<form class="form mb-3" action="<?php echo $model->isNewRecord ? 'create' : 'update?id=' . $model->id; ?>" method="post">
    <div class="card card-default">
        <h5 class="card-title card-header text-center">Общие настройки</h5>
        <div class="card-body">
            <div class="form-group required">
                <label for="member_name"><?php echo $curLabels['member_name']['label']; ?></label>
                <input id="member_name" class="form-control" type="text" name="member_name" value="<?php echo $model->member_name; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group required">
                <label for="member_id"><?php echo $curLabels['member_id']['label']; ?></label>
                <input id="member_id" class="form-control" type="text" name="member_id" value="<?php echo $model->member_id; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group required">
                <label for="bot_id"><?php echo $curLabels['bot_id']['label']; ?></label>
                <input id="bot_id" class="form-control" type="text" name="bot_id" value="<?php echo $model->bot_id; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group required">
                <label for="domain"><?php echo $curLabels['domain']['label']; ?></label>
                <input id="domain" class="form-control" type="text" name="domain" value="<?php echo $model->domain; ?>">
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div class="card card-default">
        <h5 class="card-title card-header text-center">Авторизация</h5>
        <div class="card-body">
            <div class="form-group">
                <label for="access_token"><?php echo $curLabels['access_token']['label']; ?></label>
                <input id="access_token" class="form-control" type="text" name="access_token" value="<?php echo $model->access_token; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group">
                <label for="expires_in"><?php echo $curLabels['expires_in']['label']; ?></label>
                <input id="expires_in" class="form-control" type="text" name="expires_in" value="<?php echo $model->expires_in; ?>">
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div class="card card-default">
        <h5 class="card-title card-header text-center">Продление авторизация</h5>
        <div class="card-body">
            <div class="form-group">
                <label for="refresh_token"><?php echo $curLabels['refresh_token']['label']; ?></label>
                <input id="refresh_token" class="form-control" type="text" name="refresh_token" value="<?php echo $model->refresh_token; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group">
                <label for="client_id"><?php echo $curLabels['client_id']['label']; ?></label>
                <input id="client_id" class="form-control" type="text" name="client_id" value="<?php echo $model->client_id; ?>">
                <div class="help-block"></div>
            </div>
            <div class="form-group">
                <label for="client_secret"><?php echo $curLabels['client_secret']['label']; ?></label>
                <input id="client_secret" class="form-control" type="text" name="client_secret" value="<?php echo $model->client_secret; ?>">
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn <?php echo $model->isNewRecord ? 'btn-success' : 'btn-primary'; ?>">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Изменить'; ?>
        </button>
    </div>
    <input type="hidden" name="id" value="<?php echo $model->id; ?>"/>
    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
</form>
