<?php
/* @var $model application\models\joins\OperationsJoins */
use application\helpers\Helpers;
?>
<?php $curLabels = $model->attributeLabels(); ?>
<p>
    <a class="btn btn-danger row-delete" data-id="<?php echo $model->id; ?>">Удалить</a>
</p>
<table class="table table-striped table-bordered">
    <tbody>
    <tr><th><?php echo $curLabels['id']; ?></th><td><?php echo $model->id; ?></td></tr>
    <tr><th><?php echo $curLabels['result']; ?></th><td><?php echo $model->result; ?></td></tr>
    <tr><th><?php echo $curLabels['details']; ?></th><td><?php echo $model->details; ?></td></tr>
    <tr><th><?php echo $curLabels['debug']; ?></th><td><?php echo $model->debug; ?></td></tr>
    <tr><th><?php echo $curLabels['created_at']; ?></th><td><?php echo Helpers::date_reformat($model->created_at, 'd.m.Y H:i:s', 'Y-m-d H:i:s.u');  ?></td></tr>
    </tbody>
</table>



