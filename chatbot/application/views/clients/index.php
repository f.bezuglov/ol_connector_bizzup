<?php
/**
 * @var application\models\joins\AuthenticationsJoins[] $models
 * @var $params array
 * @var $selectValues array
 * @var \application\lib\Pagination $pagination
 */

$this->title                   = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;

use application\models\Authentications;

$viewTableProperties = Authentications::getViewTableProperties();
?>
<div class="row mb-4">
    <div class="col-sm-12">
        <div class="float-right">
            <a href="<?php echo __URL_PREFIX__; ?>/clients/create" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Добавить клиента</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php if (empty($models)): ?>
            Клиентов нет
        <?php elseif (is_string($models)): ?>
            <?php echo $models; ?>
        <?php else: ?>
        <div id="accordion" style="width: 100%">
            <?php foreach ($models as $model) : ?>
                <div class="card">
                    <div class="card-header" id="heading_<?php echo $model->id; ?>">

                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo $model->id; ?>" aria-expanded="true" aria-controls="collapseOne">
                            <h5 class="mb-0"><?php echo $model->member_name; ?></h5>
                        </button>

                        <div class="float-right">
                            <?php if (empty($model->configs_id)) : ?>
                                <a href="<?php echo __URL_PREFIX__; ?>/configs/create?member_id=<?php echo $model->member_id; ?>" class="btn btn-success btn-sm pull-right"><i class="fa fa-cog"></i>&nbsp;&nbsp;Добавить настройки</a>
                            <?php else : ?>
                                <a href="<?php echo __URL_PREFIX__; ?>/chats/index?member_id=<?php echo $model->member_id; ?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-edit"></i>&nbsp;&nbsp;История чатов</a>
                                <a href="<?php echo __URL_PREFIX__; ?>/configs/update?member_id=<?php echo $model->member_id; ?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-cog"></i>&nbsp;&nbsp;Изменить настройки</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div id="collapse_<?php echo $model->id; ?>" class="collapse" aria-labelledby="heading_<?php echo $model->member_id; ?>" data-parent="#accordion">
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                                    <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong><?php echo $propertyParam['label']; ?></strong>
                                        </div>
                                        <div class="col-sm-7">
                                            <?php echo $model->$propertyName; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-12">
                                <div class="float-right">
                                    <a href="<?php echo __URL_PREFIX__; ?>/clients/update?id=<?php echo $model->id; ?>" class="btn btn-success btn-sm"><i class="fa fa-pen-alt"></i>&nbsp;&nbsp;Изменить</a>
                                    <a href="<?php echo __URL_PREFIX__; ?>/clients/delete" data-id="<?php echo $model->id; ?>" class="btn btn-danger btn-sm client-delete"><i class="fa fa-trash-alt"></i>&nbsp;&nbsp;Удалить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>"/>
<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
