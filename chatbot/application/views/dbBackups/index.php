<?php
/* @var $paginationHeader string
 * @var $paginationFooter string
 * @var $paths String[]
 * @var $params array
 * @var $selectValues array
 */

$this->title = 'Дампы базы данных';
$this->params['breadcrumbs'][] = $this->title;

use application\helpers\Helpers;
?>
<div class="row mb-4">
    <div class="col-sm-12">
        <div class="float-left">
            <button id="row-create" class="btn btn-success btn-sm ladda-button" title="Создать дамп БД (экспорт)" data-style="expand-right" ><span class="fa fa-plus"></span>&nbsp;Создать дамп БД (экспорт)</button>
        </div>
        <div class="float-right">
            <div class="summary"><?php echo $paginationHeader; ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive grid-container">
            <table class="table table-striped table-bordered">
                <thead>
                <tr class="sorts">
                    <th style="width:8%;">#</th>
                    <th style="width:70%;">Путь к дампу БД</th>
                    <th style="min-width:50px;" rowspan="2">&nbsp;</th>
                </tr>
                </thead>
                <tbody id="pjax-container">
                <?php if (empty($paths)): ?>
                    <tr>
                        <td colspan="3">Дампов нет</td>
                    </tr>
                <?php elseif (is_string($paths)): ?>
                    <tr>
                        <td colspan="3"><?php echo $paths; ?></td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($paths as $key=>$path) : ?>
                        <tr id="<?php echo $key; ?>" class="data-row" data-key="<?php echo $key; ?>">
                            <td><?php echo $key+1; ?></td>
                            <td><?php echo $path; ?></td>
                            <td>
                                <?php if ($key > 0) : ?>
                                <div>
                                    <a href="#" title="Удалить" class="row-delete" data-path="<?php echo $path; ?>">
                                        <i class="fa fa-trash-alt"></i>
                                    </a>
                                </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>" />
