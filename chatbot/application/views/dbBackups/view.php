<?php
/* @var $model application\models\joins\OperationsJoins */
use application\lib\Helpers;
?>
<?php $curLabels = $model->attributeLabels(); ?>
<p>
    <a class="btn btn-primary btn-change" href="/operations/update?id=<?php echo $model->id; ?>">Изменить</a>
    <a class="btn btn-danger row-delete" data-id="<?php echo $model->id; ?>">Удалить</a>
</p>
<table class="table table-striped table-bordered">
    <tbody>
    <tr><th><?php echo $curLabels['id']; ?></th><td><?php echo $model->id; ?></td></tr>
    <tr><th><?php echo $curLabels['categories_name']; ?></th><td><?php echo $model->categories_name; ?></td></tr>
    <tr><th><?php echo $curLabels['name']; ?></th><td><?php echo $model->name; ?></td></tr>
    <tr><th><?php echo $curLabels['price']; ?></th><td><?php echo $model->price; ?></td></tr>
    <tr><th><?php echo $curLabels['duration']; ?></th><td><?php echo Helpers::date_reformat($model->duration, 'H:i', 'H:i:s'); ?></td></tr>
    <tr><th><?php echo $curLabels['created_by_name']; ?></th><td><?php echo $model->created_by_name; ?></td></tr>
    <tr><th><?php echo $curLabels['created_at']; ?></th><td><?php echo Helpers::date_reformat($model->created_at); ?></td></tr>
    </tbody>
</table>



