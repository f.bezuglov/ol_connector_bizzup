<?php
/* @var application\models\joins\LogsJoins $model */
use application\models\joins\LogsJoins;

$viewTableProperties = LogsJoins::getViewTableProperties();
unset($viewTableProperties['id'], $viewTableProperties['member_id']);
?>
<p>
    <a class="btn btn-danger row-delete" data-id="<?php echo $model->id; ?>">Удалить</a>
</p>
<table class="table table-striped table-bordered">
    <tbody>
    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
        <tr><th><?php echo $propertyParam['label'] ?? $propertyName; ?></th><td><?php echo $model->$propertyName; ?></td></tr>
    <?php endforeach; ?>
    </tbody>
</table>
<input type="hidden" name="url" value="<?php echo __URL_PREFIX__; ?>/logs" />
