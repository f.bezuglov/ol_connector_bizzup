<?php
/**
 * @var application\models\joins\LogsJoins[] $models
 * @var $params array
 * @var $selectValues array
 * @var \application\lib\Pagination $pagination
 */

$this->title = 'Записи логов';
$this->params['breadcrumbs'][] = $this->title;

use application\models\joins\LogsJoins;

$paginationHeader    = $pagination->getHeader();
$paginationFooter    = $pagination->getFooter();
$viewTableProperties = LogsJoins::getViewTableProperties();
?>
<div class="row mb-4">
    <div class="col-sm-12">
        <div class="float-left">
            <div class="btn-group">
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/logs/index" title="Сбросить фильтры"><span class="ion ion-refresh"></span></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/logs/export" title="Экспорт данных в Excel" target="_blank"><i class="ion ion-log-out"></i> Экспорт</a>
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/logs/index<?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? '' : '?togdata=all'; ?>" title="Показать все записи"><i class='ion ion-arrow-resize'></i> <?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? 'Страница' : 'Все'; ?></a>
            </div>
            <div class="btn-group">
                <button class="btn btn-default rows-delete" title="Удалить все записи без возможности восстановления"><i class="ion ion-trash-a"></i> Удалить все записи</button>
            </div>
        </div>
        <div class="float-right">
            <div class="summary"><?php echo $paginationHeader; ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive grid-container">
            <table class="table table-striped table-bordered">
                <thead>
                <tr class="sorts">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                    <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <th style="<?php echo !empty($propertyParam['style']) ? $propertyParam['style'] : 'width:auto;'; ?>">
                            <a class="pjax-link" data-sort-direct="<?php echo (!empty($params) && $params['sort']['name'] == $propertyName && $params['sort']['direct'] == "asc") ? "desc" : "asc"; ?>" data-sort="<?php echo $propertyName; ?>" href="#"><?php echo $propertyParam['label'] ?? $propertyName; ?></a>
                        </th>
                    <?php endforeach; ?>
                    <th style="min-width:70px;" rowspan="2">&nbsp;</th>
                </tr>
                <tr class="filters">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                        <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                    <?php if ($propertyName == 'member_id') : ?>
                    <td>
                        <select class="form-control" name="member_id">
                            <option value=""></option>
                            <?php foreach ($selectValues['member_name'] as $membersName) : ?>
                                <option value="<?php echo $membersName->member_id; ?>"<?php echo !empty($params['member_id']) && $params['member_id'] == $membersName->id ? ' selected="selected"' : ''; ?>><?php echo $membersName->member_name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <?php else : ?>
                        <td>
                            <input type="text" class="form-control" name="<?php echo $propertyName; ?>" value="<?php echo !empty($params[$propertyName]) ? $params[$propertyName] : ''; ?>">
                        </td>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody id="pjax-container">
                <?php if (empty($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 1; ?>">Логов нет</td>
                    </tr>
                <?php elseif (is_string($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 1; ?>"><?php echo $models; ?></td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($models as $model) : ?>
                        <tr id="<?php echo $model->id; ?>" class="data-row" data-id="<?php echo $model->id; ?>">
                            <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                                <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                                <td><?php echo $model->$propertyName; ?></td>
                            <?php endforeach; ?>
                            <td>
                                <div>
                                    <a href="#" title="Удалить" class="row-delete" data-id="<?php echo $model->id; ?>">
                                        <i class="fa fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix pagination">
            <?php echo $paginationFooter; ?>
        </div>
    </div>
</div>

<div class="modal fade text-sm" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div></div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>" />
<input type="hidden" name="url-postfix" value="/logs" />
