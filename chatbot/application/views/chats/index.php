<?php
/**
 * @var application\models\Chats[] $models
 * @var array $params
 * @var \application\models\Authentications $authModel
 * @var \application\lib\Pagination $pagination
 */

$this->title                             = 'Список чатов';
$this->params['breadcrumbs'][0]['url']   = __URL_PREFIX__ . '/clients/index';
$this->params['breadcrumbs'][0]['label'] = 'Клиенты';
$this->params['breadcrumbs'][1]          = $this->title;

use application\models\Chats;

$paginationHeader    = $pagination->getHeader();
$paginationFooter    = $pagination->getFooter();
$viewTableProperties = Chats::getViewTableProperties();
?>
<div class="row mb-4">
    <div class="col-sm-12">
        <h6>Клиент: <?php echo $authModel->member_name ?? 0; ?></h6>
    </div>
</div>
<div class="row mb-4">
    <div class="col-sm-12">
        <div class="float-left">
            <div class="btn-group">
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/chats/index" title="Сбросить фильтры"><span class="ion ion-refresh"></span></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/chats/export" title="Экспорт данных в Excel" target="_blank"><i class="ion ion-log-out"></i> Экспорт</a>
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/chats/index<?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? '' : '?togdata=all'; ?>" title="Показать все записи"><i class='ion ion-arrow-resize'></i> <?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? 'Страница' : 'Все'; ?></a>
            </div>
            <div class="btn-group">
                <button class="btn btn-default rows-delete" title="Удалить все записи без возможности восстановления"><i class="ion ion-trash-a"></i> Удалить все записи</button>
            </div>
            <div class="btn-group">
                <button id="test-add-chat" class="btn btn-default" title="Выполнить тестовый запрос ONIMBOTJOINCHAT"><i class="ion ion-arrow-expand"></i> Тест ONIMBOTJOINCHAT</button>
            </div>
        </div>
        <div class="float-right">
            <div class="summary"><?php echo $paginationHeader; ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive grid-container">
            <table class="table table-striped table-bordered table-striped-not-hidden-tr">
                <thead>
                <tr class="sorts">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                    <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <th style="<?php echo !empty($propertyParam['style']) ? $propertyParam['style'] : 'width:auto;'; ?>">
                            <a class="pjax-link" data-sort-direct="<?php echo (!empty($params) && $params['sort']['name'] == $propertyName && $params['sort']['direct'] == "asc") ? "desc" : "asc"; ?>" data-sort="<?php echo $propertyName; ?>" href="#"><?php echo $propertyParam['label'] ?? $propertyName; ?></a>
                        </th>
                    <?php endforeach; ?>
                    <th class="" title="Развернуть всё" style="min-width:50px;" rowspan="2">
                        <button type="button" class="btn btn-tool btn-all-expand state-all-collapsed" title="Показать все сообщения">
                            <i class="ion ion-chevron-down"></i>
                        </button>
                    </th>
                    <th style="min-width:70px;" rowspan="2">&nbsp;</th>
                </tr>
                <tr class="filters">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                        <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <td>
                            <input type="text" class="form-control" name="<?php echo $propertyName; ?>" value="<?php echo !empty($params[$propertyName]) ? $params[$propertyName] : ''; ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody id="pjax-container">
                <?php if (empty($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 2; ?>">Чатов нет</td>
                    </tr>
                <?php elseif (is_string($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 2; ?>"><?php echo $models; ?></td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($models as $model) : ?>
                        <tr id="<?php echo $model->id; ?>" class="data-row" data-id="<?php echo $model->id; ?>" data-bot-id="<?php echo $model->bot_id; ?>" data-chat-id="<?php echo $model->chat_id; ?>">
                            <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                                <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                                <td><?php echo $model->getStr($propertyName); ?></td>
                            <?php endforeach; ?>
                            <td title="Messages">
                                <button type="button" class="btn btn-tool btn-expand state-collapsed" title="Messages">
                                    <i class="ion ion-chevron-down"></i>
                                </button>
                            </td>
                            <td>
                                <div>
                                    <a href="#" title="Тест ONIMBOTMESSAGEADD" class="test-add-chat-message mr-1">
                                        <i class="fa fa-expand-arrows-alt"></i>
                                    </a>
                                    <a href="#" title="Delete" class="row-delete" data-id="<?php echo $model->id; ?>">
                                        <i class="fa fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="expand-row hidden"><td colspan="<?php echo count($viewTableProperties) + 2; ?>"></td></tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix pagination">
            <?php echo $paginationFooter; ?>
        </div>
    </div>
</div>

<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>" />
<input type="hidden" name="url-postfix" value="/chats" />
<input type="hidden" name="member_id" value="<?php echo $authModel->member_id; ?>" />
<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>

