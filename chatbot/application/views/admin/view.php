<?php
/* @var $model application\models\Users */
use application\models\Users;

$viewTableProperties = Users::getViewTableProperties();
?>
<p>
    <a class="btn btn-danger row-delete" data-id="<?php echo $model->id; ?>">Удалить</a>
</p>
<table class="table table-striped table-bordered">
    <tbody>
    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
        <tr><th><?php echo $propertyParam['label'] ?? $propertyName; ?></th><td><?php echo $model->$propertyName; ?></td></tr>
    <?php endforeach; ?>
    </tbody>
</table>
<input type="hidden" name="url" value="<?php echo __URL_PREFIX__; ?>/logs" />
