<?php

/* @var $model application\models\Users
 *
 */

$this->title                             = 'Добавить пользователя';
$this->params['breadcrumbs'][0]['url']   = __URL_PREFIX__ . '/admin/index';
$this->params['breadcrumbs'][0]['label'] = 'Пользователи';
$this->params['breadcrumbs'][1]          = $this->title;

if (!is_string($model)) {
    require_once __DIR__ . '/_form.php';
} else {
    echo $model;
}
