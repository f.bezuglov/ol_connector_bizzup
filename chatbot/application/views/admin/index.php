<?php
/**
 * @var application\models\joins\UsersJoins[] $models
 * @var $params array
 * @var $selectValues array
 * @var \application\lib\Pagination $pagination
 */

$this->title                             = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

use application\models\joins\UsersJoins;

$paginationHeader    = $pagination->getHeader();
$paginationFooter    = $pagination->getFooter();
$viewTableProperties = UsersJoins::getViewTableProperties();
?>

<div class="row mb-4">
    <div class="col-sm-12">
        <div class="float-left">
            <div class="btn-group">
                <a class="btn btn-success" href="<?php echo __URL_PREFIX__; ?>/admin/create" title="Добавить"><span class="ion ion-plus"></span></a>
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/admin/index" title="Сбросить фильтры"><span class="ion ion-refresh"></span></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/admin/export" title="Экспорт данных в Excel" target="_blank"><i class="ion ion-log-out"></i> Экспорт</a>
                <a class="btn btn-default" href="<?php echo __URL_PREFIX__; ?>/admin/index<?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? '' : '?togdata=all'; ?>" title="Показать все записи"><i class='ion ion-arrow-resize'></i> <?php echo (!empty($params['togdata']) && $params['togdata'] == 'all') ? 'Страница' : 'Все'; ?>
                </a>
            </div>
        </div>
        <div class="float-right">
            <div class="summary"><?php echo $paginationHeader; ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive grid-container">
            <table class="table table-striped table-bordered">
                <thead>
                <tr class="sorts">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                    <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <th style="<?php echo !empty($propertyParam['style']) ? $propertyParam['style'] : 'width:auto;'; ?>">
                            <a class="pjax-link" data-sort-direct="<?php echo (!empty($params) && $params['sort']['name'] == $propertyName && $params['sort']['direct'] == "asc") ? "desc" : "asc"; ?>" data-sort="<?php echo $propertyName; ?>" href="#"><?php echo $propertyParam['label'] ?? $propertyName; ?></a>
                        </th>
                    <?php endforeach; ?>
                    <th style="min-width:70px;" rowspan="2">&nbsp;</th>
                </tr>
                <tr class="filters">
                    <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                        <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                        <td>
                            <input type="text" class="form-control" name="<?php echo $propertyName; ?>" value="<?php echo !empty($params[$propertyName]) ? $params[$propertyName] : ''; ?>">
                        </td>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody id="pjax-container">
                <?php if (empty($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 1; ?>">Логов нет</td>
                    </tr>
                <?php elseif (is_string($models)): ?>
                    <tr>
                        <td colspan="<?php echo count($viewTableProperties) + 1; ?>"><?php echo $models; ?></td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($models as $model) : ?>
                        <tr id="<?php echo $model->id; ?>" class="data-row" data-id="<?php echo $model->id; ?>">
                            <?php foreach ($viewTableProperties as $propertyName => $propertyParam) : ?>
                                <?php if (isset($propertyParam['onTable']) && $propertyParam['onTable'] === false) { continue; } ?>
                                <td><?php echo $model->$propertyName; ?></td>
                            <?php endforeach; ?>
                            <td>
                                <div>
                                    <a href="<?php echo __URL_PREFIX__; ?>/admin/update?id=<?php echo $model->id; ?>" title="Изменить">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                    <a href="#" title="Удалить" class="row-delete" data-id="<?php echo $model->id; ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    <a href="<?php echo __URL_PREFIX__; ?>/admin/permissions?id=<?php echo $model->id; ?>" title="Разрешения">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix pagination">
            <?php echo $paginationFooter; ?>
        </div>
    </div>
</div>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>/admin" />
<input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>">

