<?php

/* @var application\models\Users $model
 * @var array $sexesList
 * @var array $statusesList
 * @var array $rolesList
 */

use application\models\joins\UsersJoins;

$viewTableProperties = UsersJoins::getViewTableProperties();
$rolesList = $model->getRoleArray();
$sexesList = $model->getSexArray();
$statusesList = $model->getStatusArray();

?>
<form class="form-row pb-4" action="<?php echo $model->isNewRecord ? 'create' : 'update'; ?>" method="post">
    <div class="col-md-12 pr-4">
        <div class="form-group required">
            <label for="name"><?php echo $viewTableProperties['name']['label']; ?></label>
            <input name="name" type="text" class="form-control" id="name" placeholder="Введите имя" value="<?php echo $model->name ?? ''; ?>">
            <div class="help-block"></div>
        </div>
        <div class="form-group<?php echo $model->isNewRecord ? ' required' : ''; ?>">
            <label for="password_hash"><?php echo $viewTableProperties['password_hash']['label']; ?></label>
            <input name="password_hash" type="password" class="form-control" id="password_hash" placeholder="Введите пароль" value="<?php echo $model->password_hash ?? ''; ?>">
            <div class="help-block"></div>
        </div>
        <div class="form-group required">
            <label for="email"><?php echo $viewTableProperties['email']['label']; ?></label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Введите email" value="<?php echo $model->email ?? ''; ?>">
            <div class="help-block"></div>
        </div>
        <div class="form-group required">
            <label for="phone"><?php echo $viewTableProperties['phone']['label']; ?></label>
            <input name="phone" type="text" class="form-control" id="phone" placeholder="Введите телефон" value="<?php echo $model->phone ?? ''; ?>">
            <div class="help-block"></div>
        </div>
        <div class="form-group required">
            <label for="role"><?php echo $viewTableProperties['role']['label']; ?></label>
            <select class="form-control" name="role" id="role" style="width: 100%">
                <option value="" <?php echo empty($model->role) ? 'selected=""' : ''; ?>><?php echo 'Выберите права'; ?></option>
                <?php foreach ($rolesList as $key => $name) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (!empty($model->role) && $model->role == $name) ? 'selected=""' : ''; ?>><?php echo $name; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group required">
            <label for="sex"><?php echo $viewTableProperties['sex']['label']; ?></label>
            <select class="form-control" name="sex" id="sex" style="width: 100%">
                <option value="" <?php echo empty($model->sex) ? 'selected=""' : ''; ?>><?php echo 'Выберите пол'; ?></option>
                <?php foreach ($sexesList as $key => $name) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (!empty($model->sex) && $model->sex == $name) ? 'selected=""' : ''; ?>><?php echo $name; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group required">
            <label for="status"><?php echo $viewTableProperties['status']['label']; ?></label>
            <select class="form-control" name="status" id="status" style="width: 100%">
                <option value="" <?php echo empty($model->status) ? 'selected=""' : ''; ?>><?php echo 'Выберите статус'; ?></option>
                <?php foreach ($statusesList as $key => $name) : ?>
                    <option value="<?php echo $key; ?>" <?php echo (!empty($model->status) && $model->status == $name) ? 'selected=""' : ''; ?>><?php echo $name; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-<?php echo $model->isNewRecord ? 'success' : 'primary'; ?>">
            <?php echo $model->isNewRecord ? 'Добавить' : 'Изменить'; ?>
        </button>
    </div>
    <input type="hidden" name="id" value="<?php echo $model->id ?? ''; ?>" />
    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
</form>
<input type="hidden" name="url-prefix" value="<?php echo __URL_PREFIX__; ?>/admin" />
