<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Страница не найдена</title>
        <link href="<?php echo __URL_PREFIX__; ?>/css/errors.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <p><b>Ошибка 404</b>. Страница не найдена</p>
            <p><a href="<?php echo __URL_PREFIX__; ?>/">Перейти на главную</a></p>
        </div>
    </body>
</html>