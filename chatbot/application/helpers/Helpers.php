<?php

namespace application\helpers;

use DateTime;

class Helpers
{

    const PATTERN_NODIR = 1;
    const PATTERN_ENDSWITH = 4;
    const PATTERN_MUSTBEDIR = 8;
    const PATTERN_NEGATIVE = 16;
    const PATTERN_CASE_INSENSITIVE = 32;

    /**
     * Функция-Переводчик
     * @param string $name
     * @param int $count количество для множественных окончаний
     * @return string
     */
    public static function translate($name)
    {

        if (!empty(__SYS_LANG_DEFAULT__)) {
            $path = __DIR__ . '/../messages/'.__SYS_LANG_DEFAULT__.'.php';
            if (file_exists($path)) {
                require_once $path;
            }
            $res = isset($GLOBALS['lang'][$name]) ? $GLOBALS['lang'][$name] : $name;
        } else {
            $res = $name;
        }

        return $res;
    }

    /**
     * Форматирует дату из одного формата в другой ( по умолчанию из Y-m-d в формат d.m.Y )
     * @param string $dateSource = 2016-05-25
     * @param string $formatTo = 'd.m.Y'
     * @param string $formatFrom = 'Y-m-d'
     * @return string
     */
    public static function date_reformat($dateSource, $formatTo = 'd.m.Y H:i:s', $formatFrom = 'Y-m-d H:i:s.u')
    {
        $dateResult = DateTime::createFromFormat($formatFrom, $dateSource);
        if(!$dateResult){
            return '';
        }

        if (strpos($formatTo, '%monthOfFull%') !== FALSE ){
            $formatToArr = explode('%monthOfFull%', $formatTo);
            foreach( $formatToArr as $key => $val ){ $formatToArr[ $key ] = $dateResult->format( $val ); }
            $res = implode( self::translate('month_of_'.$dateResult->format('m').'_full'), $formatToArr );
        } elseif (strpos($formatTo, '%monthOf%') !== FALSE ){
            $formatToArr = explode('%monthOf%', $formatTo);
            foreach( $formatToArr as $key => $val ){ $formatToArr[ $key ] = $dateResult->format( $val ); }
            $res = implode( self::translate('month_of_'.$dateResult->format('m')), $formatToArr );
        } else {
            $res = $dateResult->format( $formatTo );
        }
        return $res;
    }

    public static function money_reformat($money = '')
    {
        return !empty($money) ? number_format($money, 2, '.', ' ') . ' &#8376;' : '';
    }

    public static function type_reformat($type, $polarity)
    {
        $result = '';
        if ($polarity == '+') {
            $result .= '<nobr><span class="fa fa-plus-circle icon-plus" style="color: green;"></span> ' . $type . '</nobr>';
        }
        if ($polarity == '-') {
            $result .= '<nobr><span class="fa fa-minus-circle icon-minus" style="color: ;"></span> ' . $type . '</nobr>';
        }
        return $result;
    }

    /**
     * Преобразует дату согласно часовому поясу
     *
     * @param string $dateSource = 2016-05-25
     * @param string $formatFrom = 'Y-m-d'
     * @param string $formatTo = 'Y-m-d'
     * @param \DateTimeZone $tz
     * @return string
     */
    public static function date_convert($dateSource, $formatFrom = 'Y-m-d H:i:s', $formatTo = 'Y-m-d H:i:s', $tz = NULL)
    {
        $dateResult = \DateTime::createFromFormat($formatFrom, $dateSource, $tz);
        if (!$dateResult) {
            return null;
        }

        if (strpos($formatTo, '%monthOfFull%') !== FALSE ){
            $formatToArr = explode('%monthOfFull%', $formatTo);
            foreach( $formatToArr as $key => $val ){ $formatToArr[ $key ] = $dateResult->format( $val ); }
            $res = implode( self::translate('month_of_'.$dateResult->format('m').'_full'), $formatToArr );
        } elseif (strpos($formatTo, '%monthOf%') !== FALSE ){
            $formatToArr = explode('%monthOf%', $formatTo);
            foreach( $formatToArr as $key => $val ){ $formatToArr[ $key ] = $dateResult->format( $val ); }
            $res = implode( self::translate('month_of_'.$dateResult->format('m')), $formatToArr );
        } else {
            $res = $dateResult->format( $formatTo );
        }
        return $res;
    }


    /**
     * Convert a string from one encoding to another encoding
     * and remove invalid bytes sequences.
     *
     * @param string $string to convert
     * @param string $to encoding you want the string in
     * @param string $from encoding that string is in
     * @return string
     */
    public static function encode($string, $to = 'UTF-8', $from = 'UTF-8')
    {
        // ASCII is already valid UTF-8
        if($to == 'UTF-8' AND self::is_ascii($string))
        {
            return $string;
        }

        // Convert the string
        return @iconv($from, $to . '//TRANSLIT//IGNORE', $string);
    }

    /**
     * Tests whether a string contains only 7bit ASCII characters.
     *
     * @param string $string to check
     * @return bool
     */
    private static function is_ascii($string)
    {
        return ! preg_match('/[^\x00-\x7F]/S', $string);
    }


    /**
     * Проверяет: присутствует ли подстрока в массиве $msg_samples в строке $message
     *
     * @param string $message
     * @param array $msg_samples
     * @return bool
     */
    public static function is_contains_message($message, array $msg_samples)
    {
        foreach ($msg_samples as $sample) {
            if (mb_strpos($message, $sample) !== false)
                return true;
        }

        return false;
    }



}
