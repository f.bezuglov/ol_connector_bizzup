<?php

namespace application\models;

use application\core\Model;
use application\helpers\Helpers;

/**
 * This is the model class for table "chats".
 *
 * @property integer $id
 * @property string $member_id
 * @property integer $bot_id
 * @property integer $chat_id
 * @property string $chat_type
 * @property integer $user_id
 * @property string $message_type
 * @property string $chat_author_id
 * @property string $chat_entity_type
 * @property integer $access_history
 * @property string $dialog_id
 * @property integer $language
 * @property integer $details
 * @property integer $is_workflow_start
 * @property integer $is_transfer_session
 * @property integer $is_operator_answer
 * @property integer $is_close
 * @property string $updated_at
 * @property string $created_at
 */
class Chats extends Model
{

    protected $id, $member_id, $bot_id, $chat_id, $chat_type, $user_id, $message_type, $chat_author_id, $chat_entity_type,
        $access_history, $dialog_id, $language, $details, $is_workflow_start, $is_transfer_session, $is_operator_answer,
        $is_close, $updated_at, $created_at;

    const IS_WORKFLOW_START_FALSE = 1;
    const IS_WORKFLOW_START_TRUE = 2;

    const IS_TRANSFER_SESSION_FALSE = 1;
    const IS_TRANSFER_SESSION_TRUE = 2;

    const IS_OPERATOR_ANSWER_FALSE = 1;
    const IS_OPERATOR_ANSWER_TRUE = 2;

    const IS_CLOSE_FALSE = 1;
    const IS_CLOSE_TRUE = 2;

    public static function tableName()
    {
        return 'chats';
    }

    public function rules()
    {
        return [
            [['member_id', 'chat_id'], 'required'],
            [['chat_id', 'bot_id', 'user_id', 'chat_author_id', 'access_history', 'is_workflow_start', 'is_transfer_session', 'is_operator_answer', 'is_open'], 'integer'],
            [['member_id', 'chat_type', 'message_type', 'chat_entity_type', 'dialog_id', 'details'], 'string'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres()
    {
        $result = [];

        $variables = $this->modelToArray();

        foreach ($variables as $variableName => $variable) {
            if (empty($this->$variableName)) { continue; }
            $result[] = ['=', self::tableName() . '.' . $variableName, $this->$variableName];
        }

        return $result;
    }


    public static function getViewTableProperties()
    {
        $result = [];

        $object    = new Chats();
        $variables = $object->modelToArray();
        unset($variables['member_id']);

        foreach ($variables as $variableName => $variable) {
            switch ($variableName) {
                case 'id':
                    $result[$variableName] = [
                        'style' => 'width:2%;',
                        'label' => '#',
                    ];
                    break;
                case 'chat_type':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'user_id':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'message_type':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'chat_author_id':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'access_history':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'dialog_id':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'language':
                    $result[$variableName] = [
                        'onTable' => false,
                    ];
                    break;
                case 'details':
                    $result[$variableName] = [
                        'onTable' => false,
                        'label' => 'Подробнее',
                    ];
                    break;
                case 'updated_at':
                    $result[$variableName] = [
                        'onTable' => false,
                        'label' => 'Изменено',
                    ];
                    break;
                case 'created_at':
                    $result[$variableName] = [
                        'onTable' => false,
                        'label' => 'Создано',
                    ];
                    break;
                default:
                    $result[$variableName] = [
                        'label' => $variableName,
                    ];
            }
        }
        return $result;
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'is_workflow_start':
                return (empty($this->is_workflow_start) || $this->is_workflow_start == self::IS_WORKFLOW_START_FALSE) ?  self::IS_WORKFLOW_START_FALSE : self::IS_WORKFLOW_START_TRUE;
                break;
            case 'is_transfer_session':
                return (empty($this->is_transfer_session) || $this->is_transfer_session == self::IS_TRANSFER_SESSION_FALSE) ? self::IS_TRANSFER_SESSION_FALSE : self::IS_TRANSFER_SESSION_TRUE;
                break;
            case 'is_operator_answer':
                return (empty($this->is_operator_answer) || $this->is_operator_answer == self::IS_OPERATOR_ANSWER_FALSE) ? self::IS_OPERATOR_ANSWER_FALSE : self::IS_OPERATOR_ANSWER_TRUE;
                break;
            case 'is_close':
                return (empty($this->is_close) || $this->is_close == self::IS_CLOSE_FALSE) ? self::IS_CLOSE_FALSE : self::IS_CLOSE_TRUE;
                break;
            default:
                return parent::__get($property);
        }
    }

    public function getStr($property) {
        switch ($property)
        {
            case 'is_workflow_start':
                return !empty($this->is_workflow_start) ? ($this->is_workflow_start == self::IS_WORKFLOW_START_FALSE ? 'false' : 'true') : 'empty';
                break;
            case 'is_transfer_session':
                return !empty($this->is_transfer_session) ? ($this->is_transfer_session == self::IS_TRANSFER_SESSION_FALSE ? 'false' : 'true') : 'empty';
                break;
            case 'is_operator_answer':
                return !empty($this->is_operator_answer) ? ($this->is_operator_answer == self::IS_OPERATOR_ANSWER_FALSE ? 'false' : 'true') : 'empty';
                break;
            case 'is_close':
                return !empty($this->is_close) ? ($this->is_close == self::IS_CLOSE_FALSE ? 'false' : 'true') : 'empty';
                break;
            default:
                return $this->$property;
        }
    }

    public function loadFromPost($arr) {
        $params = array_change_key_case($arr['data']['PARAMS'], CASE_LOWER);
        $this->details = print_r($arr, true);
        return parent::loadFromPost($params);
    }

    public function setDefaultValues() {
        $this->is_workflow_start = Chats::IS_WORKFLOW_START_FALSE;
        $this->is_transfer_session = Chats::IS_TRANSFER_SESSION_FALSE;
        $this->is_operator_answer = Chats::IS_OPERATOR_ANSWER_FALSE;
        $this->is_close = Chats::IS_CLOSE_FALSE;
    }

}