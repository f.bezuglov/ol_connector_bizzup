<?php

namespace application\models;

use application\core\Model;

/**
 * This is the model class for table "authentications".
 *
 * @property integer $id
 * @property string $member_id
 * @property string $member_name
 * @property integer $bot_id
 * @property string $access_token
 * @property string $refresh_token
 * @property string $application_token
 * @property integer $expires_in
 * @property string $domain
 * @property string $client_id
 * @property string $client_secret
 * @property string $lang
 * @property string $updated_at
 * @property string $created_at
 */
class Authentications extends Model {

    protected $id, $member_id, $member_name, $bot_id, $access_token, $refresh_token, $application_token,
        $expires_in, $domain, $client_id, $client_secret, $lang, $updated_at, $created_at;

    public static function tableName()
    {
        return 'authentications';
    }

    public function rules()
    {
        return [
            [['member_name', 'member_id', 'domain'], 'required'],
            [['expires_in', 'bot_id'], 'integer'],
            [['member_id', 'client_id', 'client_secret', 'member_name', 'access_token', 'refresh_token', 'application_token', 'domain', 'lang'], 'string'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres() {
        return [
            ['=', 'authentications.id', $this->id],
            ['=', 'authentications.member_id', $this->member_id],
            ['=', 'authentications.member_name', $this->member_name],
            ['=', 'authentications.bot_id', $this->bot_id],
            ['=', 'authentications.access_token', $this->access_token],
            ['=', 'authentications.refresh_token', $this->refresh_token],
            ['=', 'authentications.application_token', $this->application_token],
            ['=', 'authentications.expires_in', $this->expires_in],
            ['=', 'authentications.domain', $this->domain],
            ['=', 'authentications.lang', $this->lang],
            ['=', 'authentications.client_id', $this->client_id],
            ['=', 'authentications.client_secret', $this->client_secret],
            ['=', 'authentications.updated_at', $this->updated_at],
            ['=', 'authentications.created_at', $this->created_at],
        ];
    }

    public static function getViewTableProperties()
    {
        return
            [
                'id'              =>
                    [
                        'style' => 'width:2%;',
                        'label' => '#',
                    ],
                'member_id'              =>
                    [
                        'label' => 'member_id',
                    ],
                'member_name'     =>
                    [
                        'label' => 'Наименование',
                    ],
                'bot_id'  =>
                    [
                        'label' => 'ID бота в битриксе',
                    ],
                'domain'             =>
                    [
                        'label' => 'Домен',
                    ],
                'access_token'     =>
                    [
                        'label' => 'access_token',
                    ],
                'refresh_token'     =>
                    [
                        'label' => 'refresh_token',
                    ],
                'application_token' =>
                    [
                        'label' => 'application_token',
                    ],
                'expires_in' =>
                    [
                        'label' => 'expires_in',
                    ],
                'client_id'              =>
                    [
                        'label' => 'client_id',
                    ],
                'client_secret'   =>
                    [
                        'label' => 'client_secret',
                    ],
                'updated_at'              =>
                    [
                        'label' => 'Время обновления',
                    ],
                'created_at'   =>
                    [
                        'label' => 'Время создания',
                    ],
            ];
    }

    public static function checkAuthMemberIdExist($member_id)
    {
        if (empty($member_id) || !is_string($member_id)) {
            return false;
        }
        $searchModel            = new Authentications();
        $searchModel->member_id = $member_id;
        $model                  = $searchModel->selectRow();
        return !empty($model->member_id);
    }

    /**
     * @throws \Exception
     */
    public static function loadAuthToSessions($memberId) {
        $_SESSION['auth'] = [];
        if (empty($memberId)) {
            throw new \Exception('Отсутствует member_id');
        }
        $searchModel = new Authentications();
        $searchModel->member_id = $memberId;
        $model = $searchModel->selectRow();
        if (empty($model->id)) {
            throw new \Exception('Клиент не найден');
        }
        $_SESSION['auth'][$memberId] = $model->modelToArray();
        $_SESSION['auth']['member_id'] = $memberId;
    }

    public function checkConfigsExist() {
        if (empty($this->member_id)) {
            return false;
        }
        $searchModel = new Configs();
        $searchModel->member_id = $this->member_id;
        $model = $searchModel->selectRow();

        return !empty($model->id);

    }

    public function checkIntegrationSession($arr) {

        if ($arr['member_id'] == $_SESSION['auth']['member_id'] && $arr['mis_number'] == $_SESSION['auth']['mis_number']) {
            $this->member_id = $arr['member_id'];
            return $this->checkConfigsExist();
        } else {
            return false;
        }
    }

    public function updateRowTokens()
    {
        if (empty($this->member_id)) {
           throw new \Exception('Отсутствует member_id');
        }

        $this->sqlParamsArr = [
            'member_id' => $this->member_id,
            'access_token' => $this->access_token,
            'refresh_token' => $this->refresh_token,
            'expires_in' => $this->expires_in,
        ];

        $this->sqlStr = "UPDATE authentications SET access_token=:access_token, refresh_token=:refresh_token, expires_in=:expires_in, updated_at=current_timestamp WHERE member_id=:member_id";

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

}