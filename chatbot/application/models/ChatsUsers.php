<?php

namespace application\models;

use application\core\Model;

/**
 * This is the model class for table "chats_messages".
 *
 * @property integer id
 * @property string member_id
 * @property integer user_id
 * @property string name
 * @property string first_name
 * @property string last_name
 * @property string work_position
 * @property string gender
 * @property string is_bot
 * @property string is_connector
 * @property string is_network
 * @property string is_extranet
 * @property integer is_operator
 * @property string updated_at
 * @property string created_at
 */
class ChatsUsers extends Model
{

    protected $id, $member_id, $user_id, $name, $first_name, $last_name, $work_position, $gender, $is_bot,
        $is_connector, $is_network, $is_extranet, $is_operator, $updated_at, $created_at;

    const IS_OPERATOR_FALSE = 1;
    const IS_OPERATOR_TRUE = 2;

    public static function tableName()
    {
        return 'chats_users';
    }

    public function rules()
    {
        return [
            [['member_id', 'user_id'], 'required'],
            [['user_id', 'is_operator'], 'integer'],
            [['member_id', 'name', 'first_name', 'last_name', 'work_position', 'gender', 'is_bot', 'is_connector', 'is_network', 'is_extranet'], 'string'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres()
    {
        $result = [];

        $variables = $this->modelToArray();

        foreach ($variables as $variableName => $variable) {
            if (empty($this->$variableName)) { continue; }
            $result[] = ['=', self::tableName() . '.' . $variableName, $this->$variableName];
        }

        return $result;
    }


    public static function getViewTableProperties()
    {
        $result = [];

        $object    = new Chats();
        $variables = $object->modelToArray();
        unset($variables['member_id'], $variables['details'], $variables['updated_at'], $variables['created_at']);

        foreach ($variables as $variableName => $variable) {
            switch ($variableName) {
                case 'id':
                    $result[$variableName] = [
                        'style' => 'width:2%;',
                        'label' => '#',
                    ];
                    break;
                default:
                    $result[$variableName] = [
                        'label' => $variableName,
                    ];
            }
        }
        return $result;
    }

    public function loadFromPost($arr) {
        $params = array_change_key_case($arr['data']['USER'], CASE_LOWER);
        return parent::loadFromPost($params);
    }

    public static function checkUserIsOperator($id) {
        try {
            $chatsUserSearchModel     = new self();
            $chatsUserSearchModel->id = $id;
            $chatsUserModel           = $chatsUserSearchModel->selectRow();
            if ($chatsUserSearchModel->isError) {
                throw new \Exception($chatsUserSearchModel->sqlMessage);
            }
            if (empty($chatsUserModel->id)) {
                throw new \Exception('Пользователь не найден');
            }

            return $chatsUserModel->is_operator === self::IS_OPERATOR_TRUE;
        } catch (\Exception $ex) {
            throw $ex;
        }

    }

}