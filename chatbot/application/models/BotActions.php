<?php

namespace application\models;

use application\core\Model;
use application\helpers\Helpers;
use application\lib\Bitrix24;
use Exception;


/**
 * This is the model class for table "bot_actions".
 *
 * @property integer $id
 * @property string $member_id
 * @property integer $chat_messages_id
 * @property string $action
 * @property string $details
 * @property string $result
 * @property string $updated_at
 * @property string $created_at
 *
 * @property ChatsMessages $chatsMessagesModel
 * @property Chats $chatsModel
 * @property Configs $configsModel
 */
class BotActions extends Model
{

    protected $id, $member_id, $chat_messages_id, $action, $details, $result, $updated_at, $created_at;
    public $chatsMessagesModel, $chatsModel, $configsModel;

    public static function tableName()
    {
        return 'bot_actions';
    }

    public function rules()
    {
        return [
            [['member_id', 'chat_messages_id'], 'required'],
            [['chat_messages_id'], 'integer'],
            [['member_id', 'action', 'details', 'result'], 'string'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres()
    {
        $result = [];

        $variables = $this->modelToArray();

        foreach ($variables as $variableName => $variable) {
            if (empty($this->$variableName)) {
                continue;
            }
            $result[] = ['=', self::tableName() . '.' . $variableName, $this->$variableName];
        }

        return $result;
    }


    public static function getViewTableProperties()
    {
        $result = [];

        $object = new BotActions();
        $variables = $object->modelToArray();
        unset($variables['member_id'], $variables['chatsMessagesModel'], $variables['chatsModel'], $variables['configsModel']);

        foreach ($variables as $variableName => $variable) {
            switch ($variableName) {
                case 'id':
                    $result[$variableName] = [
                        'style' => 'width:2%;',
                        'label' => '#',
                    ];
                    break;
                default:
                    $result[$variableName] = [
                        'label' => $variableName,
                    ];
            }
        }
        return $result;
    }

    /**Функция обработки входящих сообщений
     *
     * @return void
     * @throws Exception
     */
    public function executeActions()
    {
        $this->action = '';
        $this->details = '';
        $this->result = 'error';
        if ($this->chatsMessagesModel->is_ignore == ChatsMessages::IS_IGNORE_TRUE) {
            $this->executeIgnoreAction();
        }
        if ($this->chatsMessagesModel->is_missed_call == ChatsMessages::IS_MISSED_CALL_TRUE) {
            $this->executeMissedCallAction();
        }
        if ($this->chatsMessagesModel->is_system == ChatsMessages::IS_SYSTEM_TRUE) {
            $this->executeSystemAction();
        }
        if ($this->chatsMessagesModel->is_operator == ChatsMessages::IS_OPERATOR_TRUE) {
            $this->executeOperatorAction();
        }
        if ($this->chatsMessagesModel->is_incoming == ChatsMessages::IS_INCOMING_TRUE) {
            $this->executeIncomingAction();
        }

        $botActionsInsertModel = new BotActions();
        $botActionsInsertModel->member_id = $this->member_id;
        $botActionsInsertModel->chat_messages_id = $this->chat_messages_id;
        $botActionsInsertModel->action = $this->action;
        $botActionsInsertModel->result = $this->result;
        $botActionsInsertModel->details = $this->details;
        $botActionsInsertModel->insertRow();
    }

    /**
     * Функция обработки игнорируемого сообщения (в т.ч. сообщения при отправке ответа на пропущенный звонок)
     *
     * Параметры ChatsMessages
     * 1. is_ignore = true
     * 2. is_missed_call = false
     * 3. is_system = false
     * 4. is_operator = false
     * 5. is_incoming = false
     *
     * @return void
     */
    private function executeIgnoreAction()
    {
        $this->action = 'Игнорируемое сообщение is_ignore = true. Сообщение пропущено.';
        $this->result = 'success';
    }

    /**
     * Функция обработки пропущенного звонка (если звонки для wazzup24 не работают)
     *
     * Параметры ChatsMessages
     * 1. is_ignore = false
     * 2. is_missed_call = true
     * 3. is_system = false
     * 4. is_operator = false
     * 5. is_incoming = false
     *
     * Отправляем автоответ из настроек интерфейса. Если не заполнен, то не отправляем. При установке заполняем текстом по умолчанию из предыдущей версии бота.
     * В начале ответа на пропущенный вызов, независимо от содержания текста клиента добаваляем два смайла, по ним потом идентифицируем сообщение:
     * 🤖☎
     *
     * @return void
     * @throws Exception
     */
    public function executeMissedCallAction()
    {
        // если не заполнено строка сообщения о попущенном звонке в конфигурации то выходим
        if (empty($this->configsModel->is_missed_call_answer_str)) {
            $this->action = 'Пропущенный звонок is_missed_call = true. Сообщение пропущено, так как не заполнена строка ответа в конфигурации';
            $this->result = 'success';
            return;
        }
        // отправляем сообщение клиенту строку из конфигурации и к ней добавляем в начало '🤖☎'
        $bitrix24 = new Bitrix24($this->member_id);

        $answer = $bitrix24->answerForMissedCall($this->chatsMessagesModel->chat_id, '🤖☎' . $this->configsModel->is_missed_call_answer_str);
        $this->action = 'Пропущенный звонок is_missed_call = true. Отправлено сообщение.';

        if (!$bitrix24->isError) {
            $chatsUpdateModel = new Chats();
            $chatsUpdateModel->id = $this->chatsModel->id;
            $chatsUpdateModel->is_workflow_start = Chats::IS_WORKFLOW_START_TRUE;
            $chatsUpdateModel->updateRow();
        }

        $this->details = $bitrix24->method . ': ' . PHP_EOL . print_r($bitrix24->arDataRest, true) . PHP_EOL . 'Answer: ' . print_r($answer, true) . PHP_EOL . 'bitrix message: ' . $bitrix24->sqlMessage;
        $this->result = $bitrix24->isError ? 'error' : 'success';
    }

    /**
     * Функция обработки системного сообщения
     *
     * Параметры ChatsMessages
     * 1. is_ignore = false
     * 2. is_missed_call = false
     * 3. is_system = true
     * 4. is_operator = false
     * 5. is_incoming = false
     *
     *
     * Если параметры Chats
     * 1. is_workflow_start = false
     * 2. is_transfer_session = false
     * 3. is_operator_answer = false
     * 4. is_close = false
     * Не предпринимаем ничего, в лог пишем статусы параметров
     *
     * Если параметры Chats:
     * 1. is_workflow_start = true
     * или
     * 3. is_operator_answer = true
     * Устанавливаем is_workflow_start и is_operator_answer в false, в лог пишем статусы параметров
     *
     *
     * Закрываем диалог только если
     * 2. is_transfer_session = false
     * 3. is_operator_answer = false
     *
     * @return void
     * @throws Exception
     */
    private function executeSystemAction()
    {
        if ($this->chatsModel->is_workflow_start == Chats::IS_WORKFLOW_START_TRUE
            || $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_TRUE) {
            $chatsUpdateModel = new Chats();
            $chatsUpdateModel->id = $this->chatsModel->id;
            $chatsUpdateModel->is_workflow_start = Chats::IS_WORKFLOW_START_FALSE;
            $chatsUpdateModel->is_operator_answer = $this->chatsModel->is_operator_answer = Chats::IS_OPERATOR_ANSWER_FALSE;
            $chatsUpdateModel->updateRow();

            $this->action = 'Системное сообщение is_system = true. Установлены значения is_workflow_start = false, is_operator_answer = false';
            $this->result = 'success';
        } else {
            $this->action = 'Системное сообщение is_system = true. Сообщение пропущено, так как is_workflow_start = false, is_operator_answer = false';
            $this->result = 'success';
        }

        if (!empty($this->configsModel->is_system_autoclose)
            && $this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_FALSE
            && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_FALSE) {
            $bitrix24 = new Bitrix24($this->member_id);
            $answer = $bitrix24->closeDialog($this->chatsMessagesModel->chat_id);
            $this->action .= '. Диалог закрыт.';
        }

    }


    /**
     * Функция обработки сообщения от оператора
     *
     * Параметры ChatsMessages
     * 1. is_ignore = false
     * 2. is_missed_call = false
     * 3. is_system = false
     * 4. is_operator = true
     * 5. is_incoming = false
     *
     * Если параметры Chats
     * 2. is_transfer_session = false
     * 3. is_operator_answer = true или false
     *
     * Выполняем трансфер диалога без запуска БП
     * Устанавливаем is_transfer_session и is_operator_answer в true, в лог пишем статусы параметров и «- параметр operator установлен true»
     *
     *
     * Если параметры Chats
     * 2. is_transfer_session = true
     * 3. is_operator_answer = true или false
     * Устанавливаем is_operator_answer в true, в лог пишем статусы параметров и «- параметр operator установлен true»
     *
     * @return void
     * @throws Exception
     */
    private function executeOperatorAction()
    {
        if ($this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_FALSE) {
            $this->transferDialog();
            $chatsUpdateModel = new Chats();
            $chatsUpdateModel->id = $this->chatsModel->id;
            $chatsUpdateModel->is_operator_answer = Chats::IS_OPERATOR_ANSWER_TRUE;
            $chatsUpdateModel->is_transfer_session = Chats::IS_TRANSFER_SESSION_TRUE;
            $chatsUpdateModel->updateRow();

            $message = 'Сообщение от оператора is_operator = true. ' . $this->action . 'Установлены значения is_operator_answer = true, is_transfer_session = true';
            $this->action = $message;
        } else {
            $chatsUpdateModel = new Chats();
            $chatsUpdateModel->id = $this->chatsModel->id;
            $chatsUpdateModel->is_operator_answer = Chats::IS_OPERATOR_ANSWER_TRUE;
            $chatsUpdateModel->updateRow();

            $this->action = 'Сообщение от оператора is_operator = true. Установлены значения is_operator_answer = true';
            $this->result = 'success';
        }
    }

    /**
     * Функция обработки сообщения от клиента
     *
     * Параметры ChatsMessages
     * 1. is_ignore = false
     * 2. is_missed_call = false
     * 3. is_system = false
     * 4. is_operator = false
     * 5. is_incoming = true
     *
     *
     * Если параметры Chats
     * 1. is_workflow_start = true или false
     * 2. is_transfer_session = true
     * 3. is_operator_answer = true
     * Не предпринимаем ничего, в лог пишем статусы параметров и «сообщение пропущено»
     *
     *
     * Если параметры Chats
     * 1. is_workflow_start = true или false
     * 2. is_transfer_session = false
     * 3. is_operator_answer = true
     * Выполняем трансфер диалога без запуска БП, в лог пишем статусы параметров и «- передано в ОЛ без запуска БП, - параметр transfer установлен true»
     * Меняем: is_transfer_session - true
     *
     *
     * Если параметры Chats
     * 1. is_workflow_start = true или false
     * 2. is_transfer_session = false
     * 3. is_operator_answer = false
     * Выполняем трансфер диалога, запускаем БП. В лог пишем статусы параметров и «- передано в ОЛ, - запущен БП id …,
     * - параметр transfer установлен true - параметр workflow установлен true»
     * Меняем: is_transfer_session - true, is_workflow_start - true
     *
     *
     * Если параметры Chats
     * 1. is_workflow_start = false
     * 2. is_transfer_session = true
     * 3. is_operator_answer = false
     * Выполняем запуск БП, в лог пишем статусы параметров и «- запущен БП id… - параметр workflow установлен true»
     * Меняем: is_workflow_start - true
     *
     *
     * Если набор параметров отличается от описанных то
     * Не предпринимаем ничего, в лог пишем статусы параметров и «Требуется корректировка действий» в вотсап Столбову отправляем сообщение о критической ошибке в боте (текст с параметрами дам позже)
     *
     * @return void
     * @throws Exception
     */
    private function executeIncomingAction()
    {

        // Если параметры Chats
        // 1. is_workflow_start = true или false
        // 2. is_transfer_session = true
        // 3. is_operator_answer = true
        // Не предпринимаем ничего, в лог пишем статусы параметров и «сообщение пропущено»

        if ($this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_TRUE
            && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_TRUE) {
            $this->action = 'Входящее сообщение is_incoming = true. Сообщение пропущено.';
            $this->result = 'success';
            return;

        } else

            // Если параметры Chats
            // 1. is_workflow_start = true или false
            // 2. is_transfer_session = false
            // 3. is_operator_answer = true
            // Выполняем трансфер диалога без запуска БП, в лог пишем статусы параметров и «- передано в ОЛ без запуска БП, - параметр is_transfer_session установлен true»
            // Меняем: is_transfer_session - true

            if ($this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_FALSE
                && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_TRUE) {
                if ($this->transferDialog()) {
                    $chatsUpdateModel = new Chats();
                    $chatsUpdateModel->id = $this->chatsModel->id;
                    $chatsUpdateModel->is_transfer_session = Chats::IS_TRANSFER_SESSION_TRUE;
                    $chatsUpdateModel->updateRow();
                }
                $message = 'Входящее сообщение is_incoming = true. ' . $this->action . 'Установлены значения is_transfer_session = true';
                $this->action = $message;
            } else

                // Если параметры Chats
                // 1. is_workflow_start = true или false
                // 2. is_transfer_session = false
                // 3. is_operator_answer = false
                // Выполняем трансфер диалога, запускаем БП. В лог пишем статусы параметров и «- передано в ОЛ, - запущен БП id …,
                // - параметр transfer установлен true - параметр workflow установлен true»
                // Меняем: is_transfer_session - true, is_workflow_start - true

                if ($this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_FALSE
                    && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_FALSE) {
                    if ($this->workflowStart() && $this->transferDialog()) {
                        $chatsUpdateModel = new Chats();
                        $chatsUpdateModel->id = $this->chatsModel->id;
                        $chatsUpdateModel->is_workflow_start = Chats::IS_WORKFLOW_START_TRUE;
                        $chatsUpdateModel->is_transfer_session = Chats::IS_TRANSFER_SESSION_TRUE;
                        $chatsUpdateModel->updateRow();
                    }

                    $message = 'Входящее сообщение is_incoming = true. ' . $this->action
                        . ($this->chatsMessagesModel->workflow_type == 'CONTACT' ? ('ID БП для контакта =' . $this->configsModel->tmpl_contact_id) : ('ID БП для лида =' . $this->configsModel->tmpl_lead_id))
                        . '. Установлены значения is_workflow_start = true, is_transfer_session = true';
                    $this->action = $message;
                } else

                    // Если параметры Chats
                    // 1. is_workflow_start = false
                    // 2. is_transfer_session = true
                    // 3. is_operator_answer = false
                    // Выполняем запуск БП, в лог пишем статусы параметров и «- запущен БП id… - параметр workflow установлен true»
                    // Меняем: is_workflow_start - true

                    if ($this->chatsModel->is_workflow_start == Chats::IS_WORKFLOW_START_FALSE
                        && $this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_TRUE
                        && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_FALSE) {
                        if ($this->workflowStart()) {
                            $chatsUpdateModel = new Chats();
                            $chatsUpdateModel->id = $this->chatsModel->id;
                            $chatsUpdateModel->is_workflow_start = Chats::IS_WORKFLOW_START_TRUE;
                            $chatsUpdateModel->updateRow();
                        }

                        $message = 'Входящее сообщение is_incoming = true. ' . $this->action
                            . ($this->chatsMessagesModel->workflow_type == 'CONTACT' ? ('ID БП для контакта =' . $this->configsModel->tmpl_contact_id) : ('ID БП для лида =' . $this->configsModel->tmpl_lead_id))
                            . '. Установлены значения is_workflow_start = true.';
                        $this->action = $message;
                    } else

                        // Если параметры Chats
                        // 1. is_workflow_start = true
                        // 2. is_transfer_session = true
                        // 3. is_operator_answer = false
                        // Неверные значения

                        if ($this->chatsModel->is_workflow_start == Chats::IS_WORKFLOW_START_TRUE
                            && $this->chatsModel->is_transfer_session == Chats::IS_TRANSFER_SESSION_TRUE
                            && $this->chatsModel->is_operator_answer == Chats::IS_OPERATOR_ANSWER_FALSE) {

                            $this->action = 'Неверные значения';
                        }
    }


    /**
     * Функция запуска БП
     *
     * @return bool
     */
    public function workflowStart()
    {
        $bitrix24 = new Bitrix24($this->member_id);

        if (empty($this->configsModel->tmpl_contact_id) && $this->chatsMessagesModel->workflow_type == 'CONTACT') {
            $this->action .= 'Бизнес процесс для контакта отсутствует. ';
            $this->result = 'success';
        }

        if (!empty($this->configsModel->tmpl_contact_id) && $this->chatsMessagesModel->workflow_type == 'CONTACT') {
            $answer = $bitrix24->workflowStartForContact($this->configsModel->tmpl_contact_id, $this->chatsMessagesModel->workflow_contact_id, $this->chatsMessagesModel->message, $this->chatsMessagesModel->chat_id);
            $this->action .= 'Бизнес процесс для контакта запущен. ';

            $this->details = !empty($this->details) ? ($this->details . PHP_EOL . PHP_EOL) : $this->details;
            $this->details .= $bitrix24->method . ': ' . PHP_EOL . print_r($bitrix24->arDataRest, true) . PHP_EOL . 'Answer: ' . print_r($answer, true) . PHP_EOL . 'bitrix message: ' . $bitrix24->sqlMessage;
            $this->result = $bitrix24->isError ? 'error' : 'success';
        }


        if (empty($this->configsModel->tmpl_lead_id) && $this->chatsMessagesModel->workflow_type == 'LEAD') {
            $this->action .= 'Бизнес процесс для лида отсутствует. ';
            $this->result = 'success';
        }

        if (!empty($this->configsModel->tmpl_lead_id) && $this->chatsMessagesModel->workflow_type == 'LEAD') {
            $answer = $bitrix24->workflowStartForLead($this->configsModel->tmpl_lead_id, $this->chatsMessagesModel->workflow_lead_id, $this->chatsMessagesModel->message, $this->chatsMessagesModel->chat_id);
            $this->action .= 'Бизнес процесс для лида запущен. ';

            $this->details = !empty($this->details) ? ($this->details . PHP_EOL . PHP_EOL) : $this->details;
            $this->details .= $bitrix24->method . ': ' . PHP_EOL . print_r($bitrix24->arDataRest, true) . PHP_EOL . 'Answer: ' . print_r($answer, true) . PHP_EOL . 'bitrix message: ' . $bitrix24->sqlMessage;
            $this->result = $bitrix24->isError ? 'error' : 'success';
        }

        return !$bitrix24->isError;
    }

    /**
     * Функция передачи диалога
     *
     *
     * @return bool
     */
    public function transferDialog()
    {
        try {
            $bitrix24 = new Bitrix24($this->member_id);
            $answer = '';
            if ($this->configsModel->towards_send_dialog == Configs::TOWARD_SEND_DIALOG_OPEN_LINE) {
                $answer = $bitrix24->transferDialogOpenLine($this->chatsMessagesModel->chat_id);
                $this->action .= 'Диалог передан в ОЛ. ';
            }
            if ($this->configsModel->towards_send_dialog == Configs::TOWARD_SEND_DIALOG_OPERATOR) {
                $answer = $bitrix24->transferDialogOperator($this->chatsMessagesModel->chat_id, $this->chatsMessagesModel->workflow_type, $this->chatsMessagesModel->workflow_contact_id, $this->chatsMessagesModel->workflow_lead_id);
                $this->action .= 'Диалог передан оператору. ';
            }

            $this->details = !empty($this->details) ? ($this->details . PHP_EOL . PHP_EOL) : $this->details;
            $this->details .= $bitrix24->method . ': ' . PHP_EOL . print_r($bitrix24->arDataRest, true) . PHP_EOL . 'Answer: ' . print_r($answer, true) . PHP_EOL . 'bitrix message: ' . $bitrix24->sqlMessage;
            $this->result = 'success';
            return true;

        } catch (Exception $ex) {
            $this->details .= !empty($this->details) ? ($this->details . PHP_EOL . PHP_EOL . $ex->getMessage()) : $ex->getMessage();
            $this->result = 'error';
            return false;
        }
    }

}