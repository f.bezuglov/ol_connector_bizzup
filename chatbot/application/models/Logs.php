<?php

namespace application\models;

use application\core\Model;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property string $member_id
 * @property string $debug
 * @property string $result
 * @property string $details
 * @property string $class_name
 * @property string $updated_at
 * @property string $created_at
 */
class Logs extends Model {

	protected $id, $member_id, $debug, $result, $details, $class_name, $updated_at, $created_at;

    public function searchInit(){}

    public static function tableName()
    {
        return 'logs';
    }

    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id', 'debug', 'result', 'details', 'class_name'], 'string'],
            [['created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres() {
        return [
            ['=', 'logs.id', $this->id],
            ['=', 'logs.member_id', $this->member_id],
            ['=', 'logs.debug', $this->debug],
            ['=', 'logs.class_name', $this->class_name],
            ['=', 'logs.result', $this->result],
            ['like', 'logs.details', $this->details],
            ['=', 'logs.updated_at', $this->updated_at],
            ['=', 'logs.created_at', $this->created_at],
        ];
    }

    public static function getViewTableProperties() {
        return
            [
                'id' => ['style' => 'width:2%;', 'label' => '#'],
                'member_id' => ['label' => 'Клиент'],
                'result'     => ['label' => 'Результат'],
                'class_name'  => ['label' => 'Имя класса'],
                'details' => ['onTable' => false, 'label' => 'Сообщение'],
                'debug' => ['onTable' => false, 'label' => 'Подробности'],
                'created_at' => ['label' => 'Дата создания'],
            ];
    }

    public static function insertRowLogs($result, $class_name = '', $details = '', $debug = '') {
        $model = new Logs();
        if (empty($debug)) {
            $debug = ($result !== 'success') ? ((debug_backtrace()[1]['class'] ?? '') . ' | ' . (debug_backtrace()[1]['function'] ?? '')) : '';
        }

        $params           = [
            'member_id' => $_SESSION['auth']['member_id'] ?? '',
            'result' => $result,
            'details' => htmlspecialchars($details),
            'class_name' => $class_name,
            'debug' => $debug,
        ];

        $model->sqlStr = "INSERT INTO logs (member_id, class_name, debug, result, details) VALUES (:member_id, :class_name, :debug, :result, :details)";

        $result = $model->dbSet($model->sqlStr, $params);

        return $result;
    }

}