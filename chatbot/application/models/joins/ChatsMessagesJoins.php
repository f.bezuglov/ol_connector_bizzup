<?php

namespace application\models\joins;

use application\models\ChatsMessages;

class ChatsMessagesJoins extends ChatsMessages {

    public function searchInit() {
        $this->sqlSelectStr = "chats_messages.id as id, "
            . "chats_messages.member_id as member_id, "
            . "chats_messages.bot_id as bot_id, "
            . "chats_messages.chat_id as chat_id, "
            . "chats_messages.from_user_id as from_user_id, "
            . "chats_messages.message as message, "
            . "chats_messages.to_chat_id as to_chat_id, "
            . "chats_messages.message_type as message_type, "
            . "chats_messages.system as system, "
            . "chats_messages.skip_command as skip_command, "
            . "chats_messages.skip_connector as skip_connector, "
            . "chats_messages.important_connector as important_connector, "
            . "chats_messages.silent_connector as silent_connector, "
            . "chats_messages.author_id as author_id, "
            . "chats_messages.chat_author_id as chat_author_id, "
            . "chats_messages.chat_entity_type as chat_entity_type, "
            . "chats_messages.chat_entity_id as chat_entity_id, "
            . "chats_messages.chat_entity_data_1 as chat_entity_data_1, "
            . "chats_messages.chat_entity_data_2 as chat_entity_data_2, "
            . "chats_messages.chat_entity_data_3 as chat_entity_data_3, "
            . "chats_messages.command_context as command_context, "
            . "chats_messages.message_original as message_original, "
            . "chats_messages.to_user_id as to_user_id, "
            . "chats_messages.dialog_id as dialog_id, "
            . "chats_messages.message_id as message_id, "
            . "chats_messages.chat_type as chat_type, "
            . "chats_messages.language as language, "
            . "chats_messages.details as details, "
            . "chats_messages.is_bot as is_bot, "
            . "chats_messages.workflow_type as workflow_type, "
            . "chats_messages.workflow_contact_id as workflow_contact_id, "
            . "chats_messages.workflow_lead_id as workflow_lead_id, "
            . "chats_messages.updated_at as updated_at, "
            . "chats_messages.created_at as created_at";

        $this->sqlJoinsStr = ""
        . " LEFT JOIN chats_users ON configs.from_user_id=chats_users.user_id AND configs.member_id=chats_users.member_id";
    }


}
