<?php

namespace application\models\joins;

use application\models\Logs;

class LogsJoins extends Logs {
    public $member_name;

    public function searchInit() {
        $this->sqlSelectStr = "logs.id as id, logs.member_id as member_id, 
        logs.class_name as class_name, logs.debug as debug, logs.result as result,
        logs.details as details, logs.created_at as created_at,        
        authentications.member_name as member_name";

        $this->sqlJoinsStr = "
        LEFT JOIN authentications ON authentications.member_id=logs.member_id         
        ";
    }

    public static function getViewTableProperties() {
        $properties = [
            'member_name' => [
                'onTable' => false,
                'label' => 'Клиент',
            ],
        ];
        return array_merge($properties, parent::getViewTableProperties());
    }

    public function andFilterWheres() {
        $andFilterWheres = [
            ['=', 'authentications.member_name', $this->member_name],
        ];
        return array_merge($andFilterWheres, parent::andFilterWheres());
    }


}
