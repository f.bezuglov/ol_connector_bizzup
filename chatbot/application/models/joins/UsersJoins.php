<?php

namespace application\models\joins;

use application\models\Users;

class UsersJoins extends Users {
    public $created_by_name, $updated_by_name;

    public function searchInit() {
        $this->sqlSelectStr = "users.id as id, users.role as role, users.name as name,
        users.sex as sex, users.percent as percent, users.photo as photo,
        users.specialization as specialization, users.email as email, users.phone as phone,
        users.status as status,
        users.created_by as created_by, users.created_at as created_at,
        created_by.name as created_by_name";

        $this->sqlJoinsStr = "
        LEFT JOIN users as created_by ON created_by.id=users.created_by         
        ";
    }

    public static function getViewTableProperties() {
        $properties = [
            'created_by_name' => [
                'onTable' => false,
                'label' => 'Создатель записи',
                ]
        ];
        return array_merge($properties, parent::getViewTableProperties());
    }

    public function andFilterWheres() {
        $andFilterWheres = [
        ];
        return array_merge($andFilterWheres, parent::andFilterWheres());
    }


}
