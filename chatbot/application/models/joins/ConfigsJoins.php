<?php

namespace application\models\joins;

use application\models\Configs;

class ConfigsJoins extends Configs {
    public $member_name;

    public function searchInit() {
        $this->sqlSelectStr = "configs.id as id, "
            . "configs.member_id as member_id, "
            . "authentications.member_name as member_name, "
            . "configs.towards_send_dialog as towards_send_dialog, "
            . "configs.tmpl_contact_id as tmpl_contact_id, "
            . "configs.tmpl_lead_id as tmpl_lead_id, "
            . "array_to_json(configs.is_ignore_str) as is_ignore_str, "
            . "configs.is_system_autoclose as is_system_autoclose, "
            . "array_to_json(configs.is_system_str) as is_system_str, "
            . "array_to_json(configs.is_operator_str) as is_operator_str, "
            . "array_to_json(configs.is_missed_call_str) as is_missed_call_str, "
            . "configs.is_missed_call_answer_str as is_missed_call_answer_str, "
            . "configs.updated_at as updated_at, "
            . "configs.created_at as created_at";

        $this->sqlJoinsStr = " LEFT JOIN authentications ON authentications.member_id=configs.member_id";
    }

    public static function getViewTableProperties() {
        $viewTableProperties = [
            'member_name' => ['label' => 'Клиент'],
        ];
        return array_merge($viewTableProperties, parent::getViewTableProperties());
    }

    public function andFilterWheres() {
        $andFilterWheres = [
            ['=', 'authentications.member_name', $this->member_name],
        ];
        return array_merge($andFilterWheres, parent::andFilterWheres());
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'is_ignore_str':
                return json_decode($this->is_ignore_str ?? '[]');
                break;
            case 'is_system_str':
                return json_decode($this->is_system_str ?? '[]');
                break;
            case 'is_operator_str':
                return json_decode($this->is_operator_str ?? '[]');
                break;
            case 'is_missed_call_str':
                return json_decode($this->is_missed_call_str ?? '[]');
                break;
            default:
                return parent::__get($property);
        }
    }

}
