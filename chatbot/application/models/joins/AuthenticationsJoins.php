<?php

namespace application\models\joins;

use application\models\Authentications;

class AuthenticationsJoins extends Authentications {
    public $configs_id, $configs_need_update;

    public function searchInit() {
        $this->sqlSelectStr = "authentications.id as id, "
            . "authentications.member_id as member_id, "
            . "authentications.member_name as member_name, "
            . "authentications.bot_id as bot_id, "
            . "authentications.access_token as access_token, "
            . "authentications.refresh_token as refresh_token, "
            . "authentications.application_token as application_token, "
            . "authentications.expires_in as expires_in, "
            . "authentications.client_id as client_id, "
            . "authentications.client_secret as client_secret, "
            . "authentications.domain as domain, "
            . "authentications.lang as lang, "
            . "authentications.updated_at as updated_at, "
            . "authentications.created_at as created_at, "
            . "configs.id as configs_id";

        $this->sqlJoinsStr = " LEFT JOIN configs ON configs.member_id=authentications.member_id";
    }

    public function andFilterWheres() {
        $andFilterWheres = [
            ['=', 'configs.id', $this->configs_id],
            ['=', 'configs.need_update', $this->configs_need_update],
        ];
        return array_merge($andFilterWheres, parent::andFilterWheres());
    }

}
