<?php

namespace application\models;

use application\core\Model;
use application\helpers\Helpers;

/**
 * This is the model class for table "chats_messages".
 *
 * @property integer $id
 * @property string $member_id
 * @property integer $bot_id
 * @property integer $chat_id
 * @property integer $from_user_id
 * @property string $message
 * @property integer $to_chat_id
 * @property string $message_type
 * @property string $system
 * @property string $skip_command
 * @property string $skip_connector
 * @property string $important_connector
 * @property string $silent_connector
 * @property integer $author_id
 * @property integer $chat_author_id
 * @property string $chat_entity_type
 * @property string $chat_entity_id
 * @property string $chat_entity_data_1
 * @property string $chat_entity_data_2
 * @property string $chat_entity_data_3
 * @property string $command_context
 * @property string $message_original
 * @property integer $to_user_id
 * @property string $dialog_id
 * @property integer $message_id
 * @property string $chat_type
 * @property string $language
 * @property string $details
 * @property integer $is_incoming
 * @property integer $is_ignore
 * @property integer $is_system
 * @property integer $is_operator
 * @property integer $is_missed_call
 * @property string $workflow_type
 * @property integer $workflow_contact_id
 * @property integer $workflow_lead_id
 * @property string $updated_at
 * @property string $created_at
 */
class ChatsMessages extends Model
{

    protected $id, $member_id, $bot_id, $chat_id, $from_user_id, $message, $to_chat_id, $message_type, $system,
        $skip_command, $skip_connector, $important_connector, $silent_connector, $author_id, $chat_author_id,
        $chat_entity_type, $chat_entity_id, $chat_entity_data_1, $chat_entity_data_2, $chat_entity_data_3, $command_context,
        $message_original, $to_user_id, $dialog_id, $message_id, $chat_type, $language, $details, $is_incoming,
        $is_ignore, $is_system, $is_operator, $is_missed_call, $workflow_type,
        $workflow_contact_id, $workflow_lead_id, $updated_at, $created_at;

    public $configsModel;

    const IS_INCOMING_FALSE = 1;
    const IS_INCOMING_TRUE  = 2;

    const IS_IGNORE_FALSE = 1;
    const IS_IGNORE_TRUE  = 2;

    const IS_SYSTEM_FALSE = 1;
    const IS_SYSTEM_TRUE  = 2;

    const IS_OPERATOR_FALSE = 1;
    const IS_OPERATOR_TRUE  = 2;

    const IS_MISSED_CALL_FALSE = 1;
    const IS_MISSED_CALL_TRUE  = 2;

    const SUPPORTED_TYPES = ['image', 'audio', 'file'];

    public static function tableName()
    {
        return 'chats_messages';
    }

    public function rules()
    {
        return [
            [['member_id', 'bot_id', 'chat_id'], 'required'],
            [
                [
                    'chat_id', 'bot_id', 'user_id', 'from_user_id', 'to_chat_id', 'author_id', 'chat_author_id', 'to_user_id'
                    , 'message_id', 'is_incoming', 'is_ignore', 'is_system', 'is_operator', 'is_missed_call', 'workflow_contact_id', 'workflow_lead_id'
                ], 'integer'
            ],
            [
                [
                    'member_id', 'message', 'message_type', 'system', 'skip_command', 'skip_connector', 'important_connector'
                    , 'silent_connector', 'chat_entity_type', 'chat_entity_id', 'chat_entity_data_1', 'chat_entity_data_2'
                    , 'chat_entity_data_3', 'command_context', 'message_original', 'dialog_id', 'language', 'details', 'workflow_type'
                ], 'string'
            ],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres()
    {
        $result = [];

        $variables = $this->modelToArray();

        foreach ($variables as $variableName => $variable) {
            if (empty($this->$variableName)) {
                continue;
            }
            $result[] = ['=', self::tableName() . '.' . $variableName, $this->$variableName];
        }

        return $result;
    }

    public static function getViewTableProperties()
    {
        $result = [];

        $object    = new ChatsMessages();
        $variables = $object->modelToArray();
        unset($variables['member_id'], $variables['configsModel']);

        foreach ($variables as $variableName => $variable) {
            switch ($variableName) {
                case 'id':
                    $result[$variableName] = [
                        'onTable' => true,
                        'style'   => 'width:2%;',
                        'label'   => '#',
                    ];
                    break;
                case 'from_user_id':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'message':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'to_chat_id':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'message_id':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'is_ignore':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'is_incoming':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'is_system':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'is_operator':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'is_missed_call':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                case 'workflow_type':
                    $result[$variableName] = [
                        'onTable' => true,
                    ];
                    break;
                default:
                    $result[$variableName] = [
                        'onTable' => false,
                        'label'   => $variableName,
                    ];
            }
        }
        return $result;
    }

    public function __get($property)
    {
        switch ($property) {
            case 'is_incoming':
                return (!empty($this->is_incoming) && $this->is_incoming == self::IS_INCOMING_TRUE) ? self::IS_INCOMING_TRUE : self::IS_INCOMING_FALSE;
                break;
            case 'is_ignore':
                return (!empty($this->is_ignore) && $this->is_ignore == self::IS_IGNORE_TRUE) ? self::IS_IGNORE_TRUE : self::IS_IGNORE_FALSE;
                break;
            case 'is_system':
                return (!empty($this->is_system) && $this->is_system == self::IS_SYSTEM_TRUE) ? self::IS_SYSTEM_TRUE : self::IS_SYSTEM_FALSE;
                break;
            case 'is_operator':
                return (!empty($this->is_operator) && $this->is_operator == self::IS_OPERATOR_TRUE) ? self::IS_OPERATOR_TRUE : self::IS_OPERATOR_FALSE;
                break;
            case 'is_missed_call':
                return (!empty($this->is_missed_call) && $this->is_missed_call == self::IS_MISSED_CALL_TRUE) ? self::IS_MISSED_CALL_TRUE : self::IS_MISSED_CALL_FALSE;
                break;
            default:
                return parent::__get($property);
        }
    }

    public function getStr($property) {
        switch ($property)
        {
            case 'is_incoming':
                return (!empty($this->is_incoming) && $this->is_incoming == self::IS_INCOMING_TRUE) ? 'true' : 'false';
                break;
            case 'is_ignore':
                return (!empty($this->is_ignore) && $this->is_ignore == self::IS_IGNORE_TRUE) ? 'true' : 'false';
                break;
            case 'is_system':
                return (!empty($this->is_system) && $this->is_system == self::IS_SYSTEM_TRUE) ? 'true' : 'false';
                break;
            case 'is_operator':
                return (!empty($this->is_operator) && $this->is_operator == self::IS_OPERATOR_TRUE) ? 'true' : 'false';
                break;
            case 'is_missed_call':
                return (!empty($this->is_missed_call) && $this->is_missed_call == self::IS_MISSED_CALL_TRUE) ? 'true' : 'false';
                break;
            default:
                return $this->$property;
        }
    }

    public function loadFromPost($arr)
    {
        $params        = array_change_key_case($arr['data']['PARAMS'], CASE_LOWER);
        
        $this->details = print_r($arr, true);

        $this->bot_id = array_key_first($arr['data']['BOT']);

        // проверка игнорировать или нет
        if (Helpers::is_contains_message($arr['data']['PARAMS']['MESSAGE'], $this->configsModel->is_ignore_str) === true
            || Helpers::is_contains_message($arr['data']['PARAMS']['MESSAGE'], ['🤖☎']) === true
            || empty($arr['data']['PARAMS']['MESSAGE'])
            /*|| trim($arr['data']['PARAMS']['MESSAGE']) == '1'
            || trim($arr['data']['PARAMS']['MESSAGE']) == '2'
            || trim($arr['data']['PARAMS']['MESSAGE']) == '3'*/
        ) {
            $this->is_ignore = self::IS_IGNORE_TRUE;
        } else

        // проверка пропущенный звонок или нет
        if (Helpers::is_contains_message($arr['data']['PARAMS']['MESSAGE'], $this->configsModel->is_missed_call_str) === true) {
            $this->is_missed_call = self::IS_MISSED_CALL_TRUE;
        } else

        // проверка бот или нет
        if (Helpers::is_contains_message($arr['data']['PARAMS']['MESSAGE'], $this->configsModel->is_system_str) === true) {
            $this->is_system = self::IS_SYSTEM_TRUE;
        } else

        // проверка оператор или нет
        if (Helpers::is_contains_message($arr['data']['PARAMS']['MESSAGE'], $this->configsModel->is_operator_str) === true
            || (!empty($arr['data']['USER']['IS_EXTRANET']) && $arr['data']['USER']['IS_EXTRANET'] == 'N' && !$this->is_system && !$this->is_missed_call)
        ) {
            $this->is_operator = self::IS_OPERATOR_TRUE;
        }

        // проверка входящее или исходящее сообщение
        // incoming = true - это сообщение от клиента
        if (!$this->is_ignore && !$this->is_system && !$this->is_operator && !$this->is_missed_call) {
            $this->is_incoming = self::IS_INCOMING_TRUE;
        }

        // Отдельная порверка для сообщений с файлами
        // Выполняем только в случае, если сообщение пустое, а файл прикреплен
        // Только для поддерживаемого типа будет передан диалог как "входящий"
        $isFilesEmpty = empty($arr['data']['PARAMS']['FILES']);
        $isMessageEmpty = empty($arr['data']['PARAMS']['MESSAGE']); 
        if ($isMessageEmpty && !$isFilesEmpty) {
            $firstFileKey = array_key_first($arr['data']['PARAMS']['FILES']);
            if (isset($firstFileKey) && isset($arr['data']['PARAMS']['FILES'][$firstFileKey]['type'])) {
                $fileType = $arr['data']['PARAMS']['FILES'][$firstFileKey]['type'];
                if (in_array($fileType, self::SUPPORTED_TYPES)) {
                    $this->is_ignore = self::IS_IGNORE_FALSE;
                    $this->is_incoming = self::IS_INCOMING_TRUE;
                }
            }
        }

        // контакт или лид?
        $ced_2 = explode('|', $arr['data']['PARAMS']['CHAT_ENTITY_DATA_2'] ?? '');

        foreach ($ced_2 as $i => $entry) {
            switch ($entry) {
                case 'CONTACT':
                    $this->workflow_contact_id = intval($ced_2[$i + 1]);
                    if (!empty($this->workflow_contact_id)) {
                        $this->workflow_type = $entry;
                    }
                    break;
                case 'LEAD':
                    $this->workflow_lead_id = intval($ced_2[$i + 1]);
                    if (!empty($this->workflow_lead_id)) {
                        $this->workflow_type = $entry;
                    }
                    break;
            }
        }
        unset($this->configsModel);
        return parent::loadFromPost($params);
    }




}