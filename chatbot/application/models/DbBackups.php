<?php

namespace application\models;

use application\lib\Db;
use application\lib\Mysqldump;
use application\helpers\FileHelper;

class DbBackups extends Db
{

    public $path;

    public function __construct()
    {
        parent::__construct();
        $this->path = __DIR__ . '/../../uploads/db_backups';
    }

    public function getFiles()
    {
        $files = FileHelper::findFiles($this->path, ['only' => ['*.gz'], 'recursive' => FALSE]);
        return $files;
    }

    public function create()
    {
        if (file_exists($this->path)) {
            if (is_dir($this->path)) {
                if (!is_writable($this->path)) {
                    $this->isError = true;
                    $this->message = "Дирректория не доступна для записи.\n";
                    return false;
                }
                $fileName = 'dump_' . date('y-m-d_H-i') . '.gz';
                $filePath = $this->path . DIRECTORY_SEPARATOR . $fileName;

                try {
                    $config = require __DIR__ . '/../config/db.php';
                    //exec('pg_dump --dbname=postgresql://' . $config['user'] . ':' . $config['password'] . '@' . $config['host'] . ':5432/' . $config['name'] . ' | gzip > ' . $filePath,$output);
                    exec('pg_dump -Fp -Z9 --dbname=postgresql://' . $config['user'] . ':' . $config['password'] . '@' . $config['host'] . ':5432/' . $config['name'] . ' > ' . $filePath,$output);

                } catch (\Exception $e) {
                    $this->isError = true;
                    $this->message = "Ошибка создания дампа: " . $e->getMessage() . "\n";
                    return false;
                }
                $this->message = "Экспорт успешно завершен. Файл '" . $fileName . "' в папке " . $this->path;
                return true;
            } else {
                $this->isError = true;
                $this->message = "Путь должен быть папкой.\n";
                return false;
            }
        } else {
            $this->isError = true;
            $this->message = "Указанный путь не существует.\n";
            return false;
        }

    }

    public function delete()
    {
        if (file_exists($this->path)) {
            if (is_file($this->path)) {
                unlink($this->path);
                return true;
            } else {
                $this->message = "Путь должен быть файлом.\n";
                return false;
            }
        } else {
            return false;
        }
    }
}