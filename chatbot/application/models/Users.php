<?php

namespace application\models;

use application\core\Model;

/**
 * This is the model class for table "chats".
 *
 * @property integer $id
 * @property string $role
 * @property integer $name
 * @property integer $email
 * @property string $sex
 * @property integer $photo
 * @property string $phone
 * @property string $status
 * @property string $password_hash
 * @property string $updated_at
 * @property string $created_at
 */
class Users extends Model {

    protected $id, $role, $name, $email, $sex, $photo, $phone, $status, $password_hash, $updated_at, $created_at;

    const ROLE_ADMIN = 1;
    const ROLE_RESPONSIBLE = 2;
    const ROLE_VIEWER = 3;

    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 2;

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['name', 'role', 'email', 'sex', 'phone', 'status'], 'required'],
            [['name', 'email', 'phone', 'password_hash'], 'string'],
            [['role', 'sex', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres() {
        $result = [];

        $variables = $this->modelToArray();

        foreach ($variables as $variableName => $variable) {
            if (empty($this->$variableName)) { continue; }
            $result[] = ['=', self::tableName() . '.' . $variableName, $this->$variableName];
        }

        return $result;
    }

    public static function getViewTableProperties() {
        return
            [
                'id' => ['style' => 'width:2%;', 'label' => '#'],
                'role' => ['label' => 'Права'],
                'name' => ['label' => 'Пользователь'],
                'email' => ['label' => 'Email'],
                'sex' => ['label' => 'Пол'],
                'photo' => ['onTable' => false, 'label' => 'Фото'],
                'password_hash' => ['onTable' => false, 'label' => 'Пароль'],
                'phone' => ['onTable' => false, 'label' => 'Телефон'],
                'status' => ['onTable' => false, 'label' => 'Статус'],
                'updated_at' => ['label' => 'Дата обновления'],
                'created_at' => ['label' => 'Дата создания'],
            ];
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'role':
                return $this->getRoleStr();
                break;
            case 'sex':
                return $this->getSexStr();
                break;
            case 'status':
                return $this->getStatusStr();
                break;
            case 'password_hash':
                return null;
                break;
            default:
                return parent::__get($property);
        }
    }

    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'password_hash':
                if (!$this->isNewRecord && empty($value)) {
                    $this->$property = null;
                } else {
                    $this->$property = $value;
                }
                break;
            default:
                return parent::__set($property, $value);
        }
    }


    public static function getDefaultPhoto()
    {
        return ($_SESSION['curUser']['sex'] == self::SEX_MALE) ? 'male' : 'female';
    }

    public function getRoleArray()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_RESPONSIBLE => 'Ответственный',
            self::ROLE_VIEWER => 'Наблюдатель',
        ];
    }

    public function getRoleStr()
    {
        if (key_exists($this->role, $this->getRoleArray())) {
            return $this->getRoleArray()[$this->role];
        } else {
            return '';
        }
    }

    public static function getSexArray()
    {
        return [
            self::SEX_MALE => "Мужской",
            self::SEX_FEMALE => "Женский",
        ];
    }

    public function getSexStr()
    {
        if (key_exists($this->sex, $this->getSexArray())) {
            return $this->getSexArray()[$this->sex];
        } else {
            return '';
        }
    }

    public static function getStatusArray()
    {
        return [
            self::STATUS_ACTIVE => "Активный",
            self::STATUS_BLOCKED => "Заблокированный",
        ];
    }

    public function getStatusStr()
    {
        if (key_exists($this->status, $this->getStatusArray())) {
            return $this->getStatusArray()[$this->status];
        } else {
            return '';
        }
    }

    public function validate(): bool
    {
        if (empty($this->id) && !$this->checkUniqueName()) {
            $this->isError = true;
            $this->sqlMessage = "Ошибка проверки данных имени пользователя: значение не уникально";
            return false;
        }
        return parent::validate();
    }

    protected function checkUniqueName() {
        $searchNotUniqueNameModel = new Users();
        $searchNotUniqueNameModel->name = $this->name;
        $model = $searchNotUniqueNameModel->selectRow();
        if (!empty($model->id)) {
            return false;
        }
        return true;
    }


    public function validateLoginInDb($password) {
        if (empty($this->name)) {
            return false;
        };
        $this->status = self::STATUS_ACTIVE;
        $result = $this->selectRow();

        if (empty($result) || empty($result->id) || empty($result->password_hash) || empty($result->name)) {
            return false;
        }
        if (!empty(__PEPPER__)) {
            $pepper = __PEPPER__;
        } else {
            $pepper = '';
        }
        $pwd_peppered = hash_hmac("sha256", $password, $pepper);
        if (password_verify($pwd_peppered, $result->password_hash)) {
            static::updateSession($result);
            return true;
        } else {
            return false;
        }
    }

    public function validatePasswordInDb($password) {
        if (empty($this->id)) {
            throw new \Exception('Пустой id пользователя');
        };
        $this->status = self::STATUS_ACTIVE;
        $result = $this->selectRow();
        if (empty($result) || empty($result->password_hash)) {
            return false;
        }
        if (!empty(__PEPPER__)) {
            $pepper = __PEPPER__;
        } else {
            $pepper = '';
        }
        $pwd_peppered = hash_hmac("sha256", $password, $pepper);
        return password_verify($pwd_peppered, $result->password_hash);
    }

    public function generatePasswordHash($password)
    {
        if (!empty(__PEPPER__)) {
            $pepper = __PEPPER__;
        } else {
            $pepper = '';
        }
        $pwd_peppered = hash_hmac("sha256", $password, $pepper);
        $this->password_hash = password_hash($pwd_peppered, PASSWORD_ARGON2I);
    }

    public function updatePasswordHash($password) {
        if (empty($this->id)) {
            $this->sqlMessage = 'Неверный запрос';
            return false;
        }
        $this->generatePasswordHash($password);

        $this->sqlParamsArr = [
            'id' => $this->id,
            'password_hash' => $this->password_hash
        ];

        $this->sqlStr = "UPDATE " . $this::tableName() . " SET password_hash=:password_hash WHERE id=:id";

        if (!$this->dbSet($this->sqlStr,  $this->sqlParamsArr)) {
            $resultDetail = (!empty($this->sqlMessage) ? ("Message from pdo: " . $this->sqlMessage . " | ") : "")
                . "sql: " . trim(strip_tags($this->sqlStr))
                . " | params: " . http_build_query($this->sqlParamsArr, '', ', ');
            Logs::insertRowLogs(  'error', get_class($this), $resultDetail, debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        } else {
            return true;
        }
    }

    public static function generateToken() {
        return bin2hex(random_bytes(15));
    }

    public function isUniqueName() {
        if (empty($this->name)) {
            throw new \Exception('Пустое имя пользователя');
        }
        $this->id = null;
        return $this->selectRowsCount() == 0 && empty($this->sqlMessage);
    }

    public static function updateSession($object) {
        $_SESSION['curUser']['id'] = $object->id;
        $_SESSION['curUser']['name'] = $object->name;
        $_SESSION['curUser']['email'] = $object->email;
        $_SESSION['curUser']['photo'] = $object->photo;
        $_SESSION['curUser']['sex'] = $object->sex;
        $_SESSION['curUser']['role'] = $object->role;
    }

    public function insertRow()
    {
        $this->generatePasswordHash($this->password_hash);
        return parent::insertRow();
    }

    public function updateRow()
    {
        $this->generatePasswordHash($this->password_hash);
        return parent::updateRow();
    }

}