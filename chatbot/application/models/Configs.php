<?php

namespace application\models;

use application\core\Model;

/**
 * This is the model class for table "configs".
 *
 * @property integer $id
 * @property string $member_id
 * @property integer $towards_send_dialog
 * @property integer $tmpl_contact_id
 * @property integer $tmpl_lead_id
 * @property string $is_ignore_str
 * @property string $is_system_autoclose
 * @property string $is_system_str
 * @property string $is_operator_str
 * @property string $is_missed_call_str
 * @property string $is_missed_call_answer_str
 * @property string $updated_at
 * @property string $created_at
 */
class Configs extends Model
{
    protected $id, $member_id, $towards_send_dialog, $tmpl_contact_id, $tmpl_lead_id, $is_ignore_str, $is_system_autoclose, $is_system_str,
        $is_operator_str, $is_missed_call_str, $is_missed_call_answer_str, $updated_at, $created_at;

    const TOWARD_SEND_DIALOG_OPEN_LINE = 1;
    const TOWARD_SEND_DIALOG_OPERATOR = 2;

    const IS_IGNORE_STR = '["=== SYSTEM WZ === Произошла ошибка. Информация уже направлена разработчикам Wazzup."'
    . ',"=== SYSTEM WZ === Check this message on your device"'
    . ',"=== SYSTEM WZ === 24-часовая сессия истекла."'
    . ',"=== SYSTEM WZ === Проверьте это сообщение на своем устройстве"'
    . ',"=== SYSTEM WZ === Клиент не установил приложение или привязал его к другому номеру."]';

    const IS_SYSTEM_STR = '["=== Исходящее сообщение, автор: Битрикс24 ==="'
        . ',"=== Outgoing message, author: Bitrix24 ==="]';
    const    IS_OPERATOR_STR        = '["=== Исходящее сообщение, автор: Битрикс24 ("'
        . ',"=== Исходящее сообщение, автор: Телефон ==="'
        . ',"=== Исходящее сообщение, автор: WAZZUP ==="]';
    const    IS_MISSED_CALL_STR = '["=== SYSTEM WZ === Пропущенный звонок от клиента"]';
    const    IS_MISSED_CALL_ANSWER_STR = 'К сожалению данный номер не поддерживает звонки через WhatsApp, пожалуйста, напишите нам текстом.';


    public static function tableName()
    {
        return 'configs';
    }

    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['towards_send_dialog', 'tmpl_contact_id', 'tmpl_lead_id', 'is_system_autoclose'], 'integer'],
            [['member_id', 'is_missed_call_answer_str'], 'string'],
            [['is_ignore_str', 'is_system_str', 'is_operator_str', 'is_missed_call_str'], 'array'],
            [['updated_at', 'created_at'], 'datetime'],
        ];
    }

    public function andFilterWheres()
    {
        return [
            ['=', 'configs.id', $this->id],
            ['=', 'configs.member_id', $this->member_id],
            ['=', 'configs.towards_send_dialog', $this->towards_send_dialog],
            ['=', 'configs.tmpl_contact_id', $this->tmpl_contact_id],
            ['=', 'configs.tmpl_lead_id', $this->tmpl_lead_id],
            ['like', 'configs.is_ignore_str', $this->is_ignore_str],
            ['=', 'configs.is_system_autoclose', $this->is_system_autoclose],
            ['like', 'configs.is_system_str', $this->is_system_str],
            ['like', 'configs.is_operator_str', $this->is_operator_str],
            ['like', 'configs.is_missed_call_str', $this->is_missed_call_str],
            ['like', 'configs.is_missed_call_answer_str', $this->is_missed_call_answer_str],
            ['=', 'configs.updated_at', $this->updated_at],
            ['=', 'configs.created_at', $this->created_at],
        ];
    }

    public static function getViewTableProperties()
    {
        return
            [
                'id' => ['style' => 'width:2%;', 'label' => '#'],
                'member_id' => ['label' => 'Клиент'],
                'towards_send_dialog' => ['label' => 'Передать диалог'],
                'tmpl_contact_id'  => ['label' => 'Шаблон бизнес-процесса для контакта'],
                'tmpl_lead_id' => ['label' => 'Шаблон бизнес-процесса для лида'],
                'is_ignore_str' => ['label' => 'Подстрока игнорируемого сообщения'],
                'is_system_autoclose' => ['label' => 'Автозакрытие диалогов при получении подстроки системного сообщения'],
                'is_system_str' => ['label' => 'Подстрока системного сообщения'],
                'is_operator_str' => ['label' => 'Подстрока сообщения оператора'],
                'is_missed_call_str' => ['label' => 'Подстрока пропущенного звонка'],
                'is_missed_call_answer_str' => ['label' => 'Сообщение на пропущенный звонок (если не заполнено, то не отправлять)']
            ];
    }

    public function loadFromPost($arr): bool
    {
        if (!parent::loadFromPost($arr)) {
            return false;
        }
        if (empty($arr['is_system_autoclose'])) {
            $this->is_system_autoclose = 0;
        }
        return true;
    }

    public function setDefaultValues() {
        $this->is_ignore_str = self::IS_IGNORE_STR;
        $this->is_system_str = self::IS_SYSTEM_STR;
        $this->is_operator_str = self::IS_OPERATOR_STR;
        $this->is_missed_call_str = self::IS_MISSED_CALL_STR;
        $this->is_missed_call_answer_str = self::IS_MISSED_CALL_ANSWER_STR;
    }


}