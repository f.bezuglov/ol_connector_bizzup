<?php

namespace application\core;

use application\helpers\Helpers;
use application\models\Logs;
use DateTime;
use application\lib\Db;
use application\lib\Bitrix24;

abstract class Model extends Db
{

    protected $sqlStr, $sqlSelectStr, $sqlJoinsStr, $sqlConditionStr, $sqlSortDirectStr, $sqlLimitStr, $sqlParamsArr;

    public function __construct()
    {
        parent::__construct();
        $this->sqlStr = '';
        $this->sqlSelectStr = '';
        $this->sqlJoinsStr = '';
        $this->sqlConditionStr = '';
        $this->sqlSortDirectStr = '';
        $this->sqlLimitStr = '';
        $this->sqlParamsArr = [];
        $this->id = 0;
    }

    public abstract static function tableName();

    public abstract static function getViewTableProperties();

    public abstract function rules();

    public abstract function andFilterWheres();


    public function __get($property)
    {
        switch ($property) {
            case 'updated_at':
                return Helpers::date_reformat($this->updated_at);
                break;
            case 'created_at':
                return Helpers::date_reformat($this->created_at);
                break;
            case 'details':
                return '<pre>' . $this->details . '</pre>';
                break;
            default:
                return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        switch ($property) {
            default:
                $this->$property = $value;
        }
    }

    public function __isset($property)
    {
        return isset($this->$property);
    }

    public function loadFromPost($arr): bool
    {
        if ($arr && is_array($arr)) {
            $curVariables = $this->getClassVariables();
            foreach ($arr as $postVariableName => $postVariable) {
                if (key_exists($postVariableName, $curVariables)) {
                    $this->$postVariableName = $postVariable;
                }
            }
            return true;
        }
        return false;
    }

    public function loadFromGet(): bool
    {
        if ($_GET && is_array($_GET)) {
            $curVariables = $this->getClassVariables();
            foreach ($_GET as $getVariableName => $getVariable) {
                if (key_exists($getVariableName, $curVariables)) {
                    $this->$getVariableName = $getVariable;
                }
            }
            return true;
        }
        return false;
    }

    public function validate(): bool
    {
        $rules = $this->rules();
        if (!isset($rules)) {
            $this->isError = true;
            $this->sqlMessage = "Неверные правила проверки данных";
            return false;
        }
        foreach ($rules as $rule) {
            switch ($rule[1]) {
                case "required" :
                    if (isset($rule['with'])) {
                        foreach ($rule['with'] as $variableName => $variableValue) {
                            if ($this->$variableName !== $variableValue) {
                                continue 3;
                            }
                        }
                    }
                    foreach ($rule[0] as $ruleVariable) {
                        if (isset($this->$ruleVariable) || $ruleVariable == 'except') {
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "string" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || is_string($this->$ruleVariable)) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? trim(strip_tags($this->$ruleVariable)) : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "integer" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || (isset($this->$ruleVariable) && is_int((int)$this->$ruleVariable))) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? (int)$this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "number" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || (isset($this->$ruleVariable) && is_numeric((float)$this->$ruleVariable))) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? (string)(float)$this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "date" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || DateTime::createFromFormat('Y-m-d', $this->$ruleVariable)) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? $this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "datetime" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || DateTime::createFromFormat('Y-m-d H:i:s', $this->$ruleVariable)) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? $this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "time" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (!isset($this->$ruleVariable) || DateTime::createFromFormat('H:i', $this->$ruleVariable)) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? $this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "array" :
                    foreach ($rule[0] as $ruleVariable) {
                        if (isset($this->$ruleVariable) && is_array($this->$ruleVariable)) {
                            $this->$ruleVariable = isset($this->$ruleVariable) ? $this->$ruleVariable : null;
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariable;
                            return false;
                        }
                    }
                    break;
                case "in_array" :
                    foreach ($rule[0] as $ruleVariableName => $ruleVariable) {
                        if (isset($this->$ruleVariableName) && in_array($this->$ruleVariableName, $ruleVariable)) {
                            continue;
                        } else {
                            $this->isError = true;
                            $this->sqlMessage = "Ошибка проверки данных " . $rule[1] . ": " . $this->tableName() . '.' . $ruleVariableName;
                            return false;
                        }
                    }
            }
        }
        return true;
    }

    /**
     * @throws \Exception
     */
    public function selectRowsCount()
    {
        if (method_exists($this, 'searchInit')) {
            $this->searchInit();
        }
        $this->setSqlConditionStr();

        $this->sqlStr = "SELECT COUNT(" . $this->tableName() . ".id)
        FROM " . $this->tableName() . $this->sqlJoinsStr . $this->sqlConditionStr;

        return $this->fetchColumn($this->sqlStr, $this->sqlParamsArr);
    }

    public function selectRowsCountByMemberId()
    {
        if (method_exists($this, 'searchInit')) {
            $this->searchInit();
        }
        $this->setSqlConditionStr();

        $this->sqlStr = "SELECT COUNT(" . $this->tableName() . ".member_id)
        FROM " . $this->tableName() . "" . $this->sqlJoinsStr . $this->sqlConditionStr;

        return $this->fetchColumn($this->sqlStr, $this->sqlParamsArr);
    }

    /**
     * выборка из БД. результат в модель того же класса
     *
     * @return $this[]
     *
     */

    public function selectRows(): static
    {
        if (method_exists($this, 'searchInit')) {
            $this->searchInit();
        }
        $this->setSqlConditionStr();

        $curClass = get_called_class();

        $this->sqlStr = "SELECT " . (empty($this->sqlSelectStr) ? '*' : $this->sqlSelectStr) . "
        FROM " . $this->tableName()
            . $this->sqlJoinsStr
            . $this->sqlConditionStr
            . $this->sqlSortDirectStr
            . $this->sqlLimitStr;

        $rows = $this->fetchAll($this->sqlStr, $this->sqlParamsArr);

        $models = [];
        $curVariables = $curClass::getClassVariables();
        foreach ($rows as $rowId => $row) {
            $model = new $curClass;
            foreach ($curVariables as $variableName => $variable) {
                if (!isset($row[$variableName])) {
                    continue;
                }
                $model->$variableName = $row[$variableName];
            }
            $model->isNewRecord = false;
            $models[$rowId] = $model;
        }
        return $models;
    }

    /**
     * выборка из БД. результат в модель того же класса
     *
     * @return $this
     */
    public function selectRow(): static
    {
        if (method_exists($this, 'searchInit')) {
            $this->searchInit();
        }
        $this->setSqlConditionStr();

        $curClass = get_called_class();

        $this->sqlStr = "SELECT " . (empty($this->sqlSelectStr) ? '*' : $this->sqlSelectStr) . "
        FROM " . $this->tableName()
            . $this->sqlJoinsStr
            . $this->sqlConditionStr . ' LIMIT 1';

        $result = $this->fetchAll($this->sqlStr, $this->sqlParamsArr);

        $curVariables = $curClass::getClassVariables();
        $model = new $curClass;
        foreach ($curVariables as $variableName => $variable) {
            if (!isset($result[0][$variableName])) {
                continue;
            }
            $model->$variableName = $result[0][$variableName];
        }
        if (isset($model->id) && $model->id != 0) {
            $model->isNewRecord = false;
        }

        return $model;
    }


    public function deleteRow()
    {
        $this->sqlParamsArr = [
            'id' => $this->id
        ];
        $this->sqlStr = "DELETE FROM " . $this->tableName()
            . " WHERE " . $this->tableName() . ".id=:id";

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public function deleteRows()
    {
        $this->sqlStr = "DELETE FROM " . $this->tableName();
        $this->execute($this->sqlStr);
    }

    public function sequenceRestartId($number)
    {
        $this->sqlStr = "ALTER SEQUENCE " . $this->tableName() . "_id_seq RESTART WITH " . (int)$number;
        $this->execute($this->sqlStr);
    }

    public function deleteRowByFilter($filter)
    {
        if (empty($filter) && !is_array($filter)) {
            Logs::insertRowLogs("error", get_class($this), 'Неверный запрос', debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            $this->sqlMessage = 'Неверный запрос';
            return false;
        }
        $curVariables = $this::getClassVariables();
        unset($curVariables['id']);

        $this->sqlParamsArr = [];
        $partParamsSql = '';
        $i = 0;
        foreach ($filter as $curVariableName => $curVariableValue) {

            if (strpos($curVariableName, '<=') === 0) {
                $curVariableName = ltrim($curVariableName, '<=');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '<=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '<=:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '>=') === 0) {
                $curVariableName = ltrim($curVariableName, '>=');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '>=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '>=:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '<') === 0) {
                $curVariableName = ltrim($curVariableName, '<');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '<:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '<:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '>') === 0) {
                $curVariableName = ltrim($curVariableName, '>');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '>:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '>:' . $curVariableTmp;
                }
            } else {
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '=:' . $curVariableTmp;
                }
            }
            $i++;
        }

        $this->sqlStr = "DELETE FROM  " . $this::tableName() . " WHERE " . $partParamsSql;

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public function insertRow()
    {
        $curVariables = $this::getClassVariables();
        unset($curVariables['id']);
        $this->sqlParamsArr = [];
        $firstPartSql = '';
        $secondPartSql = '';
        foreach ($curVariables as $curVariableName => $curVariableValue) {
            if (!isset($this->$curVariableName)) {
                continue;
            }

            $firstPartSql .= empty($firstPartSql) ? ('' . $curVariableName . '') : (', ' . $curVariableName . '');
            if (!is_array($this->$curVariableName)) {
                $secondPartSql .= empty($secondPartSql) ? (':' . $curVariableName) : (', :' . $curVariableName);
                $this->sqlParamsArr[$curVariableName] = $this->$curVariableName;
            } else {
                $secondPart = '';
                foreach ($this->$curVariableName as $key => $item) {
                    $curArrayItemName = $curVariableName . '_' . $key;
                    $secondPart .= empty($secondPart) ? ('ARRAY[:' . $curArrayItemName) : (', :' . $curArrayItemName);
                    $this->sqlParamsArr[$curArrayItemName] = $item;
                }
                $secondPart .= empty($secondPart) ? '' : ']';
                $secondPartSql .= empty($secondPartSql) ? $secondPart : (', ' . $secondPart);
            }
        }

        $this->sqlStr = "INSERT INTO " . $this::tableName() . " (" . $firstPartSql . ") VALUES (" . $secondPartSql . ")";

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    /*
     *
     * INSERT INTO patients (member_id, mis_id, bitrix_id, hash) VALUES ('8f53e24647055b223bfbf33ed35cb54f', '332639', '19', '845ae7635ad1447567954049f7eb1b8b') ON CONFLICT ON CONSTRAINT uniq_idx_patients_member_id_mis_id DO UPDATE SET member_id='8f53e24647055b223bfbf33ed35cb54f', mis_id='332639', bitrix_id='19', hash='845ae7635ad1447567954049f7eb1b8b';
     */
    public function replaceRow()
    {
        if (empty($this->member_id) || empty($this->bot_id) || empty($this->chat_id)) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный запрос';
            Logs::insertRowLogs("error", get_class($this), 'Неверный запрос | ' . http_build_query($this->modelToArray(), '', ', '), debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        }
        $curVariables = $this::getClassVariables();
        unset($curVariables['id']);
        $this->sqlParamsArr = [];
        $firstPartSql = '';
        $secondPartSql = '';
        $replacePartSql = '';
        foreach ($curVariables as $curVariableName => $curVariableValue) {
            if (!isset($this->$curVariableName)) {
                continue;
            }
            $this->sqlParamsArr[$curVariableName] = $this->$curVariableName;
            $firstPartSql .= empty($firstPartSql) ? ('' . $curVariableName . '') : (', ' . $curVariableName . '');
            $secondPartSql .= empty($secondPartSql) ? (':' . $curVariableName) : (', :' . $curVariableName);
            $replacePartSql .= empty($replacePartSql) ? ('' . $curVariableName . '=:' . $curVariableName) : (', ' . $curVariableName . '=:' . $curVariableName);
        }

        $this->sqlStr = "INSERT INTO " . $this::tableName() . " (" . $firstPartSql . ") VALUES (" . $secondPartSql . ")"
            . " ON CONFLICT (member_id, bot_id, chat_id) DO UPDATE SET " . $replacePartSql;

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public function replaceBitrixRow()
    {
        if (empty($this->member_id) || empty($this->bitrix_id)) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный запрос';
            Logs::insertRowLogs("error", get_class($this), 'Неверный запрос | ' . http_build_query($this->modelToArray(), '', ', '), debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        }
        $curVariables = $this::getClassVariables();
        unset($curVariables['id']);
        $this->sqlParamsArr = [];
        $firstPartSql = '';
        $secondPartSql = '';
        $replacePartSql = '';
        foreach ($curVariables as $curVariableName => $curVariableValue) {
            if (!isset($this->$curVariableName)) {
                continue;
            }
            $this->sqlParamsArr[$curVariableName] = $this->$curVariableName;
            $firstPartSql .= empty($firstPartSql) ? ('' . $curVariableName . '') : (', ' . $curVariableName . '');
            $secondPartSql .= empty($secondPartSql) ? (':' . $curVariableName) : (', :' . $curVariableName);
            $replacePartSql .= empty($replacePartSql) ? ('' . $curVariableName . '=:' . $curVariableName) : (', ' . $curVariableName . '=:' . $curVariableName);
        }

        $this->sqlStr = "INSERT INTO " . $this::tableName() . " (" . $firstPartSql . ") VALUES (" . $secondPartSql . ")"
            . " ON CONFLICT (member_id, bitrix_id) DO UPDATE SET " . $replacePartSql;

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public function updateRow()
    {
        if (empty($this->id)) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный запрос';
            Logs::insertRowLogs("error", get_class($this), 'Неверный запрос | ' . http_build_query($this->modelToArray(), '', ', '), debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        }
        $curVariables = $this::getClassVariables();
        unset($curVariables['id'], $curVariables['created_by'], $curVariables['created_at']);

        $this->sqlParamsArr = [
            'id' => $this->id,
        ];
        $partSql = '';
        foreach ($curVariables as $curVariableName => $curVariableValue) {
            if (!isset($this->$curVariableName)) {
                continue;
            }
            if (!is_array($this->$curVariableName)) {
                $this->sqlParamsArr[$curVariableName] = $this->$curVariableName;
            } else {
                $this->sqlParamsArr[$curVariableName] = $this->to_pg_array($this->$curVariableName);
            }
            $partSql .= empty($partSql) ? ($curVariableName . '=:' . $curVariableName) : (', ' . $curVariableName . '=:' . $curVariableName);
        }

        if (empty($partSql)) {
            $this->sqlMessage = 'Пустой запрос';
            Logs::insertRowLogs("error", get_class($this), 'Пустой запрос', debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        }

        $this->sqlStr = "UPDATE " . $this::tableName() . " SET " . $partSql . " WHERE id=:id";

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public function updateRowByFilter($filter)
    {
        if (empty($filter) && !is_array($filter)) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный запрос';
            Logs::insertRowLogs("error", get_class($this), 'Неверный запрос', debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            return false;
        }
        $curVariables = $this::getClassVariables();
        unset($curVariables['id'], $curVariables['created_at']);

        $this->sqlParamsArr = [];
        $partParamsSql = '';
        $i = 0;
        foreach ($filter as $curVariableName => $curVariableValue) {

            if (strpos($curVariableName, '<=') === 0) {
                $curVariableName = ltrim($curVariableName, '<=');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '<=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '<=:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '>=') === 0) {
                $curVariableName = ltrim($curVariableName, '>=');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '>=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '>=:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '<') === 0) {
                $curVariableName = ltrim($curVariableName, '<');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '<:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '<:' . $curVariableTmp;
                }
            } elseif (strpos($curVariableName, '>') === 0) {
                $curVariableName = ltrim($curVariableName, '>');
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '>:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '>:' . $curVariableTmp;
                }
            } else {
                $curVariableTmp = $curVariableName . $i;
                $this->sqlParamsArr[$curVariableTmp] = $curVariableValue;
                if (empty($partParamsSql)) {
                    $partParamsSql = $curVariableName . '=:' . $curVariableTmp;
                } else {
                    $partParamsSql = $partParamsSql . ' AND ' . $curVariableName . '=:' . $curVariableTmp;
                }
            }
            $i++;
        }
        $partSql = '';
        foreach ($curVariables as $curVariableName => $curVariableValue) {
            if (!isset($this->$curVariableName)) {
                continue;
            }
            $this->sqlParamsArr[$curVariableName] = $this->$curVariableName;
            $partSql .= empty($partSql) ? ($curVariableName . '=:' . $curVariableName) : (', ' . $curVariableName . '=:' . $curVariableName);
        }

        if (empty($partSql)) {
            Logs::insertRowLogs("error", get_class($this), 'Пустой запрос', debug_backtrace()[1]['class'] . ' | ' . debug_backtrace()[1]['function']);
            $this->sqlMessage = 'Пустой запрос';
            return false;
        }

        $this->sqlStr = "UPDATE " . $this::tableName() . " SET " . $partSql . " WHERE " . $partParamsSql;

        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    public static function getClassVariables()
    {
        $variables = get_class_vars(get_called_class());
        unset($variables['PDO'], $variables['sqlMessage'], $variables['isError'], $variables['numError'], $variables['isNewRecord'],
            $variables['isNeedUpdate'], $variables['isNeedInsert']);
        unset($variables['sqlSelectStr'], $variables['sqlJoinsStr'], $variables['sqlConditionStr']);
        unset($variables['sqlSortDirectStr'], $variables['sqlLimitStr'], $variables['sqlParamsArr']);
        unset($variables['sqlStr']);
        return $variables;
    }

    public static function loadMultipleFromPost($modelClass)
    {

        $modelClassArr = explode('\\', $modelClass);
        $modelClassShortName = array_pop($modelClassArr);
        $curVariables = get_class_vars($modelClass);

        $models = [];

        if (!empty($_POST[$modelClassShortName]) && is_array($_POST[$modelClassShortName])) {
            foreach ($_POST[$modelClassShortName] as $postIndex => $postVariables) {
                if (is_array($postVariables)) {
                    $model = new $modelClass;
                    foreach ($postVariables as $postVariableName => $postVariable) {
                        if (key_exists($postVariableName, $curVariables)) {
                            $model->$postVariableName = $postVariable;
                        }
                    }
                    $models[$postIndex] = $model;
                }
            }
        }


        return $models;
    }

    public function setSqlJoinsStr($str)
    {
        $this->sqlJoinsStr = $str;
    }

    public function setSqlConditionStr()
    {
        $this->sqlConditionStr = '';
        $andFilterWheres = $this->andFilterWheres();

        foreach ($andFilterWheres as $andFilterWhere) {

            if (empty($andFilterWhere) || !is_array($andFilterWhere) || !in_array($andFilterWhere[0], ['=', 'like']) || empty($andFilterWhere[1]) || empty($andFilterWhere[2])) {
                continue;
            }
            if ($andFilterWhere[0] == '=') {
                $tmpNameParameter = str_replace('.', '_', $andFilterWhere[1]);
                if (strpos($andFilterWhere[2], '<=') === 0) {
                    $this->sqlParamsArr[$tmpNameParameter] = ltrim($andFilterWhere[2], '<=');
                    $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE ' . $andFilterWhere[1] . '<=:' . $tmpNameParameter) : (' AND ' . $andFilterWhere[1] . '<=:' . $tmpNameParameter);
                } elseif (strpos($andFilterWhere[2], '>=') === 0) {
                    $this->sqlParamsArr[$tmpNameParameter] = ltrim($andFilterWhere[2], '>=');
                    $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE ' . $andFilterWhere[1] . '>=:' . $tmpNameParameter) : (' AND ' . $andFilterWhere[1] . '>=:' . $tmpNameParameter);
                } elseif (strpos($andFilterWhere[2], '<') === 0) {
                    $this->sqlParamsArr[$tmpNameParameter] = ltrim($andFilterWhere[2], '<');
                    $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE ' . $andFilterWhere[1] . '<:' . $tmpNameParameter) : (' AND ' . $andFilterWhere[1] . '<:' . $tmpNameParameter);
                } elseif (strpos($andFilterWhere[2], '>') === 0) {
                    $this->sqlParamsArr[$tmpNameParameter] = ltrim($andFilterWhere[2], '>');
                    $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE ' . $andFilterWhere[1] . '>:' . $tmpNameParameter) : (' AND ' . $andFilterWhere[1] . '>:' . $tmpNameParameter);
                } else {
                    $this->sqlParamsArr[$tmpNameParameter] = $andFilterWhere[2];
                    $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE ' . $andFilterWhere[1] . '=:' . $tmpNameParameter) : (' AND ' . $andFilterWhere[1] . '=:' . $tmpNameParameter);
                }
            }
            if ($andFilterWhere[0] == 'like') {
                $tmpNameParameter = str_replace('.', '_', $andFilterWhere[1]);
                $this->sqlParamsArr[$tmpNameParameter] = '%' . $andFilterWhere[2] . '%';
                $this->sqlConditionStr .= empty($this->sqlConditionStr) ? (' WHERE LOWER(' . $andFilterWhere[1] . ') LIKE LOWER(:' . $tmpNameParameter . ')') : (' AND LOWER(' . $andFilterWhere[1] . ') LIKE LOWER(:' . $tmpNameParameter) . ')';
            }
        }
    }

    public function setSqlSortDirectStr($sortsArr = [])
    {
        $this->sqlSortDirectStr = '';
        $curClass = get_called_class();
        foreach ($sortsArr as $variableName => $sortDirect) {
            if (!empty($variableName) && key_exists($variableName, $curClass::getClassVariables())) {
                $sortDirect = (!empty($sortDirect) && in_array(mb_strtolower($sortDirect), ['asc', 'desc'])) ? $sortDirect : 'asc';
                $this->sqlSortDirectStr .= empty($this->sqlSortDirectStr) ? (' ORDER BY ' . $variableName . ' ' . $sortDirect) : (' ,' . $variableName . ' ' . $sortDirect);
            }
        }
    }

    public function setSqlLimitStr($startRow, $limitRow)
    {
        $this->sqlParamsArr['startRow'] = $startRow;
        $this->sqlParamsArr['limitRow'] = $limitRow;
        $this->sqlLimitStr = ' LIMIT :limitRow OFFSET :startRow';
    }

    public function resetSqlLimitStr()
    {
        $this->sqlLimitStr = '';
    }

    public function resetSqlParamsArr()
    {
        $this->sqlParamsArr = [];
    }

    public function getSqlStr()
    {
        return $this->sqlStr;
    }

    public static function findOne($conditions = [])
    {
        $curClass = get_called_class();
        $searchModel = new $curClass;

        foreach ($conditions as $variableName => $variableValue) {
            $searchModel->$variableName = $variableValue;
        }

        $searchModel->setSqlConditionStr();

        $searchModel->sqlStr = "SELECT * FROM " . $searchModel->tableName() . $searchModel->sqlConditionStr . ' LIMIT 1';

        $result = $searchModel->execute($searchModel->sqlStr, $searchModel->sqlParamsArr);

        return $result[0] ?? [];
    }

    public static function findAll($conditions = [])
    {
        $curClass = get_called_class();
        $searchModel = new $curClass;

        foreach ($conditions as $variableName => $variableValue) {
            $searchModel->$variableName = $variableValue;
        }

        $searchModel->setSqlConditionStr();

        $searchModel->sqlStr = "SELECT * FROM " . $searchModel->tableName() . $searchModel->sqlConditionStr;

        return $searchModel->execute($searchModel->sqlStr, $searchModel->sqlParamsArr);
    }

    public function setDeletedRow()
    {
        if (empty($this->id)) {
            throw new \Exception('Неверный запрос');
        }
        $this->sqlParamsArr = [
            'id' => $this->id
        ];

        $this->sqlStr = "UPDATE " . $this->tableName() . " SET status_deleted=2 WHERE id=:id";
        $this->execute($this->sqlStr, $this->sqlParamsArr);
    }

    /**
     * @throws \Exception
     */
    public function getRequiredFields()
    {
        $result = [];
        $rules = $this->rules();
        if (empty($rules)) {
            throw new \Exception('Неверные правила проверки данных');
        }
        foreach ($rules as $rule) {
            switch ($rule[1]) {
                case "required" :
                    if (!empty($rule['with'])) {
                        foreach ($rule['with'] as $variableName => $variableValue) {
                            if ($this->$variableName !== $variableValue) {
                                continue 3;
                            }
                        }
                    }
                    foreach ($rule[0] as $ruleVariable) {
                        $result[$ruleVariable] = true;
                    }
                    break;
            }
        }
        return $result;
    }

    /**
     * переносим переменные объекта Model в массив
     *
     * @return array|bool
     */
    public function modelToArray()
    {
        $variables = get_object_vars($this);

        unset($variables['sqlStr'], $variables['sqlSelectStr'], $variables['sqlJoinsStr'],
            $variables['sqlConditionStr'], $variables['sqlSortDirectStr'], $variables['sqlLimitStr'], $variables['sqlParamsArr'], $variables['numError'],
            $variables['isError'], $variables['sqlMessage'], $variables['isNewRecord'], $variables['isNeedUpdate'], $variables['isNeedInsert']);

        return $variables;
    }

}
