<?php

namespace application\core;

class View {

	public $path, $route, $title, $params;
	public $layout = 'default';

	public function __construct($route) {
		$this->route = $route;
		$this->path = $route['controller'].'/'.$route['action'];
	}

	public function render($vars = []) {
		extract($vars);
		$path = __DIR__ . '/../views/'.$this->path.'.php';
		if (file_exists($path)) {
			ob_start();
			require $path;
			$content = ob_get_clean();
			require __DIR__ . '/../views/layouts/'.$this->layout.'.php';
		}
	}

    public function echoContent($vars = []) {
        extract($vars);
        $path = __DIR__ . '/../views/'.$this->path.'.php';
        if (file_exists($path)) {
            ob_start();
            if (!empty($get_needed_css) && is_array($get_needed_css)) {
                foreach ($get_needed_css as $file) {
                    echo '<link href="' . $file . '" rel="stylesheet">';
                }
            }
            require $path;
            if (!empty($get_needed_js) && is_array($get_needed_js)) {
                foreach ($get_needed_js as $file) {
                    echo '<script src="' . $file . '"></script>';
                }
            }
            $content = ob_get_clean();
            echo $content;
        }
    }

	public function redirect($url) {
		header('location: ' . __URL_PREFIX__ . '/' . $url);
		exit;
	}

    public static function redirectToLogin() {
        header('location: ' . __URL_PREFIX__ . '/login');
        exit;
    }

	public static function errorCode($code) {
		http_response_code($code);
		$path = __DIR__ . '/../views/errors/'.$code.'.php';
		if (file_exists($path)) {
			require $path;
		}
		exit;
	}

	public function echoJson($status, $message, $data = []) {
	    $result = ['status' => $status, 'message' => $message];
	    if (!empty($data)) {
            $result['data'] = $data;
        }
		exit(json_encode($result));
	}

    public function echoHtml($status, $message) {
	    $class = ($status == 'error') ? 'has-error' : '';
        echo '<div class="' . $class . '">' . $message . '</div>';
    }

	public function location($url) {
		exit(json_encode(['url' => $url]));
	}

}	