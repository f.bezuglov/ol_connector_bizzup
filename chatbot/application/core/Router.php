<?php

namespace application\core;

class Router {

    protected $routes = [];
    protected $params = [];

    public function __construct() {

        $arr = require __DIR__ . '/../config/routes.php';
        foreach ($arr as $key => $val) {
            $this->add($key, $val);
        }

    }

    public function add($route, $params) {
        $route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $route);
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }

    public function match($url) {
        $fullUrl = empty($url) ? $_SERVER['REQUEST_URI'] : $url;

        $posFromIndex = strpos($fullUrl, __URL_PREFIX__);

        if ($posFromIndex !== 0) {
            return false;
        }
        if ($posFromIndex === 0) {
            $count = strlen(__URL_PREFIX__);
            $url = trim(substr($fullUrl, $count), '/');
        }

        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                if(!empty($matches['params'])) {
                    parse_str(parse_url('?' . $matches['params'], PHP_URL_QUERY), $urlParam );
                    $routeParams['params'] = $urlParam;
                    $params = array_merge($params, $routeParams);
                }
                $this->params = $params;

                return true;
            }
        }
        return false;
    }

    public function run( $url = '' ){

        if ($this->match($url)) {
            $path = 'application\controllers\\'.ucfirst($this->params['controller']).'Controller';
            if (class_exists($path)) {
                $action = $this->params['action'].'Action';

                if (method_exists($path, $action)) {
                    $controller = new $path($this->params);

                    if (!empty($this->params['params'])) {
                        $controller->$action($this->params['params']);
                    } else {
                        $controller->$action();
                    }

                } else {
                    View::errorCode(404);
                }
            } else {
               View::errorCode(404);
            }
        } else {
           View::errorCode(404);
        }
    }
}