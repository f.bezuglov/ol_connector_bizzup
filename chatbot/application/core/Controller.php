<?php

namespace application\core;

use application\models\Authentications;

abstract class Controller {

	public $route;
	public $view;

	public abstract static function acls();

	public function __construct($route) {
		$this->route = $route;

		if (!(
            isset($_SESSION['curUser'])
            || $this->route['action'] == 'login'
            || $this->route['controller'] == 'bitrix'
            || ($this->route['controller'] == 'chats' && $this->route['action'] == 'addChat')
            || ($this->route['controller'] == 'chatsMessages' && $this->route['action'] == 'addChatMessage')
        ))
		{
            View::redirectToLogin();
        } else if (!$this->checkAclAction($this->route['action'])) {
			View::errorCode(403);
		}

        // для запуска настройки конфигурации в битриксе
        if ($this->route['controller'] == 'bitrix' && $this->route['action'] != 'install') {
            // в pjaxе нет $_POST['member_id'] но есть $_GET['member_id']
            if (empty($_POST['member_id']) && !empty($_GET['member_id'])) {
                $_POST['member_id'] = $_GET['member_id'];
            }
            if (!Authentications::checkAuthMemberIdExist($_POST['member_id'] ?? 0)) {
                exit('Доступ запрещен');
            }
        }

        // для вызова добавления чата и сообщения чата
        if (($this->route['controller'] == 'chats' && $this->route['action'] == 'addChat')
            || ($this->route['controller'] == 'chatsMessages' && $this->route['action'] == 'addChatMessage')) {
            if (empty($_POST['member_id']) && !empty($_POST['auth']['member_id'])) {
                $_POST['member_id'] = $_POST['auth']['member_id'];
            }
            if (!Authentications::checkAuthMemberIdExist($_POST['member_id'] ?? 0)) {
                exit('Доступ запрещен');
            }
        }

        $this->view = new View($route);
		static::TOKENS();
	}

	public static function checkAclAction($action) {
	    $result = false;
		$acls = static::acls();

		foreach ($acls as $acl) {
		    if (!empty($acl['allow'])
                && !empty($acl['actions'])
                && in_array($action, $acl['actions'])
                && !empty($acl['roles'])) {
                if (in_array('?', $acl['roles'])) {
                    $result = true;
                } elseif (in_array('@', $acl['roles'])) {
                    $result = true;
                } elseif (in_array($_SESSION['curUser']['role'], $acl['roles'])) {
                    $result = true;
                }
            }
        }
        return $result;
	}

    public static function checkAclPrivilege($privilege) {
        $result = false;
        $acls = static::acls();

        foreach ($acls as $acl) {
            if (!empty($acl['allow'])
                && !empty($acl['privileges'])
                && in_array($privilege, $acl['privileges'])
                && !empty($acl['roles'])) {
                if (in_array('?', $acl['roles'])) {
                    $result = true;
                } elseif (in_array('@', $acl['roles'])) {
                    $result = true;
                } elseif (in_array($_SESSION['curUser']['role'], $acl['roles'])) {
                    $result = true;
                }
            }
        }
        return $result;
    }

    // устанавливает или проверяет токен
    public static function TOKENS ($act="set") {
        if (($act=="set") && (!isset($_SESSION['token']))) {
            $_SESSION['token'] = md5(uniqid(__PEPPER__));
            return true;
        }
        if (($act=="get") && isset($_SESSION['token'])) {
            return ( !empty($_POST['token']) && ($_POST['token'] == $_SESSION['token']) ) ||
                    ( !empty($_GET['token'])  && ( $_GET['token'] == $_SESSION['token']) );
        }
        return false;
    }

}