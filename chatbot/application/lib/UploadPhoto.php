<?php

namespace application\lib;

use Imagine\Image\Box;
use Exception;

class UploadPhoto {

    public $message;
    private $path, $url, $w, $h, $x, $y, $x2, $y2, $file;

    private $maxSize = 2097152;
    private $extensions = 'jpeg, jpg, png, gif';
    private $width = 200;
    private $height = 200;
    private $jpegQuality = 100;
    private $pngCompressionLevel = 1;
    private $imageName, $imageTmpName, $imageType, $imageSize, $imageError;

    public function __construct()
    {
        if (__UPLOAD_PHOTO_URL__ === null) {
            throw new Exception('Отсутствует аттрибут url');
        } else {
            $this->url = rtrim(__UPLOAD_PHOTO_URL__, '/') . '/';
        }
        if (__UPLOAD_PHOTO_PATH__ === null) {
            throw new Exception('Отсутствует аттрибут path');
        } else {
            $this->path = __DIR__ . '/../../../html/mres' . rtrim(__UPLOAD_PHOTO_PATH__, '/') . '/';
        }
    }

    public function loadFromPost()
    {
        if ($_POST && is_array($_POST)) {
            if (!isset($_FILES['file'])) {
                $this->message = 'Ошибка файла';
                return false;
            }
            $this->imageName = $_FILES['file']['name'];
            $this->imageTmpName = $_FILES['file']['tmp_name'];
            $this->imageType = isset($_FILES['file']['type']) ? str_replace('image/', '', $_FILES['file']['type']) : '';
            $this->imageSize = $_FILES['file']['size'];
            $this->imageError = $_FILES['file']['error'];


            $this->x = $_POST['x'] ?? 0;
            $this->y = $_POST['y'] ?? 0;
            $this->x2 = $_POST['x2'] ?? 0;
            $this->y2 = $_POST['y2'] ?? 0;
            if (empty($_POST['w']) || empty($_POST['h'])) {
                $this->w = $this->x;
                $this->h = $this->x;
                $this->x = 0;
                $this->y = 0;
            } else {
                $this->w = $_POST['w'];
                $this->h = $_POST['h'];
            }


            return true;
        }
        $this->message = 'Пустой POST запрос';
        return false;
    }

    public function validate() {
        if ($this->imageSize > $this->maxSize ) {
            $this->message = 'Превышен допустимый размер загружаемого файла (' . $this->maxSize . ' Мб)';
            return false;
        }
        $extensionArr = explode(', ', $this->extensions);
        if (!in_array($this->imageType, $extensionArr)) {
            $this->message = 'Разрешены только следующие форматы файлов: ' . $this->extensions;
            return false;
        }
        return true;
    }

    public function run()
    {
        $this->imageName = uniqid() . '.' . $this->imageType;

        $image = BaseImage::crop(
            $this->imageTmpName,
            intval($this->w),
            intval($this->h),
            [$this->x, $this->y]
        )->resize(
            new Box($this->width, $this->height)
        );

        if (!file_exists($this->path) || !is_dir($this->path)) {
            $this->message = 'Неверно указана папка для сохранения: ' . $this->path;
            return false;
        } else {
            $saveOptions = ['jpeg_quality' => $this->jpegQuality, 'png_compression_level' => $this->pngCompressionLevel];
            if ($image->save($this->path . $this->imageName, $saveOptions)) {
                return $this->url . $this->imageName;
            } else {
                $this->message = 'Невозможно загрузить файл';
                return false;
            }
        }
    }


}
