<?php

namespace application\lib;

use DateTime;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Export2Xls {

    private $models, $title, $styleBorders;

    public function __construct($models, $title)
    {

        $this->models = (!empty($models) && is_array($models)) ? $models : [];
        $this->title  = $title ?? '';
        $this->styleBorders = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                /*'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                ],*/
            ],
        ];
    }

    public function run()
    {
        $oSpreadsheet_Out = new Spreadsheet();

        $oSpreadsheet_Out->createSheet(0);
        $oSpreadsheet_Out->setActiveSheetIndex(0);
        $activeSheet = $oSpreadsheet_Out->getActiveSheet();
        $activeSheet->setTitle($this->title);

        $curClass = get_class($this->models[0]);
        $exportVariables = method_exists($curClass, 'exportVariablesParams') ? $curClass::exportVariablesParams() : [];
        $columnIndex = 1;
        foreach ($exportVariables as $variableName=>$params) {
            $label = $this->models[0]->attributeLabels()[$variableName] ?? '';
            $activeSheet->getColumnDimensionByColumn($columnIndex)->setWidth($params['width']);
            $activeSheet->getRowDimension(1)->setRowHeight(30);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $activeSheet->setCellValueByColumnAndRow($columnIndex, 1, $label);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getFont()->setBold(true);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->applyFromArray($this->styleBorders);
            $activeSheet->getStyleByColumnAndRow($columnIndex, 1)->getAlignment()->setWrapText(true);
            $columnIndex++;
        }
        $rowIndex = 2;
        foreach ($this->models as $model) {
            $columnIndex = 1;
            foreach ($exportVariables as $variableName=>$params) {
                $value = isset($params['value']) ? $params['value']($model->$variableName) : $model->$variableName;
                $activeSheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $value);
                $activeSheet->getStyleByColumnAndRow($columnIndex, $rowIndex)->getAlignment()->setHorizontal($params['alignment']);
                $activeSheet->getStyleByColumnAndRow($columnIndex, $rowIndex)->applyFromArray($this->styleBorders);
                $columnIndex++;
            }
            $rowIndex++;
        }

        $nowDateTime = new DateTime();

        $outFileName = __DIR__ . '/../../uploads/' . $nowDateTime->format('U') . '.xlsx';
        $oWriter = IOFactory::createWriter($oSpreadsheet_Out, 'Xlsx');
        $oWriter->save($outFileName);
        if(file_exists($outFileName)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($outFileName).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($outFileName));
            flush(); // Flush system output buffer
            readfile($outFileName);
        }
        unlink($outFileName);

    }
}
