<?php

namespace application\lib;

use Exception;
use PDO;

class Db
{
    public bool $isError;
    public string $sqlMessage;
    public int $numError;
    public bool $isNeedUpdate;
    public bool $isNeedInsert;
    public bool $isNewRecord;
    protected static PDO $PDO;

    function __construct()
    {
        if (!isset(self::$PDO)) {
            $config = require __DIR__ . '/../config/db.php';
            self::$PDO = new PDO('pgsql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['name'] . ';keepalives_idle=60', $config['user'], $config['password']);
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        $this->isNewRecord = true;
        $this->isNeedUpdate = false;
        $this->isNeedInsert = false;
    }


    public function dbLastInsertId()
    {
        return self::$PDO->lastInsertId();
    }

    public function dbBeginTransaction(): void
    {
        self::$PDO->beginTransaction();
    }

    public function dbRollBack(): void
    {
        if (self::$PDO->inTransaction()) {
            self::$PDO->rollBack();
        }
    }

    public function dbInTransaction(): void
    {
        self::$PDO->inTransaction();
    }

    public function dbCommit(): bool
    {
        return self::$PDO->commit();
    }


    public function execute($sql, $params = [])
    {
        $this->isError = false;
        $this->sqlMessage = '';
        $stmt = self::$PDO->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                if ($val === 'null') {
                    $stmt->bindValue(":$key", null, PDO::PARAM_NULL);
                } elseif (is_int($val)) {
                    $stmt->bindValue(":$key", $val, PDO::PARAM_INT);
                } elseif (is_numeric($val)) {
                    $stmt->bindValue(":$key", $val, PDO::PARAM_STR);
                } elseif (is_bool($val)) {
                    $stmt->bindValue(":$key", $val, PDO::PARAM_BOOL);
                } elseif (is_null($val)) {
                    $stmt->bindValue(":$key", $val, PDO::PARAM_NULL);
                } elseif (is_string($val)) {
                    $stmt->bindValue(":$key", $val, PDO::PARAM_STR);
                } elseif (is_array($val)) {
                    $stmt->bindValue(":$key", $this->to_pg_array($val), PDO::PARAM_STR);
                }
            }
        }

        $stmt->execute();

        return $stmt;
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function fetchAll($sql, array $params = []): array
    {
        if ($result = $this->execute($sql, $params)) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        return [];
    }

    /**
     * @param $sql
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function fetchColumn($sql, array $params = [])
    {
        if ($result = $this->execute($sql, $params)) {
            return $result->fetchColumn();
        }
        return 0;
    }

    public function to_pg_array($set): string
    {
        settype($set, 'array'); // can be called with a scalar or array
        $result = array();
        foreach ($set as $t) {
            if (empty($t)) {
                continue;
            }
            if (is_array($t)) {
                $result[] = $this->to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t); // escape double quote
                if (!is_numeric($t)) // quote only non-numeric values
                    $t = '"' . $t . '"';
                $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}'; // format
    }

    /**
     * @throws Exception
     */
    public function getBdSize()
    {
        $config = require __DIR__ . '/../config/db.php';
        $sql = "SELECT pg_size_pretty(pg_database_size('{$config['name']}'))";
        return $this->fetchColumn($sql);
    }
}