<?php

/**
 * Функции для подключения к Wazzup24 и вызова API
 *
 */

namespace application\lib;

set_time_limit(2400);

use Exception;

class Wazzup24
{

    const TIMEOUT_SOCKET = 15;
    /**
     * @var string url api wazzup
     *
     */
    const API_URL = 'https://api.wazzup24.com/v3/message';

    /**
     * @var string Тип канала. Доступные значения:
     * INSTAGRAM: ‘instagram’,
     * TELEGRAM: ‘telegram’,
     * Групповой чат в Telegram: ‘telegroup’,
     * WHATSAPP: ‘whatsapp’,
     * Групповой чат WhatsApp: ‘whatsgroup’
     *
     */
    const CHAT_TYPE = 'whatsapp';

    /**
     * @var string Ключ апи wazzup
     *
     */
    private $apiKey;

    /**
     * @var string Id канала (uuidv4), через который нужно отправить сообщение
     *
     */
    private $channelId;

    /**
     * @var string Id (uuidv4) шаблона сообщения, которое нужно отправить
     *
     */
    private $template;

    /**
     * @var string Id чата (аккаунт контакта в мессенджере):
     *
     * для whatsapp — только цифры, без пробелов и спец. символов в формате 79011112233
     * для instagram — аккаунт без «@» вначале
     * для группового чата WhatsApp (‘whatsgroup’) — приходит в вебхуках входящих сообщений
     *
     */
    private $chatId;

    /**
     * @var string сервис коротких ссылок
     *
     */
    const SERVICE_SHORT_LINK = 'https://bzzp.ru/get-short-url.php?url=';

    /**
     * @var string url подтверждения пользователем прочтения сообщения
     *
     */
    const SELF_REQUEST_URL = '/mres/mis.php/request-confirm';

    /**
     * @var string Текст сообщения wazzup24.
     *
     */
    private $text;

    private $token;

    public $isError = false;
    public $message, $numError;

    public function run($phone, $domain, $token, $step)
    {
        $this->chatId = $phone;

        $config = require __DIR__ . '/../config/wazzup24.php';
        if (empty($config) || !is_array($config)) {
            $this->isError = true;
            $this->message = 'Отсутствует конфигурационный файл: ' . __DIR__ . '/../config/wazzup24.php';
            return false;
        }

        $this->apiKey = $config['apiKey'] ?? '';
        if (empty($this->apiKey)) {
            $this->isError = true;
            $this->message = 'Отсутствует параметр apiKey в конфигурационном файле: ' . __DIR__ . '/../config/wazzup24.php';
            return false;
        }

        $this->channelId = $config['channelId'] ?? '';
        if (empty($this->channelId)) {
            $this->isError = true;
            $this->message = 'Отсутствует параметр channelId в конфигурационном файле: ' . __DIR__ . '/../config/wazzup24.php';
            return false;
        }

        $this->template = $config['templates'][$step] ?? '';
        if (empty($this->template)) {
            $this->isError = true;
            $this->message = "Отсутствует параметр templates шаг $step в конфигурационном файле: " . __DIR__ . "/../config/wazzup24.php";
            return false;
        }

        if ($step == 1) {
            $longSelfRequestUrl = 'https://' . $_SERVER['HTTP_HOST'] . self::SELF_REQUEST_URL . '?token=' . $token;

            $selfRequestUrl = $this->getShortUrl($longSelfRequestUrl);
            if ($selfRequestUrl === false) {
                return false;
            }

            $url = parse_url($selfRequestUrl);

            $this->text = '@template: ' . $this->template . '{ [[https://' . $domain . ']]; [[' . trim($url['path'], '/') . ']] }';
        } else {
            $this->text = '@template: ' . $this->template . '{ [[https://' . $domain . ']] }';
        }

        return $this->callCurl();
    }

    private function callCurl()
    {

        $this->isError = false;
        $this->message = null;

        $post = [
            'chatType'  => self::CHAT_TYPE,
            'chatId'    => $this->chatId,
            'text' => $this->text,
            'channelId' => $this->channelId
        ];

        $postData = json_encode($post);

        // curl --location --request POST https://api.wazzup24.com/v3/message --header 'Authorization: Bearer 6d9d6d00c0e44223bcda8dfea5b3c8fb' --header 'Content-Type: application/json' --data-raw '{"channelId":"254c9878-d1b6-47d3-b9ad-0b1ffccfaedc","chatType":"whatsapp","chatId":"77778490510","text":"kuku"}'

        try {
            $ch = null;
            if (!($curl = curl_init())) {
                throw new Exception('err_curlinit');
            }

            if (curl_errno($curl) != 0) {
                throw new Exception('err_curlinit' . curl_errno($ch) . ' ' . curl_error($ch));
            }

            curl_setopt($curl, CURLOPT_URL, self::API_URL);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Authorization: Bearer " . $this->apiKey,
                "Content-Type: application/json",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);

            $resultRaw = curl_exec($curl);

            curl_close($curl);

            $result = json_decode($resultRaw ?? '', true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new Exception("Неверный ответ сервера: " . $resultRaw);
            }

            if (isset($result['messageId'])) {
                return $result;
            } else {
                throw new Exception("Неизвестная ошибка. Выход из обработки.");
            }
        } catch (Exception $ex) {
            $this->isError  = true;
            $this->message  = $ex->getMessage();
            $this->numError = $ex->getCode();
            return false;
        }
    }

    private function getShortUrl($longSelfRequestUrl)
    {
        $urlLong = self::SERVICE_SHORT_LINK . $longSelfRequestUrl;

        $url = @file_get_contents($urlLong);
        if ($url === false) {
            $this->isError = true;
            $this->message = 'Ссылка ' . self::SERVICE_SHORT_LINK . $longSelfRequestUrl . ' не найдена';
        }
        return $url;

    }

}

