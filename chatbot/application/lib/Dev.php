<?php

ini_set('xdebug.var_display_max_depth', '-1');
ini_set('xdebug.var_display_max_children', '-1');
ini_set('xdebug.var_display_max_data', '-1');

ini_set('display_errors', 1);
error_reporting(E_ALL);

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');

function debug($str) {
	echo '<pre>';
	var_dump($str);
	echo '</pre>';
	exit;
}