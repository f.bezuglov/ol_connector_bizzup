<?php

namespace application\lib;

class Pagination {
    private $max;
    private $amount, $currentPage, $route, $totalRow, $limitRow, $startRow, $endRow;

    public function __construct($totalRow, $limitRow, $route, $max = 10) {
        $this->totalRow = $totalRow;
        $this->limitRow = $limitRow;
        $this->route = $route;
        $this->max = $max;
        $this->amount = $this->amount();
        $this->setCurrentPage();
        $this->calculateStartRow();
    }

    public function getHeader() {
        $html = 'Показаны <b>' . $this->startRow . '-' . $this->endRow . '</b> из <b>' . $this->totalRow . '</b> записи.';
        return $html;
    }

    public function getFooter() {
        $links = null;
        $limits = $this->limits();
        $html = '<nav><ul class="pagination">';
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {
            if ($page == $this->currentPage) {
                $links .= '<li class="page-item active"><span class="page-link">'.$page.'</span></li>';
            } else {
                $links .= $this->generateHtml($page);
            }
        }
        if (!is_null($links)) {
            if ($this->currentPage > 1) {
                $links = $this->generateHtml(1, 'Назад').$links;
            }
            if ($this->currentPage < $this->amount) {
                $links .= $this->generateHtml($this->amount, 'Вперед');
            }
        }
        $html .= $links.' </ul></nav>';
        return $html;
    }

    private function generateHtml($page, $text = null) {
        if (!$text) {
            $text = $page;
        }
        $paramsStr = '';
        if (!empty($this->route['params']) && is_array($this->route['params'])) {
            foreach ($this->route['params'] as $paramName=>$paramValue) {
                if (is_string($paramValue)) {
                    if ($paramName == 'page') { continue; }
                    $paramsStr .= '&' . $paramName . '=' . $paramValue;
                } elseif (is_array($paramValue)) {
                    foreach ($paramValue as $subParamName=>$subParamValue) {
                        if (is_string($subParamValue)) {
                            $paramsStr .= '&' . $paramName . '[' . $subParamName . ']=' . $subParamValue;
                        }
                    }
                }

            }
        }

        return '<li class="page-item"><a class="page-link" data-page="'.$page.$paramsStr.'" href="/'.$this->route['controller'].'/'.$this->route['action'].'?page='.$page.$paramsStr.'">'.$text.'</a></li>';
    }

    private function limits() {
        $left = $this->currentPage - round($this->max / 2);
        $start = $left > 0 ? $left : 1;
        if ($start + $this->max <= $this->amount) {
            $end = $start > 1 ? $start + $this->max : $this->max;
        }
        else {
            $end = $this->amount;
            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }
        return array($start, $end);
    }

    private function setCurrentPage() {
        if (isset($_GET['page'])) {
            $currentPage = (int)$_GET['page'];
        } else {
            $currentPage = 1;
        }
        $this->currentPage = $currentPage;
        if ($this->currentPage > 0) {
            if ($this->currentPage > $this->amount) {
                $this->currentPage = $this->amount;
            }
        } else {
            $this->currentPage = 1;
        }
    }

    private function amount() {
        if ($this->totalRow > 0 && $this->limitRow > 0) {
            $amount = (int)ceil($this->totalRow / $this->limitRow);
        } else {
            $amount = 1;
        }
        return $amount;
    }

    private function calculateStartRow() {
        $this->startRow = (((($this->currentPage - 1) * $this->limitRow) + 1) > $this->totalRow) ? $this->totalRow : ((($this->currentPage - 1) * $this->limitRow) + 1);
        $this->endRow = (($this->currentPage  * $this->limitRow) > $this->totalRow) ? $this->totalRow : ($this->currentPage  * $this->limitRow);

    }

    public function getStartRow() {
        return ($this->totalRow == 0) ? $this->startRow : $this->startRow - 1;
    }

    public function getLimitRow() {
        return $this->limitRow;
    }
}