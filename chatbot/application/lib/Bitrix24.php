<?php

/**
 * Функции для подключения к Битрикс24 и вызова API
 *
 */

namespace application\lib;

use application\models\Authentications;
use application\models\Configs;
use application\models\Logs;
use Exception;

class Bitrix24
{
    const VERSION = '0.01', BATCH_COUNT = 50, TIMEOUT_SOCKET = 30;
    private $minutesLastCall, $tenMinutesFirstCall, $tenMinutesTotalCall;

    public $method, $arDataRest;

    private $isRetrySend = false;
    private $isRetrySendNewAuth = false;
    
    public $next = 0;
    public $isError = false;
    public $sqlMessage, $numError, $batch;
    protected $webHook, $accessToken, $member_id;
    private $timeProcessing = 0;
    public $timeBegin, $timeEnd;

    public function __construct($member_id)
    {
        if (empty($_SESSION['auth'][$member_id])) {
            exit('Критическая ошибка: отсутствуют аутентификация к апи битрикс');
        }
        $this->member_id = $member_id;
        $this->accessToken = $_SESSION['auth'][$member_id]['access_token'];
        $this->minutesLastCall = microtime(true);
    }

    /**
     * Отправка запроса в битрикс
     *
     * @return array
     * @throws Exception
     *
     */
    private function callCurl()
    {
        ini_set('max_execution_time', 900);
        set_time_limit(2400);

        // Ожидание полсекунды, так как битрикс не любит много запросов
        $this->synchronize();

        $this->isError = false;

        if (!empty($this->webHook)) {
            $url = $this->webHook . $this->method . '.json';
            $postFields = $this->arDataRest;
        } else {
            $clientEndpoint = 'https://' . $_SESSION['auth'][$this->member_id]['domain'] . '/rest/';
            $url = $clientEndpoint . $this->method . '.json';
            $postFields = $this->arDataRest;
            $postFields['auth'] = $this->accessToken;
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTREDIR, 10);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Bizzup MRES v 0.01');
        if ($postFields) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT_SOCKET * 60);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_SOCKET);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

        $resultRaw = curl_exec($ch);

        if (curl_errno($ch)) {
            if (!$this->isRetrySend) {
                sleep(3);
                $this->isRetrySend = true;
                return $this->callCurl();
            }
            throw new Exception('curl curl_errno ' . curl_errno($ch) . '. Ответ от curl:  ' . curl_error($ch));
        }

        $result = json_decode($resultRaw ?? '', true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            if (!$this->isRetrySend) {
                sleep(3);
                $this->isRetrySend = true;
                return $this->callCurl();
            }
            throw new Exception('curl json_error. Ответ с битрикса: ' . $resultRaw);
        }

        curl_close($ch);

        if (!empty($result['error']) && $result['error'] == 'expired_token') {
            $this->getNewAuth();
            return $this->callCurl();
        }

        if (!empty($result['error'])) {
            $messageArr = [];
            $messageArr[] = $result['error'];
            if (!empty($result['error_description'])) {
                $messageArr[] = $result['error_description'];
            }
            $message = implode(': ', $messageArr);
            throw new Exception($message);
        }

        if ($this->method == 'batch' && !empty($result['result']['result_error']) && is_array($result['result']['result_error'])) {

            $message = '';
            foreach ($result['result']['result_error'] as $key => $error) {
                $messageArr = [];
                if (!empty($error['error'])) {
                    if ($error['error'] == 'OPERATION_TIME_LIMIT') {
                        sleep(3);
                        return $this->callCurl();
                    }
                    $messageArr[] = $error['error'];
                }
                if (!empty($error['error_description'])) {
                    $messageArr[] = $error['error_description'];
                }
                $message .= !empty($message) ? (' | ' . $key . ' ' . implode(': ', $messageArr)) : ($key . ' ' . implode(': ', $messageArr));
            }
            throw new Exception($message);
        }

        $this->tenMinutesTotalCall += floatval($result['time']['processing'] ?? 0);
        $this->isRetrySend         = false;

        return $result;
    }

    /**
     * Получение нового ключа авторизации
     *
     * @throws Exception
     * @return void
     *
     */
    public function getNewAuth(): void
    {
        if (empty($this->accessToken)) {
            throw new Exception('Пустая авторизация. Переустановите приложение.');
        }

        $client_id = $client_secret = '';
        if (!empty($_SESSION['auth'][$this->member_id]['client_id']) && !empty($_SESSION['auth'][$this->member_id]['client_secret'])) {
            $client_id = $_SESSION['auth'][$this->member_id]['client_id'];
            $client_secret = $_SESSION['auth'][$this->member_id]['client_secret'];
        } else {
            if (file_exists(__DIR__ . '/../config/bitrix24.php')) {
                $config = require __DIR__ . '/../config/bitrix24.php';
                if (empty($config['client_id']) || empty($config['client_secret'])) {
                    throw new Exception('Проверьте константы битрикс24 для продления ключей авторизации в файле ' . __DIR__ . '/../config/bitrix24.php');
                }
                $client_id = $config['client_id'];
                $client_secret = $config['client_secret'];
            } else {
                throw new Exception('Отсутствует файл c константами битрикс24 для продления ключей авторизации');
            }
        }

        if (empty($_SESSION['auth'][$this->member_id]['refresh_token'])) {
            throw new Exception('Отсутствует refresh_token в сессии.');
        }
        $params = [
            'client_id'     => $client_id,
            'grant_type'    => 'refresh_token',
            'client_secret' => $client_secret,
            'refresh_token' => $_SESSION['auth'][$this->member_id]['refresh_token'],
        ];

        $target_url = 'https://oauth.bitrix.info/oauth/token/';
        $post       = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $target_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            if (!$this->isRetrySendNewAuth) {
                sleep(60);
                $this->isRetrySendNewAuth = true;
                $this->getNewAuth();
            }
            throw new Exception('Ошибка продления ключей авторизации битрикс24: ' . curl_error($ch));
        }

        curl_close($ch);
        $result = json_decode($result, true);

        if (!empty($result['error']) || !empty($result['error_description'])) {
            if (!$this->isRetrySendNewAuth) {
                sleep(60);
                $this->isRetrySendNewAuth = true;
                $this->getNewAuth();
            }

            $messageArr = [];
            if (!empty($result['error'])) {
                $messageArr[] = $result['error'];
            }
            if (!empty($result['error_description'])) {
                $messageArr[] = $result['error_description'];
            }
            $message = implode(': ', $messageArr);

            throw new Exception('Ошибка продления ключей авторизации битрикс24: ' . $message);
        }
        if (empty($result['access_token']) || empty($result['refresh_token']) || empty($result['expires_in'])) {
            if (!$this->isRetrySendNewAuth) {
                sleep(60);
                $this->isRetrySendNewAuth = true;
                $this->getNewAuth();
            }
            throw new Exception('Ошибка при обновлении токена');
        }

        $_SESSION['auth'][$this->member_id]['access_token']  = $this->accessToken = $result['access_token'];
        $_SESSION['auth'][$this->member_id]['refresh_token'] = $result['refresh_token'];

        $authenticationsModel                = new Authentications();
        $authenticationsModel->member_id     = $this->member_id;
        $authenticationsModel->access_token  = $result['access_token'];
        $authenticationsModel->refresh_token = $result['refresh_token'];
        $authenticationsModel->expires_in    = $result['expires_in'];
        $authenticationsModel->updateRowTokens();

        $this->isRetrySendNewAuth = false;

    }

    /*
     * Ожидание полсекунды, так как битрикс не любит много запросов
     *
     */
    public function synchronize()
    {
        // запрос возможен два раза в секунду
        if ((microtime(true) - $this->minutesLastCall) < 0.7) {
            $sleep = 0.7 - (microtime(true) - $this->minutesLastCall);
            usleep(ceil($sleep * 1000000));
        }
        $this->minutesLastCall = microtime(true);


        ///////////////////////////////////////////////////////////////////////////////////////
        // * Начиная с REST-модуля 22.0.0 в облачной версии Битрикс24 ко всем ответам REST-запросов в массиве времени
        // * operating добавлен дополнительный ключ с дополнительной информацией о времени выполнения запроса.
        // * Ключ сообщает вам о времени выполнения запроса к методу внутри портала.
        // * Данные о времени выполнения запросов к методу суммируются. Потом проверяется.
        // * Если время запроса всех методов превышает 480 секунд за последние 10 минут, метод блокируется на 10 минут.
        ///
        ///  OPERATION_TIME_LIMIT: Method is blocked due to operation time limit.
        ///

        $maxSeconds        = 400;
        $tenMinutesSeconds = 680;
        /*
                // специально для Наджи. У них битрикс какой то уставший
                if ($this->member_id == 'd1fa47dee05587f0fe4313802c80c2a6') {
                    $maxSeconds = 200;
                    $tenMinutesSeconds = 640;
                }*/


        if (!isset($this->tenMinutesFirstCall)) {
            $this->tenMinutesFirstCall = microtime(true);
        }
        // Если прошло 10 минут то сбрасываем счетчики
        if ((microtime(true) - $this->tenMinutesFirstCall) >= $tenMinutesSeconds) {
            $this->tenMinutesTotalCall = 0;
            $this->tenMinutesFirstCall = microtime(true);
        }


        // Если время запроса всех методов превышает 480 секунд то ожидание
        if ($this->tenMinutesTotalCall > $maxSeconds) {
            // подсчитаем сколько осталось до окончания 10 минут и столько спим
            $lastTime = $tenMinutesSeconds - (microtime(true) - $this->tenMinutesFirstCall);
            echo "Ожидание {$lastTime} секунд\n";
            sleep(ceil($lastTime));
            // сбрасываем счетчики
            $this->tenMinutesTotalCall = 0;
            $this->tenMinutesFirstCall = microtime(true);
        }
    }

    /**
     * Generate a request for callCurl()
     *
     * @var $arData array
     * @var $halt   integer 0 or 1 stop batch on error
     *
     * @throws Exception
     * @return array
     *
     */
    public function call($method, $arDataRest = null)
    {
        $this->method     = $method;
        $this->arDataRest = $arDataRest;
        $result           = $this->callCurl();
        $this->next       = $result['next'] ?? 0;
        $this->total      = $result['total'] ?? 0;

        return $result;
    }

    /**
     * Generate a request for callCurl()
     *
     * @throws Exception
     * @var array $arData
     * @var integer $halt    0 or 1 stop batch on error
     * @return array
     *
     * @example $arData:
     * $arData = [
     *      'find_contact' => [
     *          'method' => 'crm.duplicate.findbycomm',
     *          'params' => [ "entity_type" => "CONTACT",  "type" => "EMAIL", "values" => array("info@bitrix24.com") ]
     *      ],
     *      'get_contact' => [
     *          'method' => 'crm.contact.get',
     *          'params' => [ "id" => '$result[find_contact][CONTACT][0]' ]
     *      ],
     *      'get_company' => [
     *          'method' => 'crm.company.get',
     *          'params' => [ "id" => '$result[get_contact][COMPANY_ID]', "select" => ["*"],]
     *      ]
     * ];
     *
     */
    public function callBatch(array $arData, int $halt = 0): array
    {
        $arResult = [];
        $arDataRest = [];
        $i          = 0;
        foreach ($arData as $key => $data) {
            if (!empty($data['method'])) {
                $i++;
                if (static::BATCH_COUNT >= $i) {
                    $arDataRest['cmd'][$key] = $data['method'];
                    if (!empty($data['params'])) {
                        $arDataRest['cmd'][$key] .= '?' . http_build_query($data['params']);
                    }
                }
            }
        }
        if (!empty($arDataRest)) {
            $arDataRest['halt'] = $halt;
            $this->method       = 'batch';
            $this->arDataRest   = $arDataRest;

            $arResult = $this->callCurl();
        }

        return $arResult;
    }

    public function install()
    {
        $batch[0]['method'] = 'imbot.register';
        $batch[0]['params'] = [
            'CODE' => 'BOT_CHATFILTER',
            'TYPE' => 'O',
            'EVENT_HANDLER' => __APP_HANDLER__,
            'EVENT_BOT_DELETE' => __APP_HANDLER__,
            'PROPERTIES' => [ // Личные данные чат-бота (обяз.)
                'NAME' => 'Бот Павел', // Имя чат-бота (обязательное одно из полей NAME или LAST_NAME)
                'LAST_NAME' => '', // Фамилия чат-бота (обязательное одно из полей NAME или LAST_NAME)
                'COLOR' => 'GREEN', // Цвет чат-бота для мобильного приложения RED, GREEN, MINT, LIGHT_BLUE, DARK_BLUE, PURPLE, AQUA, PINK, LIME, BROWN,  AZURE, KHAKI, SAND, MARENGO, GRAY, GRAPHITE
                'EMAIL' => 'support@bizzup.ru', // E-mail для связи. НЕЛЬЗЯ использовать e-mail, дублирующий e-mail реальных пользователей
                'PERSONAL_BIRTHDAY' => '2021-12-21', // День рождения в формате YYYY-mm-dd
                'WORK_POSITION' => 'Чат-бот', // Занимаемая должность, используется как описание чат-бота
                'PERSONAL_WWW' => 'https://bizzup.ru', // Ссылка на сайт
                'PERSONAL_GENDER' => 'M', // Пол чат-бота, допустимые значения M -  мужской, F - женский, пусто, если не требуется указывать
                'PERSONAL_PHOTO' => base64_encode(__URL_PREFIX__ . '/images/bot-img.jpg') // Аватар чат-бота - base64
            ]
        ];

        $result = $this->callBatch($batch);

        if (!empty($result['result']['result_error'][0]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][0]['error_description']) ? $result['result']['result_error'][0]['error_description'] : $result['result']['result_error'][0]['error'];
            return false;
        }

        if (empty($result['result']['result'][0]) || !is_numeric($result['result']['result'][0])) {
            $this->isError = true;
            $this->sqlMessage = 'Неверный ответ с битрикса';
            return false;
        }

        return $result['result']['result'][0];
    }

    /**
     * @param Configs $config
     * @return array|bool|mixed
     */
    public function uninstall($config)
    {
        $result = [];

        if (!empty($config->tmpl_contact_id) && $config->tmpl_contact_id > 0) {
            $batch[0]['method'] = 'bizproc.workflow.template.delete';
            $batch[0]['params'] = [
                'ID' => $config->tmpl_contact_id
            ];
        }
        if (!empty($config->tmpl_lead_id) && $config->tmpl_lead_id > 0) {
            $batch[1]['method'] = 'bizproc.workflow.template.delete';
            $batch[1]['params'] = [
                'ID' => $config->tmpl_lead_id
            ];
        }
        if (empty($batch)) {
            return $result;
        }

        $result = $this->callBatch($batch);

        if (!empty($result['result']['result_error'][0]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][0]['error_description']) ? $result['result']['result_error'][0]['error_description'] : $result['result']['result_error'][0]['error'];
            return false;
        }

        if (!empty($result['result']['result_error'][1]['error'])) {
            $this->isError = true;
            $this->sqlMessage = !empty($result['result']['result_error'][1]['error_description']) ? $result['result']['result_error'][1]['error_description'] : $result['result']['result_error'][1]['error'];
            return false;
        }

        return $result;
    }

    public function getTemplates()
    {
        $result[0] = [];
        $result[1] = [];

        $batch[0]['method'] = 'bizproc.workflow.template.list';
        $batch[0]['params'] = [
            'select' => ['ID', 'NAME'],
            'filter' => ['MODULE_ID' => 'crm', 'ENTITY' => 'CCrmDocumentLead']
        ];
        $batch[1]['method'] = 'bizproc.workflow.template.list';
        $batch[1]['params'] = [
            'select' => ['ID', 'NAME'],
            'filter' => ['MODULE_ID' => 'crm', 'ENTITY' => 'CCrmDocumentContact']
        ];

        $answer = $this->callBatch($batch);

        if (empty($answer['result']['result_error'][0]['error'])
            && !empty($answer['result']['result'][0])
            && is_array($answer['result']['result'][0])) {
            foreach ($answer['result']['result'][0] as $key => $lead) {
                $result[0][$key]['id'] = $lead['ID'] ?? 0;
                $result[0][$key]['name'] = $lead['NAME'] ?? 0;
            }
        } else {
            $result[0][0]['id'] = 0;
            $result[0][0]['name'] = $result['result']['result_error'][0]['error'] ?? '';
        }
        if (empty($answer['result']['result_error'][1]['error'])
            && !empty($answer['result']['result'][1])
            && is_array($answer['result']['result'][1])) {
            foreach ($answer['result']['result'][1] as $key => $lead) {
                $result[1][$key]['id'] = $lead['ID'] ?? 0;
                $result[1][$key]['name'] = $lead['NAME'] ?? 0;
            }
        } else {
            $result[1][0]['id'] = 0;
            $result[1][0]['name'] = $result['result']['result_error'][1]['error'] ?? '';
        }

        return $result;
    }

    public function installTemplates()
    {
        try {

            $result['lead'] = [];
            $result['contact'] = [];

            $batch[0]['method'] = 'bizproc.workflow.template.add';
            $batch[0]['params'] = [
                'DOCUMENT_TYPE' => ['crm', 'CCrmDocumentLead', 'LEAD'],
                'NAME' => 'Сервисный чат-бот WAZZUP',
                'DESCRIPTION' => '',
                'AUTO_EXECUTE' => 0,
                'TEMPLATE_DATA' => base64_encode(file_get_contents(__WORKFLOW_TEMPLATES__ . '/tmpl-lead.bpt'))
            ];
            $batch[1]['method'] = 'bizproc.workflow.template.add';
            $batch[1]['params'] = [
                'DOCUMENT_TYPE' => ['crm', 'CCrmDocumentContact', 'CONTACT'],
                'NAME' => 'Сервисный чат-бот WAZZUP',
                'DESCRIPTION' => '',
                'AUTO_EXECUTE' => 0,
                'TEMPLATE_DATA' => base64_encode(file_get_contents(__WORKFLOW_TEMPLATES__ . '/tmpl-contact.bpt'))
            ];

            $answer = $this->callBatch($batch);

            if (empty($answer['result']['result_error'][0]['error'])
                && !empty($answer['result']['result'][0])
                && is_numeric($answer['result']['result'][0])) {
                $result['lead']['id'] = $answer['result']['result'][0];
                $result['lead']['name'] = 'Сервисный чат-бот WAZZUP';
            } else {
                if (!empty($answer['result']['result_error'][0])) {

                    $messageArr = [];
                    if (!empty($answer['result']['result_error'][0]['error'])) {
                        $messageArr[] = $answer['result']['result_error'][0]['error'];
                    }
                    if (!empty($answer['result']['result_error'][0]['error_description'])) {
                        $messageArr[] = $answer['result']['result_error'][0]['error_description'];
                    }
                    $message = implode(': ', $messageArr);

                } else {
                    $message = 'Неизвестная ошибка';
                }
                throw new Exception($message);
            }

            if (empty($answer['result']['result_error'][1]['error'])
                && !empty($answer['result']['result'][1])
                && is_numeric($answer['result']['result'][1])) {
                $result['contact']['id'] = $answer['result']['result'][1];
                $result['contact']['name'] = 'Сервисный чат-бот WAZZUP';
            } else {

                if (!empty($answer['result']['result_error'][1])) {

                    $messageArr = [];
                    if (!empty($answer['result']['result_error'][1]['error'])) {
                        $messageArr[] = $answer['result']['result_error'][1]['error'];
                    }
                    if (!empty($answer['result']['result_error'][1]['error_description'])) {
                        $messageArr[] = $answer['result']['result_error'][1]['error_description'];
                    }
                    $message = implode(': ', $messageArr);

                } else {
                    $message = 'Неизвестная ошибка';
                }
                throw new Exception($message);
            }

            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function workflowStartForContact($tmpl_contact_id, $workflow_contact_id, $message, $chat_id)
    {
        $method = 'bizproc.workflow.start';
        $params = [
            'TEMPLATE_ID' => $tmpl_contact_id,
            'DOCUMENT_ID' => ['crm', 'CCrmDocumentContact', $workflow_contact_id],
            'PARAMETERS' => ['TEXT' => $message, 'CHAT_ID' => $chat_id]
        ];

        return $this->call($method, $params);
    }

    public function workflowStartForLead($tmpl_lead_id, $workflow_lead_id, $message, $chat_id)
    {
        $method = 'bizproc.workflow.start';
        $params = [
            'TEMPLATE_ID' => $tmpl_lead_id,
            'DOCUMENT_ID' => ['crm', 'CCrmDocumentLead', $workflow_lead_id],
            'PARAMETERS' => ['TEXT' => $message, 'CHAT_ID' => $chat_id]
        ];

        return $this->call($method, $params);
    }


    public function transferDialogOpenLine($chat_id)
    {
        $method = 'imopenlines.bot.session.operator';
        $params = [
            'CHAT_ID' => $chat_id
        ];

        return $this->call($method, $params);
    }

    public function transferDialogOperator($chat_id, $workflow_type, $workflow_contact_id, $workflow_lead_id)
    {
        $method = $params = [];
        // получим id ответственного $assigned_by
        if (!empty($workflow_type) && $workflow_type == 'CONTACT' && !empty($workflow_contact_id)) {
            $method = 'crm.contact.get';
            $params = [
                'ID' => $workflow_contact_id
            ];
        }
        if (!empty($workflow_type) && $workflow_type == 'LEAD' && !empty($workflow_lead_id)) {
            $method = 'crm.lead.get';
            $params = [
                'ID' => $workflow_lead_id
            ];
        }
        if (empty($method) || empty($params)) {
            $this->isError = true;
            $this->sqlMessage = 'Отсутствуют данные для получения ответственного';
            return false;
        }

        $answer = $this->call($method, $params);
        if ($this->isError) {
            return $answer;
        }
        $assigned_by = $answer['result']['ASSIGNED_BY_ID'] ?? 0;
        if (empty($assigned_by)) {
            $this->isError = true;
            $this->sqlMessage = 'Не найден ответственный';
            return false;
        }

        $method = 'imopenlines.bot.session.transfer';
        $params = [
            'CHAT_ID' => $chat_id,
            'USER_ID' => $assigned_by,
            'LEAVE' => 'N'
        ];

        return $this->call($method, $params);
    }


    public function answerForMissedCall($chat_id, $message)
    {
        $method = 'imopenlines.bot.session.message.send';
        $params = [
            'CHAT_ID' => $chat_id,
            'MESSAGE' => $message
        ];

        return $this->call($method, $params);
    }


    public function closeDialog($chat_id)
    {
        $method = 'imopenlines.bot.session.finish';
        $params = [
            'CHAT_ID' => $chat_id,
        ];

        return $this->call($method, $params);
    }


}

