<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;

use application\models\DbBackups;

class DbBackupsController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'delete', 'create'],
                'roles' => ['@'],
            ],
        ];
    }

	public function indexAction() {
        $searchModel = new DbBackups();

        $paths = $searchModel->getFiles();
        $totalRow = count($paths);
        $pagination = new Pagination($totalRow, $totalRow, $this->route);

        if (!empty($searchModel->message)) {
            $paths = $searchModel->message;
        }

        rsort($paths);

		$vars = [
            'paths' => $paths,
            'paginationHeader' => $pagination->getHeader(),
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/ladda/ladda-themeless.min.css'
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/ladda/spin.min.js',
                '/js/plugins/ladda/ladda.min.js',
                '/js/plugins/pjax/pjax.min.js',
                '/js/db-backups.js',
            ]
		];
		$this->view->render($vars);
	}

    public function deleteAction() {
        try {
            if (empty($_POST['path'])) {
                throw new \Exception("Ошибка запроса");
            };
            $model = new DbBackups();
            $model->path = $path;
            $model->delete();
            $this->view->echoJson('success', "Дамп БД удален");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function createAction() {
        try {
            $model = new DbBackups();
            $model->create();
            $this->view->echoJson('success', "Добавлено");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }
}