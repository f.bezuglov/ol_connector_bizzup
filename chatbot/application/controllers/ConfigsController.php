<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Bitrix24;
use application\models\Authentications;
use application\models\Configs;
use application\models\joins\ConfigsJoins;
use Matrix\Exception;

class ConfigsController extends Controller
{

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['create', 'update', 'delete', 'addTemplates'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'actions' => ['delete'],
                'roles' => ['admin'],
            ],
        ];
    }

    public function createAction()
    {
        if (!empty($_POST) && static::TOKENS("get")) {
            try {
                if (!isset($_POST['member_id']) || !is_string($_POST['member_id'])) {
                    throw new \Exception('Неверный запрос');
                }

                $model = new Configs();
                $model->loadFromPost($_POST);

                // validate all models
                if (!$model->validate()) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->insertRow();
                $this->view->echoJson('success', 'Изменения успешно сохранены', ['id' => $model->id]);

            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }
        try {
            if (empty($_GET['member_id']) || !is_string($_GET['member_id'])) {
                throw new \Exception('Неверный запрос');
            }
            $member_id = $_GET['member_id'];
            Authentications::loadAuthToSessions($member_id);

            $authSearchModel = new Authentications();
            $authSearchModel->member_id = $member_id;
            $authModel = $authSearchModel->selectRow();

            $searchModel = new ConfigsJoins();
            $searchModel->member_id = $member_id;
            $model = $searchModel->selectRow();
            if (empty($model->id)) {
                $model->member_id = $member_id;
                $model->setDefaultValues();
            }

            $model->member_id = $member_id;
            $model->member_name = $authModel->member_name;

            $bitrix = new Bitrix24($member_id);
            $templates = $bitrix->getTemplates();

            $lead_templates = $templates[0];
            $contact_templates = $templates[1];

        } catch (\Exception $ex) {
            $_SESSION['saveMessage'][0]['type'] = 'warning';
            $_SESSION['saveMessage'][0]['sqlMessage'] = $ex->getMessage();
        }

        $vars = [
            'model' => $model ?? new ConfigsJoins(),
            'lead_templates' => $lead_templates ?? [],
            'contact_templates' => $contact_templates ?? [],
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/switchery/switchery.min.css',
                '/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
                '/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css',
                '/css/plugins/ladda/ladda-themeless.min.css'
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/switchery/switchery.min.js',
                '/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
                '/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
                '/js/plugins/ladda/spin.min.js',
                '/js/plugins/ladda/ladda.min.js',
                '/js/configs.js',
            ]
        ];
        $this->view->render($vars);
    }

    public function updateAction()
    {
        if (!empty($_POST) && static::TOKENS("get")) {
            try {
                if (empty($_POST['member_id']) || !is_string($_POST['member_id'])) {
                    throw new \Exception('Неверный запрос');
                }
                $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
                if (empty($id)) {
                    throw new \Exception('Неверный запрос');
                }
                $model = new Configs();
                $model->loadFromPost($_POST);

                // validate all models
                if (!($valid = $model->validate())) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->updateRow();
                $this->view->echoJson('success', 'Изменения успешно сохранены');

            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }
        try {
            if (empty($_GET['member_id']) || !is_string($_GET['member_id'])) {
                throw new \Exception('Неверный запрос');
            }
            $member_id = $_GET['member_id'];
            Authentications::loadAuthToSessions($member_id);

            $searchModel = new ConfigsJoins();
            $searchModel->member_id = $member_id;
            $model = $searchModel->selectRow();
            if (empty($model->id)) {
                $this->view->redirect('/configs/create?member_id=' . $member_id);
            }

            $bitrix = new Bitrix24($member_id);
            $templates = $bitrix->getTemplates();

            $lead_templates = $templates[0];
            $contact_templates = $templates[1];

        } catch (\Exception $ex) {
            $_SESSION['saveMessage'][0]['type'] = 'warning';
            $_SESSION['saveMessage'][0]['sqlMessage'] = $ex->getMessage();
        }

        $vars = [
            'model' => $model,
            'lead_templates' => $lead_templates,
            'contact_templates' => $contact_templates,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/switchery/switchery.min.css',
                '/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
                '/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css',
                '/css/plugins/ladda/ladda-themeless.min.css'
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/switchery/switchery.min.js',
                '/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
                '/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
                '/js/plugins/ladda/spin.min.js',
                '/js/plugins/ladda/ladda.min.js',
                '/js/configs.js',
            ]
        ];
        $this->view->render($vars);

    }

    public function addTemplatesAction()
    {
        try {
            if (empty($_POST['member_id'])
                || !is_string($_POST['member_id'])
                || empty($_POST['action'])
                || $_POST['action'] !== 'add_templates') {
                throw new Exception("Ошибка запроса");
            }

            $member_id = $_POST['member_id'];

            $bitrix = new Bitrix24($member_id);
            $templates = $bitrix->installTemplates();

            $this->view->echoJson('success', 'Шаблоны добавлены', $templates);

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function deleteAction()
    {
        try {
            if (!isset($_POST['member_id']) || !is_string($_POST['member_id'])) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new Configs();
            $model->deleteRowByFilter(['member_id' => $_POST['member_id']]);
            $this->view->echoJson('success', "Настройки сброшены");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }
}