<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Export2Xls;
use application\lib\Pagination;

use application\models\Authentications;
use application\models\Configs;
use application\models\joins\AuthenticationsJoins;
use application\models\Logs;
use application\models\joins\LogsJoins;
use application\models\Users;

class ClientsController extends Controller
{

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'create', 'update', 'delete'],
                'roles' => ['@'],
            ],
            /*[
                'allow' => true,
                'actions' => ['delete'],
                'roles' => ['admin'],
            ],*/
        ];
    }

    public function indexAction()
    {
        try {
            $searchModel = new AuthenticationsJoins();
            $searchModel->setSqlSortDirectStr(['member_name' => 'asc']);
            $models = $searchModel->selectRows();
        } catch (\Exception $ex) {
            $_SESSION['saveMessage'][0]['type'] = 'warning';
            $_SESSION['saveMessage'][0]['sqlMessage'] = $ex->getMessage();
        }

        $vars = [
            'models' => $models,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css'
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/pjax/pjax.min.js',
                '/js/clients.js',
            ]
        ];
        $this->view->render($vars);
    }

    public function viewAction()
    {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $searchModel = new Logs();
            $searchModel->id = $id;
            $viewModel = $searchModel->selectRow();
            $vars = [
                'model' => $viewModel,
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoHtml('error', $ex->getMessage());
        }
    }

    public function createAction()
    {
        $model = new Authentications();

        if (!empty($_POST)) {
            try {
                $model->loadFromPost($_POST);
                if (empty($model->expires_in)) {
                    $model->expires_in = 0;
                }
                // validate all models
                if (!$model->validate()) {
                    throw new \Exception('Ошибка проверки данных: ' . $model->sqlMessage);
                }
                $model->insertRow();
                $this->view->echoJson('success', 'index');

            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }

        $vars = [
            'model' => $model,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css',
                '/css/plugins/select2/select2.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
                '/js/plugins/bootstrap-datetimepicker/locales/bootstrap-datetimepicker.ru.js',
                '/js/plugins/select2/select2.min.js',
                '/js/plugins/inputmask/jquery.inputmask.bundle.js',
                '/js/clients-form.js',
            ]
        ];

        $this->view->render($vars);
    }

    public function updateAction()
    {
        $id = !empty($_GET['id']) ? (int)$_GET['id'] : '';
        if (empty($id)) {
            $this->view->echoJson('error', "Ошибка запроса");
        }
        $model = new Authentications();

        if (!empty($_POST)) {
            try {
                $model->loadFromPost($_POST);
                // validate all models
                if (!$model->validate()) {
                    throw new \Exception('Ошибка проверки данных: ' . $model->sqlMessage);
                }
                $model->updateRow();
                $this->view->echoJson('success', "Изменения успешно сохранены");
            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }
        $model->id = $id;
        $model = $model->selectRow();

        $vars = [
            'model' => $model,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css',
                '/css/plugins/select2/select2.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
                '/js/plugins/bootstrap-datetimepicker/locales/bootstrap-datetimepicker.ru.js',
                '/js/plugins/select2/select2.min.js',
                '/js/plugins/inputmask/jquery.inputmask.bundle.js',
                '/js/clients-form.js',
            ]
        ];

        $this->view->render($vars);
    }


    public function deleteAction()
    {
        try {
            if (empty($_POST['id']) || !is_numeric($_POST['id'])) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new Authentications();
            $model->id = (int)$_POST['id'];
            $model->deleteRow();
            $this->view->echoJson('success', "Клиент удален");
        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }
}