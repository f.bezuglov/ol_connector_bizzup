<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Export2Xls;
use application\lib\Pagination;

use application\models\Authentications;
use application\models\Chats;
use application\models\Logs;
use application\models\joins\LogsJoins;
use application\models\Users;
use Matrix\Exception;
use Mpdf\Tag\P;

class ChatsController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'export', 'view', 'delete', 'deleteAll'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'actions' => [],
                'roles' => ['admin'],
            ],
            [
                'allow' => true,
                'actions' => ['addChat'],
                'roles' => ['?'],
            ],
        ];
    }

	public function indexAction() {
        try {
            if (empty($_GET['member_id']) || !is_string($_GET['member_id'])) {
                throw new \Exception('Отсутствует параметр member_id');
            }
            $searchModel = new Chats();
            $searchModel->loadFromGet();
            $totalRow = $searchModel->selectRowsCount();

            if (!empty($_GET['togdata']) && $_GET['togdata'] == 'all') {
                $pagination = new Pagination((int)$totalRow, (int)$totalRow, $this->route);
            } else {
                $pagination = new Pagination((int)$totalRow, __LIMIT_ROW__, $this->route);
            }

            if (!empty($_GET['sort']['name']) && !empty($_GET['sort']['direct'])) {
                $sortName = $_GET['sort']['name'];
                $sortDirect = $_GET['sort']['direct'];
                $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
            } else {
                $sortName = 'id';
                $sortDirect = 'DESC';
                $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
            }

            $searchModel->setSqlLimitStr($pagination->getStartRow(), $pagination->getLimitRow());

            $models = $searchModel->selectRows();

            $member_id = $_GET['member_id'];
            $authSearchModel = new Authentications();
            $authSearchModel->member_id = $member_id;
            $authModel = $authSearchModel->selectRow();
            if (empty($authModel->id)) {
                throw new \Exception('Не найден клиент');
            }
            Authentications::loadAuthToSessions($member_id);

            $params = $_GET;
            $params['sort']['name'] = $sortName;
            $params['sort']['direct'] = $sortDirect;

        } catch (\Exception $ex) {
            $_SESSION['saveMessage'][0]['type'] = 'warning';
            $_SESSION['saveMessage'][0]['sqlMessage'] = $ex->getMessage();
        }

		$vars = [
            'pagination' => $pagination ?? new Pagination(0, 0, $this->route),
            'models' => $models ?? [new Chats()],
            'authModel' => $authModel ?? new Authentications(),
            'params' => $params ?? [],
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
                '/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
                '/js/table.js',
                '/js/chats.js',
            ]
		];
		$this->view->render($vars);
	}

    public function viewAction() {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
               throw new \Exception("Ошибка запроса");
            }
            $searchModel = new Chats();
            $searchModel->id = $id;
            $viewModel = $searchModel->selectRow();
            $vars = [
                'model' => $viewModel,
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoContent($ex->getMessage());
        }
    }

    public function deleteAction() {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id) || !static::TOKENS("get")) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new Chats();
            $model->id = $id;
            $model->deleteRow();
            $this->view->echoJson('success', "Чат №" . $model->id . " удален");
        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function deleteAllAction() {
        try {
            if (empty($_POST['member_id']) || !is_string($_POST['member_id']) || !static::TOKENS("get")) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new Chats();
            $model->deleteRowByFilter(['member_id' => $_POST['member_id']]);
            $this->view->echoJson('success', "Все записи удалены.");
        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function addChatAction($isTest = false) {
        try {
            $memberId = $_POST['member_id'];
            Authentications::loadAuthToSessions($memberId);

            if ($isTest) {
                if (!file_exists(__TESTS__ . '/add-chat.php')) {
                    $this->view->echoJson('error', "Отсутствует файл " . __TESTS__ . "/add-chat.json с массивом ONIMBOTJOINCHAT");
                }
                $post = require_once __TESTS__ . '/add-chat.php';
            } else {
                $post = $_POST;
                Logs::insertRowLogs('success', get_class($this), 'Добавление чата: ' . $post['data']['PARAMS']['BOT_ID'] ?? '');
            }
            $model = new Chats();
            $model->member_id = $memberId;
            $model->loadFromPost($post);

            $model->is_workflow_start = Chats::IS_WORKFLOW_START_FALSE;
            $model->is_transfer_session = Chats::IS_TRANSFER_SESSION_FALSE;
            $model->is_operator_answer = Chats::IS_OPERATOR_ANSWER_FALSE;
            $model->is_close = Chats::IS_CLOSE_FALSE;
            $model->replaceRow();

            if ($isTest) {
                $this->view->echoJson('success', "Успешно выполнено");
            }
        } catch (\Exception $ex) {
            if ($isTest) {
                $this->view->echoJson('error', $ex->getMessage());
            } else {
                Logs::insertRowLogs('error', get_class($this), $ex->getMessage());
            }
        }
    }
}