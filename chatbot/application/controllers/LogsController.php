<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Export2Xls;
use application\lib\Pagination;

use application\models\Authentications;
use application\models\Logs;
use application\models\joins\LogsJoins;
use application\models\Users;

class LogsController extends Controller
{

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'export', 'view', 'delete', 'deleteAll'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'actions' => [],
                'roles' => ['admin'],
            ],
        ];
    }

    public function indexAction()
    {
        try {
            $searchModel = new LogsJoins();
            $searchModel->loadFromGet();
            $totalRow = $searchModel->selectRowsCount();

            if (!empty($_GET['togdata']) && $_GET['togdata'] == 'all') {
                $pagination = new Pagination((int)$totalRow, (int)$totalRow, $this->route);
            } else {
                $pagination = new Pagination((int)$totalRow, __LIMIT_ROW__, $this->route);
            }

            if (!empty($_GET['sort']['name']) && !empty($_GET['sort']['direct'])) {
                $sortName = $_GET['sort']['name'];
                $sortDirect = $_GET['sort']['direct'];
                $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
            } else {
                $sortName = 'id';
                $sortDirect = 'DESC';
                $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
            }

            $searchModel->setSqlLimitStr($pagination->getStartRow(), $pagination->getLimitRow());

            $models = $searchModel->selectRows();

            $searchModelAuth = new Authentications();
            $searchModelAuth->setSqlSortDirectStr(['member_name' => 'ASC']);
            $selectsValues['member_name'] = $searchModelAuth->selectRows();

            $params = $_GET;
            $params['sort']['name'] = $sortName;
            $params['sort']['direct'] = $sortDirect;

        } catch (\Exception $ex) {
            $_SESSION['saveMessage'][0]['type'] = 'warning';
            $_SESSION['saveMessage'][0]['sqlMessage'] = $ex->getMessage();
        }

        $vars = [
            'pagination' => $pagination ?? new Pagination(0, 0, $this->route),
            'models' => $models ?? [new LogsJoins()],
            'params' => $params ?? [],
            'selectValues' => $selectsValues ?? [],
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
                '/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
                '/js/table.js',
            ]
        ];
        $this->view->render($vars);
    }

    public function viewAction()
    {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $searchModel = new LogsJoins();
            $searchModel->id = $id;

            $viewModel = $searchModel->selectRow();
            $vars = [
                'model' => $viewModel,
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoContent($ex->getMessage());
        }
    }

    public function deleteAction()
    {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new Logs();
            $model->id = $id;
            $model->deleteRow();
            $this->view->echoJson('success', "Лог №" . $model->id . " удален");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function deleteAllAction()
    {
        try {
            $model = new Logs();
            $model->deleteRows();
            $model->sequenceRestartId(1);
            $this->view->echoJson('success', "Все записи удалены.<br/>Счетчик id сброшен");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function exportAction()
    {
        $searchModel = new LogsJoins();
        $searchModel->loadFromGet();
        $searchModel->setSqlSortDirectStr(['created_at' => 'DESC']);
        $models = $searchModel->selectRows();
        $export = new Export2Xls($models, 'Логи');
        $export->run();
    }


}