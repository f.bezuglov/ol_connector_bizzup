<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Users;
use application\models\joins\UsersJoins;
use application\lib\UploadPhoto;

class UsersController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['login', 'requestPasswordReset', 'passwordReset'],
                'roles' => ['?'],
            ],
            [
                'allow' => true,
                'actions' => ['logout', 'profile', 'confirmEmail', 'uploadPhoto'],
                'roles' => ['@'],
            ],
        ];
    }

	public function loginAction() {

		if (isset($_SESSION['curUser'])) {
			$this->view->redirect('index');
		}
		if (!empty($_POST)) {
		    $model = new Users();
            $model->name = !empty($_POST['login']) ? filter_var($_POST['login'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_EMPTY_STRING_NULL) : '';
            $password = !empty($_POST['password']) ? filter_var($_POST['password'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_EMPTY_STRING_NULL) : '';

			if (!$model->validateLoginInDb($password)) {
				$this->view->echoJson('error', 'Неверный логин или пароль');
			}
            $this->view->echoJson('success', '');
		}
        $this->view->layout = 'login';
		$this->view->render();
	}

	public function logoutAction() {
		unset($_SESSION['curUser']);
		$this->view->redirectToLogin();
	}

    public function profileAction() {
        if (!empty($_POST) && static::TOKENS("get")) {
            $formName = $_POST['form_name'] ?? '';
            switch ($formName) {
                case 'profile':
                    $model = new Users();
                    $model->name = !empty($_POST['name']) ? filter_var($_POST['name'], FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL) : '';
                    if (empty($model->name)) {
                        $this->view->echoJson('error', "Ошибка запроса");
                    }
                    if ($model->name !== $_SESSION['curUser']['name'] && !$model->isUniqueName()) {
                        $this->view->echoJson('error', "Имя пользователя уже занято");
                    }
                    $model->id = $_SESSION['curUser']['id'];
                    $model->sex = !empty($_POST['sex']) && key_exists($_POST['sex'], $model->getSexArray()) ? $_POST['sex'] : null;
                    $model->photo = $_POST['photo'] ?? null;

                    if (!$model->updateRow()) {
                        $this->view->echoJson('error', $model->sqlMessage);
                    } else {
                        Users::updateSession($model);
                        $this->view->echoJson('success', "Сохранено");
                    }
                    break;
                case 'password':
                    $model = new Users();
                    $model->id = $_SESSION['curUser']['id'];
                    $oldPassword= $_POST['old_password'] ?? '';
                    if (empty($oldPassword)
                        //|| filter_var($oldPassword,FILTER_VALIDATE_REGEXP,  array( "options"=> array( "regexp" => "/.{6,25}/")))
                        || !$model->validatePasswordInDb($oldPassword)) {
                        $this->view->echoJson('error', "Старый пароль введён не верно");
                    }
                    $newPassword= $_POST['new_password'] ?? '';
                    $newPasswordRepeat= $_POST['new_password_repeat'] ?? '';
                    if (empty($newPassword)
                        //|| filter_var($newPassword,FILTER_VALIDATE_REGEXP,  array( "options"=> array( "regexp" => "/.{6,25}/")))
                        || $newPassword !== $newPasswordRepeat) {
                        $this->view->echoJson('error', "Ошибка ввода нового пароля");
                    }
                    if (!$model->updatePasswordHash($newPassword)) {
                        $this->view->echoJson('error', $model->sqlMessage);
                    } else {
                        $this->view->echoJson('success', "Новый пароль успешно сохранён");
                    }
                    break;
                case 'email':
                    $model = new Users();
                    $model->id = $_SESSION['curUser']['id'];
                    $model->email = $_POST['email'] ?? '';
                    if (empty($model->email) || !filter_var($model->email, FILTER_VALIDATE_EMAIL)) {
                        $this->view->echoJson('error', "Неверный формат электронной почты");
                    }
                    if (!$model->updateEmail()) {
                        $this->view->echoJson('error', $model->sqlMessage);
                    } else {
                        $this->view->echoJson('success', "Новая электронная почта успешно сохранена");
                    }
                    break;
                default:
                    $this->view->echoJson('error', "Ошибка запроса");
            }
        }
        $model = new Users();
        $model->id = $_SESSION['curUser']['id'];
        $result = $model->selectRow();
        if (!empty($result)) {
            $model = $result;
        }
        $selectsValues['sexes'] = $model::getSexArray();
        $vars = [
            'curLabels' => UsersJoins::attributeLabels(),
            'model' => $model,
            'selectsValues' => $selectsValues,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/jcrop/jquery.Jcrop.min.css',
                '/css/plugins/jcrop/cropper.css',
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/jcrop/jquery.Jcrop.min.js',
                '/js/plugins/simple-ajax-uploader/SimpleAjaxUploader.min.js',
                '/js/plugins/jcrop/cropper.js',
                '/js/users-profile.js',
            ]
        ];
        $this->view->render($vars);
    }

    public function confirmEmailAction()
    {
        $token = $_GET['token'] ?? '';
        if (!empty($token)) {
            $modelToken = new UsersEmailConfirmToken();
            $modelToken->new_email_token = $token;
            if (!empty($modelToken->selectRow())) {
                $modelToken->confirm($token);
                $_SESSION['saveMessage'] = ['success', 'Электронная почта успешно подтверждена и изменена'];
            } else {
                $_SESSION['saveMessage'] = ['error', 'Неверная ссылка подтверждения электронной почты'];
            }

        }
        $this->view->redirect('/');
    }

    public function requestPasswordResetAction()
    {
        $model = new RequestPasswordResetForm();
        if (!empty($_POST['email']) && static::TOKENS("get")) {
            $model->email = !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL) : '';
            if ($model->sendEmailRequestPassword()) {
                $_SESSION['saveMessage'] = ['success', 'Проверьте свой e-mail для получения дальнейших инструкций'];
                $this->view->echoJson('success', '/login');
            } else {
                $_SESSION['saveMessage'] = ['error', 'Не удалось отправить сообщение для подтверждения адреса электронной почты'];
                $this->view->echoJson('error', '/login');
            }
        }
        $vars = [
            'model' => $model,
        ];
        $this->view->layout = 'login';
        $this->view->render($vars);
    }

    public function passwordResetAction($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('users', 'NEW_PASSWORD_WAS_SAVED'));

            return $this->redirect(Url::toRoute('/user/login'));
        }

        $this->layout = '//main-login';
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function uploadPhotoAction() {
        if (!empty($_POST) && static::TOKENS("get")) {
            $modelPhoto = new UploadPhoto();
            if (!$modelPhoto->loadFromPost()) {
                $this->view->echoJson('error', $modelPhoto->sqlMessage);
            }
            if (!$modelPhoto->validate()) {
                $this->view->echoJson('error', $modelPhoto->sqlMessage);
            }
            if (empty($link = $modelPhoto->run())) {
                $this->view->echoJson('error', $modelPhoto->sqlMessage);
            } else {
                $this->view->echoJson('success', $link);
            }
        }
    }
}