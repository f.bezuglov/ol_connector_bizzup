<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Export2Xls;
use application\lib\Pagination;

use application\models\Authentications;
use application\models\BotActions;
use application\models\Chats;
use application\models\ChatsMessages;
use application\models\joins\ConfigsJoins;
use application\models\Logs;

class ChatsMessagesController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['view', 'delete', 'addChatMessage', 'expandRow'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'actions' => ['addChatMessage'],
                'roles' => ['?'],
            ],
        ];
    }

    public function viewAction() {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $searchModel = new ChatsMessages();
            $searchModel->id = $id;

            $viewModel = $searchModel->selectRow();
            $vars = [
                'model' => $viewModel,
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoContent($ex->getMessage());
        }
    }

    public function deleteAction() {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (!static::TOKENS("get") || empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $model = new ChatsMessages();
            $model->id = $id;
            $model->deleteRow();
            $this->view->echoJson('success', "Лог №" . $model->id . " удален");
        } catch (\Exception $ex) {
            $this->view->echoJson('error', $model->sqlMessage);
        }
    }

    public function expandRowAction() {
        try {
            $memberId = !empty($_POST['member_id']) ? (string)$_POST['member_id'] : 0;
            $botId = !empty($_POST['bot_id']) ? (int)$_POST['bot_id'] : 0;
            $chatId = !empty($_POST['chat_id']) ? (int)$_POST['chat_id'] : 0;
            if (empty($memberId) || empty($botId) || empty($chatId)) {
                throw new \Exception("Ошибка запроса");
            }
            $searchModel = new ChatsMessages();
            $searchModel->member_id = $memberId;
            $searchModel->bot_id = $botId;
            $searchModel->chat_id = $chatId;
            $searchModel->setSqlSortDirectStr(['id' => 'DESC']);
            $models = $searchModel->selectRows();

            $botActionsModels = [];
            foreach ($models as $key => $model) {
                $botActionsSearchModel = new BotActions();
                $botActionsSearchModel->chat_messages_id = $model->id;
                $botActionsSearchModel->setSqlSortDirectStr(['id' => 'ASC']);
                $botActionsModels[$key] = $botActionsSearchModel->selectRows();
            }
            $vars = [
                'models' => $models,
                'botActionsModels' => $botActionsModels
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoContent($ex->getMessage());
        }
    }

    public function addChatMessageAction($isTest = false)
    {
        try {
            $memberId = $_POST['member_id'];
            Authentications::loadAuthToSessions($memberId);

            if ($isTest) {
                $is_bot       = !empty($_POST['is_bot']);
                $testFileName = __TESTS__ . '/add-chat-message-' . ($is_bot ? 'bot' : 'man') . '.php';
                if (!file_exists($testFileName)) {
                    throw new \Exception("Отсутствует файл {$testFileName} с массивом ONIMBOTJOINCHAT");
                }
                $post = require_once $testFileName;
            } else {
                $post = $_POST;
                //Logs::insertRowLogs('success', get_class($this), 'Добавление сообщения: ' . $post['data']['PARAMS']['MESSAGE'] ?? '');
            }

            if (empty($post['data']['BOT']) || !is_array($post['data']['BOT']) || empty($post['data']['PARAMS']['CHAT_ID'])) {
                throw new \Exception('Неверный запрос');
            }

            $botId = array_key_first($post['data']['BOT']);
            $chatId = (int)$post['data']['PARAMS']['CHAT_ID'];

            // найдем конфигурацию
            $configsSearchModel = new ConfigsJoins();
            $configsSearchModel->member_id = $memberId;
            $configsModel = $configsSearchModel->selectRow();
            if (empty($configsModel->id)) {
                throw new \Exception('Не найдена конфигурация');
            }

            // найдем чат
            // пауза, так как событие на создание чата приходит одновременно с сообщением
            sleep(1);
            $chatsSearchModel = new Chats();
            $chatsSearchModel->member_id = $memberId;
            $chatsSearchModel->bot_id = $botId;
            $chatsSearchModel->chat_id = $chatId;
            $chatsModel = $chatsSearchModel->selectRow();
            if ($chatsSearchModel->isError) {
                throw new \Exception($chatsSearchModel->sqlMessage);
            }
            // если не найден чат, то добавим чат
            if (empty($chatsModel->id)) {
                $chatsSearchModel->insertRow();
                $chatsModel->id = $chatsSearchModel->dbLastInsertId();
                $chatsModel->setDefaultValues();
            }

            // добавим сообщение в БД
            $chatsMessageModel = new ChatsMessages();
            $chatsMessageModel->member_id = $memberId;
            $chatsMessageModel->configsModel = $configsModel;
            $chatsMessageModel->loadFromPost($post);
            $chatsMessageModel->insertRow();

            // выполним все нужные операции
            $botActionsModel                   = new BotActions();
            $botActionsModel->member_id        = $memberId;
            $botActionsModel->chat_messages_id = $chatsMessageModel->id;
            $botActionsModel->configsModel = $configsModel;
            $botActionsModel->chatsModel = $chatsModel;
            $botActionsModel->chatsMessagesModel = $chatsMessageModel;

            $botActionsModel->executeActions();

            if ($isTest) { $this->view->echoJson('success', "Успешно выполнено"); }

        } catch (\Exception $ex) {
            if ($isTest) {
                $this->view->echoJson('error', $ex->getMessage());
            } else {
                Logs::insertRowLogs('error', get_class($this), $ex->getMessage(), http_build_query($ex));
            }
        }
    }

    /*public function addChatMessageAction($isTest = false)
    {
        $memberId = $_POST['member_id'];
        if (!Authentications::loadAuthToSessions($memberId)) {
            $message = 'Ошибка авторизации';
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($this), $message, http_build_query($_POST));
                exit;
            }
        };

        if ($isTest) {
            $is_bot       = !empty($_POST['is_bot']);
            $testFileName = __TESTS__ . '/add-chat-message-' . ($is_bot ? 'bot' : 'man') . '.php';
            if (!file_exists($testFileName)) {
                $message = "Отсутствует файл {$testFileName} с массивом ONIMBOTJOINCHAT";
                $this->view->echoJson('error', $message);
            }
            $post = require_once $testFileName;
        } else {
            $post = $_POST;
            Logs::insertRowLogs('success', get_class($this), 'Добавление сообщения: ' . $post['data']['PARAMS']['MESSAGE'] ?? '');
        }

        if (empty($post['data']['BOT']) || !is_array($post['data']['BOT']) || empty($post['data']['PARAMS']['CHAT_ID'])) {
            $message = 'Неверный запрос';
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($this), $message, http_build_query($_POST));
                exit;
            }
        }

        $botId = array_key_first($post['data']['BOT']);
        $chatId = (int)$post['data']['PARAMS']['CHAT_ID'];

        // найдем чат
        $chatSearchModel = new Chats();
        $chatSearchModel->member_id = $memberId;
        $chatSearchModel->bot_id = $botId;
        $chatSearchModel->chat_id = $chatId;
        $chatModel = $chatSearchModel->selectRow();
        if ($chatSearchModel->isError) {
            $message = $chatSearchModel->sqlMessage;
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($chatSearchModel), $message);
                exit;
            }
        }
        if (empty($chatModel->id)) {
            $message = 'Не найден чат';
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($chatSearchModel), $message);
                exit;
            }
        }

        // добавим сообщение
        $chatMessageModel = new ChatsMessages();
        $chatMessageModel->member_id = $memberId;
        $chatMessageModel->loadFromPost($post);
        if (!$chatMessageModel->insertRow()) {
            $message = $chatMessageModel->sqlMessage;
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($chatMessageModel), $message);
                exit;
            }
        }






        $botActionsModel                   = new BotActions();
        $botActionsModel->member_id        = $memberId;
        $botActionsModel->chat_messages_id = $chatMessageModel->id;
        $botActionsModel->chatModel = $chatModel;
        $botActionsModel->chatMessagesModel = $chatMessageModel;

        if (!$botActionsModel->executeActions()) {
            $message = $botActionsModel->sqlMessage;
            if ($isTest) {
                $this->view->echoJson('error', $message);
            } else {
                Logs::insertRowLogs('error', get_class($botActionsModel), $message);
                exit;
            }
        };
        if ($isTest) { $this->view->echoJson('success', "Успешно выполнено"); }
    }*/
}