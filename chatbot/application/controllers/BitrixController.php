<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Bitrix24;
use application\models\Authentications;
use application\models\Configs;
use application\models\joins\ConfigsJoins;

class BitrixController extends Controller
{

    public function __construct($route)
    {
        parent::__construct($route);

        $this->view->layout = 'bitrix';
        $this->view->path = 'bitrix/index';
    }

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'createConfigs', 'updateConfigs', 'addTemplates', 'uninstall', 'deleteConfigs'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'actions' => ['install'],
                'roles' => ['?'],
            ],
        ];
    }

    public function installAction()
    {
        try {
            if (empty($_POST['member_id']) || empty($_POST['AUTH_ID']) || empty($_POST['AUTH_EXPIRES'])
                || empty($_GET['APP_SID']) || empty($_POST['REFRESH_ID']) || empty($_GET['DOMAIN'])) {
                throw new \Exception('Отсутствуют необходимые параметры');
            }
            $memberId = $_POST['member_id'];
            $isExistModel = new Authentications();

            $isExistModel->member_id = $memberId;
            $resultIsExistModel = $isExistModel->selectRow();

            // Сохраняем аутентификацию
            $model = new Authentications();

            $model->member_id = $memberId;
            $model->access_token = htmlspecialchars($_POST['AUTH_ID']);
            $model->expires_in = htmlspecialchars($_POST['AUTH_EXPIRES']);
            $model->application_token = htmlspecialchars($_GET['APP_SID']);
            $model->refresh_token = htmlspecialchars($_POST['REFRESH_ID']);
            $model->domain = htmlspecialchars($_GET['DOMAIN']);
            $model->lang = htmlspecialchars($_GET['LANG'] ?? '');
            $model->member_name = $model->domain;

            $_SESSION['auth'][$memberId] = $model->modelToArray();
            $_SESSION['auth']['member_id'] = $memberId;

            $bitrix = new Bitrix24($memberId);
            $model->bot_id = $bitrix->install();
            if ($bitrix->isError) {
                throw new \Exception($bitrix->sqlMessage);
            }

            if (empty($resultIsExistModel->id)) {
                if (!$model->validate()) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->insertRow();
            } else {
                $model->id = $resultIsExistModel->id;
                if (!$model->validate()) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->updateRow();
            }
            $message = 'Приложение установлено';
            $str = "<head>" .
                "<script src=\"//api.bitrix24.com/api/v1/\"></script>" .
                "<script>
				BX24.init(function(){
					BX24.installFinish();					
				});
			</script>" .
                "</head>" .
                "<body>" . $message . "</body>";
        } catch (\Exception $ex) {
            $model = new Authentications();
            $message = 'Ошибка установки</br>' . $ex->getMessage();
            $str = "<head></head>" .
                "<body>" . $message . "</body>";
        }
        echo $str;
    }

    public function uninstallAction()
    {
        try {
            $memberId = htmlspecialchars($_POST['member_id']);

            $isExistModel = new Authentications();

            $isExistModel->member_id = $memberId;
            $resultIsExistModel = $isExistModel->selectRow();

            $configsSearchModel = new Configs();
            $configsSearchModel->member_id = $memberId;
            $configsModel = $configsSearchModel->selectRow();

            $bitrix = new Bitrix24($memberId);
            $result = $bitrix->uninstall($configsModel);

            $deleteModel = new Authentications();
            $deleteModel->id = $resultIsExistModel->id;
            $deleteModel->deleteRow();

            echo 'Удаление завершено';
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function indexAction()
    {
        try {
            $memberId = htmlspecialchars($_POST['member_id']);
            $configsSearchModel = new Configs();
            $configsSearchModel->member_id = $memberId;
            $configsModel = $configsSearchModel->selectRow();
            if (empty($configsModel->id)) {
                $this->createConfigsAction();
            } else {
                $this->updateConfigsAction();
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function createConfigsAction()
    {

        if (!empty($_POST['action']) && $_POST['action'] == 'create') {
            try {
                if (empty($_POST['member_id']) || !is_string($_POST['member_id'])) {
                    $this->view->echoJson('error', 'Неверный запрос');
                }

                $model = new Configs();
                $model->loadFromPost($_POST);

                // validate all models
                if (!$model->validate()) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->insertRow();
                $this->view->echoJson('success', 'Изменения успешно сохранены', ['id' => $model->dbLastInsertId()]);

            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }

        try {
            if (empty($_POST['member_id']) || !is_string($_POST['member_id'])) {
                throw new \Exception('Неверный запрос');
            }
            $member_id = $_POST['member_id'];
            Authentications::loadAuthToSessions($member_id);

            $searchModel = new ConfigsJoins();
            $searchModel->member_id = $member_id;
            $model = $searchModel->selectRow();
            if (empty($model->id)) {
                $model->member_id = $member_id;
                $model->setDefaultValues();
            }
            $model->member_id = $member_id;

            $bitrix = new Bitrix24($member_id);
            $templates = $bitrix->getTemplates();

            $lead_templates = $templates[0];
            $contact_templates = $templates[1];

        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        $vars = [
            'model' => $model ?? new ConfigsJoins(),
            'lead_templates' => $lead_templates ?? [],
            'contact_templates' => $contact_templates ?? [],
        ];

        $this->view->render($vars);
    }

    public function updateConfigsAction()
    {

        if (!empty($_POST['action']) && $_POST['action'] == 'update') {
            try {
                if (empty($_POST['member_id']) || !is_string($_POST['member_id'])) {
                    throw new \Exception('Ошибка запроса');
                }
                $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
                if (empty($id)) {
                    throw new \Exception("Ошибка запроса");
                }
                $model = new Configs();
                $model->loadFromPost($_POST);

                // validate all models
                if (!$model->validate()) {
                    throw new \Exception($model->sqlMessage);
                }
                $model->updateRow();
                $this->view->echoJson('success', 'Изменения успешно сохранены');

            } catch (\Exception $ex) {
                $this->view->echoJson('error', $ex->getMessage());
            }
        }
        try {
            if (empty($_POST['member_id']) || !is_string($_POST['member_id'])) {
                throw new \Exception('Неверный запрос');
            }
            $member_id = $_POST['member_id'];
            Authentications::loadAuthToSessions($member_id);

            $searchModel = new ConfigsJoins();
            $searchModel->member_id = $member_id;
            $model = $searchModel->selectRow();

            $bitrix = new Bitrix24($member_id);
            $templates = $bitrix->getTemplates();

            $lead_templates = $templates[0];
            $contact_templates = $templates[1];

        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        $vars = [
            'model' => $model ?? new ConfigsJoins(),
            'lead_templates' => $lead_templates ?? [],
            'contact_templates' => $contact_templates ?? [],
        ];

        $this->view->render($vars);

    }


    public function addTemplatesAction()
    {
        try {
            if (empty($_POST['action']) || $_POST['action'] !== 'add_templates'
                || !isset($_POST['member_id']) || !is_string($_POST['member_id'])) {
                throw new \Exception("Ошибка запроса");
            }
            $bitrix = new Bitrix24($_POST['member_id']);
            $templates = $bitrix->installTemplates();
            $this->view->echoJson('success', 'Шаблоны добавлены', $templates);
        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }

    public function deleteConfigsAction()
    {
        try {
            if (!isset($_POST['member_id']) || !is_string($_POST['member_id'])) {
                throw new \Exception('Ошибка запроса');
            }
            $model = new Configs();
            $model->deleteRowByFilter(['member_id' => $_POST['member_id']]);
            $this->view->echoJson('success', "Настройки сброшены");

        } catch (\Exception $ex) {
            $this->view->echoJson('error', $ex->getMessage());
        }
    }
}