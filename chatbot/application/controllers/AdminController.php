<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Users;
use application\lib\Pagination;


class AdminController extends Controller
{

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'create', 'view', 'update', 'delete', 'permissions'],
                'roles' => ['@'],
            ]
        ];
    }

    public function indexAction() {

        $searchModel = new Users();
        $searchModel->loadFromGet();

        $totalRow = $searchModel->selectRowsCount();

        if (!empty($_GET['togdata']) && $_GET['togdata'] == 'all') {
            $pagination = new Pagination((int)$totalRow, (int)$totalRow, $this->route);
        } else {
            $pagination = new Pagination((int)$totalRow, __LIMIT_ROW__, $this->route);
        }

        if (!empty($_GET['sort']['name']) && !empty($_GET['sort']['direct'])) {
            $sortName = $_GET['sort']['name'];
            $sortDirect = $_GET['sort']['direct'];
            $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
        } else {
            $sortName = 'id';
            $sortDirect = 'DESC';
            $searchModel->setSqlSortDirectStr([$sortName => $sortDirect]);
        }

        $searchModel->setSqlLimitStr($pagination->getStartRow(), $pagination->getLimitRow());

        $models = $searchModel->selectRows();
        if (!empty($searchModel->sqlMessage)) {
            $models = $searchModel->sqlMessage;
        }

        $params = $_GET;
        $params['sort']['name'] = $sortName;
        $params['sort']['direct'] = $sortDirect;


        $vars = [
            'pagination' => $pagination,
            'models' => $models,
            'params' => $params,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/sweetalert2/sweetalert2.min.css',
                '/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/plugins/sweetalert2/sweetalert2.min.js',
                '/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
                '/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
                '/js/table.js',
            ]
        ];
        $this->view->render($vars);
    }


    public function viewAction() {
        $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;

        if (empty($id)) {
            $this->view->echoHtml('error', "Ошибка запроса");
        }

        $searchModel = new Users();
        $searchModel->id = $id;

        if(!($viewModel = $searchModel->selectRow())){
            $this->view->echoHtml('error', $searchModel->sqlMessage);
        }
        $vars = [
            'model' => $viewModel,
        ];
        $this->view->echoContent($vars);

    }

    public function createAction()
    {

        if (!empty($_POST) && static::TOKENS("get")) {

            $model = new Users();

            $model->loadFromPost($_POST);

            // validate all models
            if (!$model->validate() || !$model->updateRow()) {
                $this->view->echoJson('error', $model->sqlMessage);
            }
            $this->view->echoJson('success', "Изменения успешно сохранены", ['id' => $model->dbLastInsertId()]);
        }

        $model = new Users();

        $vars = [
            'model' => $model,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/admin.js',
            ]
        ];
        $this->view->render($vars);
    }

    public function updateAction()
    {
        if (!empty($_POST) && static::TOKENS("get")) {

            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                $this->view->echoJson('error', "Ошибка запроса");
            }
            $model = new Users();

            $model->loadFromPost($_POST);

            // validate all models
            if (!$model->validate() || !$model->updateRow()) {
                $this->view->echoJson('error', $model->sqlMessage);
            }
            $this->view->echoJson('success', "Изменения успешно сохранены");
        }

        if (empty($_GET['id']) || !is_numeric($_GET['id'])) {
            exit('Неверный запрос');
        }
        $id = (int)$_GET['id'];
        $searchModel = new Users();
        $searchModel->id = $id;
        $model = $searchModel->selectRow();
        if ($searchModel->isError) {
            $_SESSION['saveMessage'][0]['type'] = 'danger';
            $_SESSION['saveMessage'][0]['message'] = $searchModel->sqlMessage;
        }
        $model->password_hash = '';

        $vars = [
            'model' => $model,
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/toastr/toastr.min.js',
                '/js/admin.js',
            ]
        ];

        $this->view->render($vars);
    }

    public function deleteAction()
    {
        $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
        if (empty($id) || !static::TOKENS("get")) {
            $this->view->echoJson('error', "Неверный запрос");
        }
        if ($id == 1) {
            $this->view->echoJson('error', "Нельзя удалить пользователя администратора");
        }
        $model = new Users();
        $model->id = $id;
        if(!$model->deleteRow()) {
            $this->view->echoJson('error', $model->sqlMessage);
        }
        $this->view->echoJson('success', "Пользователь удален");
    }

    public function permissionsAction()
    {
        $modelForm = new AssignmentForm;
        $modelForm->model = $this->findModel($id);

        if ($modelForm->load(Yii::$app->request->post())) {
            //$modelForm->status = User::STATUS_ACTIVE;
            if ($modelForm->save()) {
                $this->flushCache();

                Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_UPDATE_PERMISSIONS'));
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('permissions', [
            'modelForm' => $modelForm
        ]);
    }
}
