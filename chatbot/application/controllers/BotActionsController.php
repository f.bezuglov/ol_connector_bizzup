<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Export2Xls;
use application\lib\Pagination;

use application\models\Authentications;
use application\models\BotActions;
use application\models\Chats;
use application\models\ChatsMessages;
use application\models\Logs;
use application\models\joins\LogsJoins;
use application\models\Users;
use Matrix\Exception;

class BotActionsController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['view'],
                'roles' => ['@'],
            ],
        ];
    }

    public function viewAction() {
        try {
            $id = !empty($_POST['id']) ? (int)$_POST['id'] : 0;
            if (empty($id)) {
                throw new \Exception("Ошибка запроса");
            }
            $searchModel = new BotActions();
            $searchModel->id = $id;

            $viewModel = $searchModel->selectRow();

            $vars = [
                'model' => $viewModel,
            ];
            $this->view->echoContent($vars);
        } catch (\Exception $ex) {
            $this->view->echoHtml('error', $ex->getMessage());
        }
    }

}