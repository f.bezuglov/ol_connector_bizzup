<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\models\Users;

class MainController extends Controller {

    public static function acls()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index','sidebarToggle'],
                'roles' => ['@'],
            ],
        ];
    }

    public function indexAction() {
        $vars = [
            'get_needed_css' => [
                '/css/plugins/toastr/toastr.min.css',
                '/css/plugins/jcrop/jquery.Jcrop.min.css',
            ],
            'get_needed_js' => [
                '/js/plugins/pjax/pjax.min.js',
                '/js/plugins/toastr/toastr.min.js',
                '/js/main-index.js',
            ]
        ];
        $this->view->render($vars);
	}

	public function sidebarToggleAction() {
	    if (empty($_SESSION['sidebar-toggle'])) {
            $_SESSION['sidebar-toggle'] = true;
        } else {
            $_SESSION['sidebar-toggle'] = false;
        }

    }

}