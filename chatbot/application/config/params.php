<?php
define('__URL_PREFIX__', '/chatbot_v3/admin');
define('__APP_HANDLER__','https://logs.bizzup.ru/chatbot_v3/app.php');
define('__TMP__', __DIR__ . '/../../tmp');
define('__LOGS__', __DIR__ . '/../../logs');
define('__TESTS__', __DIR__ . '/../../tests');
define('__WORKFLOW_TEMPLATES__', __DIR__ . '/../../workflow-templates');
define('__SYS_LANG_DEFAULT__', 'ru');
define('__PEPPER__', 'c1isvFdxMDdmjOlvxpecFw');
define('__LIMIT_ROW__', 50);
define('__TIMEZONE__', 'Europe/Moscow');
define('__UPLOAD_PHOTO_URL__', __URL_PREFIX__ . '/images/users/photo');
define('__UPLOAD_PHOTO_PATH__', '/admin/images/users/photo');
define('__MAX_LIFETIME_SESSION__', 86400); // 24*3600
