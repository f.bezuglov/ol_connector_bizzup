<?php

return [
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],
    'bitrix/install' => [
        'controller' => 'bitrix',
        'action' => 'install',
    ],
    'bitrix/uninstall' => [
        'controller' => 'bitrix',
        'action' => 'uninstall',
    ],
    'bitrix/index' => [
        'controller' => 'bitrix',
        'action' => 'index',
    ],
    'bitrix/create' => [
        'controller' => 'bitrix',
        'action' => 'createConfigs',
    ],
    'bitrix/create\?{params:\S+}' => [
        'controller' => 'bitrix',
        'action' => 'createConfigs',
    ],
    'bitrix/update' => [
        'controller' => 'bitrix',
        'action' => 'updateConfigs',
    ],
    'bitrix/delete' => [
        'controller' => 'bitrix',
        'action' => 'deleteConfigs',
    ],
    'bitrix/add-templates' => [
        'controller' => 'bitrix',
        'action' => 'addTemplates',
    ],
    'index' => [
        'controller' => 'main',
        'action' => 'index',
    ],
    'main/sidebar-toggle' => [
        'controller' => 'main',
        'action' => 'sidebarToggle',
    ],
    'admin/index' => [
        'controller' => 'admin',
        'action' => 'index',
    ],
    'admin/index\?{params:\S+}' => [
        'controller' => 'admin',
        'action' => 'index',
    ],
    'admin/create' => [
        'controller' => 'admin',
        'action' => 'create',
    ],
    'admin/update' => [
        'controller' => 'admin',
        'action' => 'update',
    ],
    'admin/update\?{params:\S+}' => [
        'controller' => 'admin',
        'action' => 'update',
    ],
    'admin/view' => [
        'controller' => 'admin',
        'action' => 'view',
    ],
    'admin/delete' => [
        'controller' => 'admin',
        'action' => 'delete',
    ],
    'admin/export' => [
        'controller' => 'admin',
        'action' => 'export',
    ],
    'users/upload-photo' => [
        'controller' => 'users',
        'action' => 'uploadPhoto',
    ],
    'login' => [
        'controller' => 'users',
        'action' => 'login',
    ],
    'logout' => [
        'controller' => 'users',
        'action' => 'logout',
    ],
    'profile' => [
        'controller' => 'users',
        'action' => 'profile',
    ],
    'clients/index' => [
        'controller' => 'clients',
        'action' => 'index',
    ],
    'clients/index\?{params:\S+}' => [
        'controller' => 'clients',
        'action' => 'index',
    ],
    'clients/delete' => [
        'controller' => 'clients',
        'action' => 'delete',
    ],
    'clients/create' => [
        'controller' => 'clients',
        'action' => 'create',
    ],
    'clients/update\?{params:\S+}' => [
        'controller' => 'clients',
        'action' => 'update',
    ],
    'configs/create' => [
        'controller' => 'configs',
        'action' => 'create',
    ],
    'configs/update' => [
        'controller' => 'configs',
        'action' => 'update',
    ],
    'configs/create\?{params:\S+}' => [
        'controller' => 'configs',
        'action' => 'create',
    ],
    'configs/update\?{params:\S+}' => [
        'controller' => 'configs',
        'action' => 'update',
    ],
    'configs/delete' => [
        'controller' => 'configs',
        'action' => 'delete',
    ],
    'configs/params\?{params:\S+}' => [
        'controller' => 'configs',
        'action' => 'params',
    ],
    'configs/add-templates' => [
        'controller' => 'configs',
        'action' => 'addTemplates',
    ],
    'db-backups/index' => [
        'controller' => 'dbBackups',
        'action' => 'index',
    ],
    'db-backups/delete' => [
        'controller' => 'dbBackups',
        'action' => 'delete',
    ],
    'db-backups/create' => [
        'controller' => 'dbBackups',
        'action' => 'create',
    ],
    'chats/index\?{params:\S+}' => [
        'controller' => 'chats',
        'action' => 'index',
    ],
    'chats/export\?{params:\S+}' => [
        'controller' => 'chats',
        'action' => 'export',
    ],
    'chats/add-chat' => [
        'controller' => 'chats',
        'action' => 'addChat',
    ],
    'chats/add-chat\?{params:\S+}' => [
        'controller' => 'chats',
        'action' => 'addChat',
    ],
    'chats/delete' => [
        'controller' => 'chats',
        'action' => 'delete',
    ],
    'chats/delete-all' => [
        'controller' => 'chats',
        'action' => 'deleteAll',
    ],
    'chats/view' => [
        'controller' => 'chats',
        'action' => 'view',
    ],
    'chats-messages/add-chat-message' => [
        'controller' => 'chatsMessages',
        'action' => 'addChatMessage',
    ],
    'chats-messages/add-chat-message\?{params:\S+}' => [
        'controller' => 'chatsMessages',
        'action' => 'addChatMessage',
    ],
    'chats-messages/expand-row' => [
        'controller' => 'chatsMessages',
        'action' => 'expandRow',
    ],
    'chats-messages/view' => [
        'controller' => 'chatsMessages',
        'action' => 'view',
    ],
    'bot-actions/view' => [
        'controller' => 'botActions',
        'action' => 'view',
    ],
    'logs/index' => [
        'controller' => 'logs',
        'action' => 'index',
    ],
    'logs/index\?{params:\S+}' => [
        'controller' => 'logs',
        'action' => 'index',
    ],
    'logs/delete' => [
        'controller' => 'logs',
        'action' => 'delete',
    ],
    'logs/delete-all' => [
        'controller' => 'logs',
        'action' => 'deleteAll',
    ],
    'logs/export' => [
        'controller' => 'logs',
        'action' => 'export',
    ],
    'logs/view' => [
        'controller' => 'logs',
        'action' => 'view',
    ],
];