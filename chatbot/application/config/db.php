<?php

return [
	'host' => 'localhost',
	'port' => 'xxx',
	'name' => 'xxx',
	'user' => 'xxx',
	'password' => 'xxx',
];

// createdb -U postgres -E utf8 -T template0 --lc-ctype=ru_RU.UTF-8 --encoding=UTF8 chatbot
// grant all privileges on database chatbot to nevski;
// pg_dump --dbname=postgresql://' . $config['user'] . ':' . $config['password'] . '@' . $config['host'] . ':5432/' . $config['name'] . ' | gzip > ' . $filePath
