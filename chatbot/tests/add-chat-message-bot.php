<?php

return [

    'event' => 'ONIMBOTMESSAGEADD',
    'data'  => [
        'BOT'    => [
            229231 => [
                'domain'            => 'millident.bitrix24.ru',
                'member_id'         => '33eff74e24a54db88b5b632f550f1cdb',
                'application_token' => '1903768cd8a4a856b187c7a3e0d69d14',
                'AUTH'              => [
                    'domain'            => 'millident.bitrix24.ru',
                    'member_id'         => '33eff74e24a54db88b5b632f550f1cdb',
                    'application_token' => '1903768cd8a4a856b187c7a3e0d69d14',
                ],
                'BOT_ID'            => 229231,
                'BOT_CODE'          => 'BOT_CHATFILTER',
            ],
        ],
        'PARAMS' => [
            'FROM_USER_ID'        => 7194,
            'MESSAGE'             => '=== Исходящее сообщение, автор: Битрикс24 ==='
                . 'Роберт, расскажите, понравилось ли Вам в нашей клинике?'
                . 'Нажмите кнопку с подходящим ответом.'
                . 'Миллидент'
                . '[Супер, порекомендую] [Всё нормально] [Не понравилось]',
            'TO_CHAT_ID'          => 56652,
            'MESSAGE_TYPE'        => 'L',
            'SYSTEM'              => 'N',
            'SKIP_COMMAND'        => 'N',
            'SKIP_CONNECTOR'      => 'N',
            'IMPORTANT_CONNECTOR' => 'N',
            'SILENT_CONNECTOR'    => 'N',
            'AUTHOR_ID'           => 7194,
            'CHAT_ID'             => 193401,
            'CHAT_AUTHOR_ID'      => 0,
            'CHAT_ENTITY_TYPE'    => 'LINES',
            'CHAT_ENTITY_ID'      => 'wz_whatsapp_c33eff74e24a54db88b5b632f550f1cdb|4|9e76a674-dc24-4c55-9a84-1f25fcc59927|7194',
            'CHAT_ENTITY_DATA_1'  => 'Y|DEAL|204870|N|N|28164|1660574739|0|0|0',
            'CHAT_ENTITY_DATA_2'  => 'LEAD|0|COMPANY|0|CONTACT|26594|DEAL|204870',
            'CHAT_ENTITY_DATA_3'  => 'N',
            'COMMAND_CONTEXT'     => 'TEXTAREA',
            'MESSAGE_ORIGINAL'    => '=== Исходящее сообщение, автор: Битрикс24 ==='
                . 'Роберт, расскажите, понравилось ли Вам в нашей клинике?'
                . 'Нажмите кнопку с подходящим ответом.'
                . 'Миллидент'
                . '[Супер, порекомендую] [Всё нормально] [Не понравилось]',
            'TO_USER_ID'          => 0,
            'DIALOG_ID'           => 'chat56652',
            'MESSAGE_ID'          => 1198042,
            'CHAT_TYPE'           => 'L',
            'LANGUAGE'            => 'ru',
        ],
        'USER'   => [
            'ID'            => 7194,
            'NAME'          => 'Роберт Егоров',
            'FIRST_NAME'    => 'Роберт',
            'LAST_NAME'     => 'Егоров',
            'WORK_POSITION' => NULL,
            'GENDER'        => 'M',
            'IS_BOT'        => 'N',
            'IS_CONNECTOR'  => 'Y',
            'IS_NETWORK'    => 'N',
            'IS_EXTRANET'   => 'Y',
        ],
    ],
    'ts'    => 1660574740,
    'auth'  => [
        'access_token'      => '246afa62003d731c004c8d620000142c000006a8c40915a06dccb15d260ff261a75fb2',
        'expires'           => 1660578340,
        'expires_in'        => 3600,
        'scope'             => 'crm,imopenlines,bizproc,imbot,user_brief',
        'domain'            => 'millident.bitrix24.ru',
        'server_endpoint'   => 'https://oauth.bitrix.info/rest/',
        'status'            => 'S',
        'client_endpoint'   => 'https://millident.bitrix24.ru/rest/',
        'member_id'         => '33eff74e24a54db88b5b632f550f1cdb',
        'user_id'           => 5164,
        'refresh_token'     => '14e92163003d731c004c8d620000142c000006be960100db4044d4350b9107b98deebf',
        'application_token' => '1903768cd8a4a856b187c7a3e0d69d14',
    ]
];
