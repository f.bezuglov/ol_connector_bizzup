<?php

return [
    'event' => 'ONIMBOTJOINCHAT',
    'data'  => [
        'BOT'    => [
            229231 => [
                'domain'            => 'migcentr.bitrix24.ru',
                'member_id'         => 'xxx',
                'application_token' => 'xxx',
                'AUTH'              => [
                    'domain'            => 'migcentr.bitrix24.ru',
                    'member_id'         => 'xxx',
                    'application_token' => 'xxx'
                ],
                'BOT_ID'            => 229231,
                'BOT_CODE'          => 'BOT_CHATFILTER'
            ]
        ],
        'PARAMS' => [
            'CHAT_TYPE'        => 'L',
            'MESSAGE_TYPE'     => 'L',
            'BOT_ID'           => 229231,
            'USER_ID'          => 0,
            'CHAT_ID'          => 193401,
            'CHAT_AUTHOR_ID'   => 0,
            'CHAT_ENTITY_TYPE' => 'LINES',
            'CHAT_ENTITY_ID'   => 'xxx|7|ac72205f-0c2e-4bf6-a28b-xxx|167551',
            'ACCESS_HISTORY'   => 1,
            'DIALOG_ID'        => 'chat193401',
            'LANGUAGE'         => 'ru'
        ]
    ],
    'ts'    => 1660574385,
    'auth'  => [
        'access_token'      => 'xxx',
        'expires'           => 1660577985,
        'expires_in'        => 3600,
        'scope'             => 'crm,imopenlines,bizproc,imbot,user_brief',
        'domain'            => 'migcentr.bitrix24.ru',
        'server_endpoint'   => 'https://oauth.bitrix.info/rest/',
        'status'            => 'S',
        'client_endpoint'   => 'https://migcentr.bitrix24.ru/rest/',
        'member_id'         => 'xxx',
        'user_id'           => 118293,
        'application_token' => 'xxx'
    ]
];