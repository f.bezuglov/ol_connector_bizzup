<?php

/**
 * Class CRest соединения и обмена с битриксом через REST API
 *
 */
file_put_contents(__DIR__ . '/../logs/url.log', htmlspecialchars($_SERVER['HTTP_REFERER']));
class CRest
{
    private const VERSION = '0.01', TIMEOUT_SOCKET = 15;

    // для авторизации с битрикс
    private $memberId, $domain, $authId, $event_token;

    // параметры настройки
    private       $app_source;
    public static $connector_name;

    public function __construct($memberId)
    {
        $this->memberId = $memberId;

        $iniArray             = parse_ini_file(__DIR__ . "/../configs/config.ini");
        $this->app_source     = $iniArray['app_source'];
        self::$connector_name = $iniArray['connector_name'];
    }

    public function setAuthentication($domain, $authId, $event_token = null)
    {
        $this->domain      = $domain;
        $this->authId      = $authId;
        $this->event_token = $event_token;
    }

    /**
     * Отправка запроса в битрикс
     *
     * @param string $method
     * @param array $arDataRest
     *
     * @throws Exception
     * @return array
     *
     */
    public function call($method, $arDataRest)
    {
        try {

            $clientEndpoint     = 'https://' . $this->domain . '/rest/';
            $url                = $clientEndpoint . $method . '.json';
            $postFields         = $arDataRest;
            $postFields['auth'] = $this->authId;


            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postFields));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTREDIR, 10);
            curl_setopt($curl, CURLOPT_USERAGENT, 'Bizzup termnate bizprocs ' . self::VERSION);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, self::TIMEOUT_SOCKET * 60);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_SOCKET);
            curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);


            $resultRaw = curl_exec($curl);

            if (curl_errno($curl)) {
                throw new Exception('curl curl_error. Ответ с битрикса:  ' . curl_error($curl));
            }

            $result = json_decode($resultRaw ?? '', true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new Exception('curl json_error. Ответ с битрикса: ' . $resultRaw);
            }

            curl_close($curl);

            if (!empty($result['error'])) {
                $messageArr = [];
                if (!empty($result['error'])) {
                    $messageArr[] = $result['error'];
                }
                if (!empty($result['error_description'])) {
                    $messageArr[] = $result['error_description'];
                }
                $message = implode(': ', $messageArr);

                throw new Exception($message);
            }

            return $result;

        } catch (Exception $ex) {
            throw $ex;
        }
    }


    public function install()
    {
        try {
            $this->uninstall();

            $method = 'imconnector.register';
            $params = [
                'ID'                => self::$connector_name,
                'NAME'              => 'fyodor-connector',
                'ICON'              => [
                    'DATA_IMAGE' => 'data:image/svg+xml;charset=US-ASCII,%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20x%3D%220px%22%20y%3D%220px%22%0A%09%20viewBox%3D%220%200%2070%2071%22%20style%3D%22enable-background%3Anew%200%200%2070%2071%3B%22%20xml%3Aspace%3D%22preserve%22%3E%0A%3Cpath%20fill%3D%22%230C99BA%22%20class%3D%22st0%22%20d%3D%22M34.7%2C64c-11.6%2C0-22-7.1-26.3-17.8C4%2C35.4%2C6.4%2C23%2C14.5%2C14.7c8.1-8.2%2C20.4-10.7%2C31-6.2%0A%09c12.5%2C5.4%2C19.6%2C18.8%2C17%2C32.2C60%2C54%2C48.3%2C63.8%2C34.7%2C64L34.7%2C64z%20M27.8%2C29c0.8-0.9%2C0.8-2.3%2C0-3.2l-1-1.2h19.3c1-0.1%2C1.7-0.9%2C1.7-1.8%0A%09v-0.9c0-1-0.7-1.8-1.7-1.8H26.8l1.1-1.2c0.8-0.9%2C0.8-2.3%2C0-3.2c-0.4-0.4-0.9-0.7-1.5-0.7s-1.1%2C0.2-1.5%2C0.7l-4.6%2C5.1%0A%09c-0.8%2C0.9-0.8%2C2.3%2C0%2C3.2l4.6%2C5.1c0.4%2C0.4%2C0.9%2C0.7%2C1.5%2C0.7C26.9%2C29.6%2C27.4%2C29.4%2C27.8%2C29L27.8%2C29z%20M44%2C41c-0.5-0.6-1.3-0.8-2-0.6%0A%09c-0.7%2C0.2-1.3%2C0.9-1.5%2C1.6c-0.2%2C0.8%2C0%2C1.6%2C0.5%2C2.2l1%2C1.2H22.8c-1%2C0.1-1.7%2C0.9-1.7%2C1.8v0.9c0%2C1%2C0.7%2C1.8%2C1.7%2C1.8h19.3l-1%2C1.2%0A%09c-0.5%2C0.6-0.7%2C1.4-0.5%2C2.2c0.2%2C0.8%2C0.7%2C1.4%2C1.5%2C1.6c0.7%2C0.2%2C1.5%2C0%2C2-0.6l4.6-5.1c0.8-0.9%2C0.8-2.3%2C0-3.2L44%2C41z%20M23.5%2C32.8%0A%09c-1%2C0.1-1.7%2C0.9-1.7%2C1.8v0.9c0%2C1%2C0.7%2C1.8%2C1.7%2C1.8h23.4c1-0.1%2C1.7-0.9%2C1.7-1.8v-0.9c0-1-0.7-1.8-1.7-1.9L23.5%2C32.8L23.5%2C32.8z%22/%3E%0A%3C/svg%3E%0A',
                    'COLOR'      => '#a6ffa3',
                    'SIZE'       => '100%',
                    'POSITION'   => 'center',
                ],
                'ICON_DISABLED'     => [
                    'DATA_IMAGE' => 'data:image/svg+xml;charset=US-ASCII,%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20x%3D%220px%22%20y%3D%220px%22%0A%09%20viewBox%3D%220%200%2070%2071%22%20style%3D%22enable-background%3Anew%200%200%2070%2071%3B%22%20xml%3Aspace%3D%22preserve%22%3E%0A%3Cpath%20fill%3D%22%230C99BA%22%20class%3D%22st0%22%20d%3D%22M34.7%2C64c-11.6%2C0-22-7.1-26.3-17.8C4%2C35.4%2C6.4%2C23%2C14.5%2C14.7c8.1-8.2%2C20.4-10.7%2C31-6.2%0A%09c12.5%2C5.4%2C19.6%2C18.8%2C17%2C32.2C60%2C54%2C48.3%2C63.8%2C34.7%2C64L34.7%2C64z%20M27.8%2C29c0.8-0.9%2C0.8-2.3%2C0-3.2l-1-1.2h19.3c1-0.1%2C1.7-0.9%2C1.7-1.8%0A%09v-0.9c0-1-0.7-1.8-1.7-1.8H26.8l1.1-1.2c0.8-0.9%2C0.8-2.3%2C0-3.2c-0.4-0.4-0.9-0.7-1.5-0.7s-1.1%2C0.2-1.5%2C0.7l-4.6%2C5.1%0A%09c-0.8%2C0.9-0.8%2C2.3%2C0%2C3.2l4.6%2C5.1c0.4%2C0.4%2C0.9%2C0.7%2C1.5%2C0.7C26.9%2C29.6%2C27.4%2C29.4%2C27.8%2C29L27.8%2C29z%20M44%2C41c-0.5-0.6-1.3-0.8-2-0.6%0A%09c-0.7%2C0.2-1.3%2C0.9-1.5%2C1.6c-0.2%2C0.8%2C0%2C1.6%2C0.5%2C2.2l1%2C1.2H22.8c-1%2C0.1-1.7%2C0.9-1.7%2C1.8v0.9c0%2C1%2C0.7%2C1.8%2C1.7%2C1.8h19.3l-1%2C1.2%0A%09c-0.5%2C0.6-0.7%2C1.4-0.5%2C2.2c0.2%2C0.8%2C0.7%2C1.4%2C1.5%2C1.6c0.7%2C0.2%2C1.5%2C0%2C2-0.6l4.6-5.1c0.8-0.9%2C0.8-2.3%2C0-3.2L44%2C41z%20M23.5%2C32.8%0A%09c-1%2C0.1-1.7%2C0.9-1.7%2C1.8v0.9c0%2C1%2C0.7%2C1.8%2C1.7%2C1.8h23.4c1-0.1%2C1.7-0.9%2C1.7-1.8v-0.9c0-1-0.7-1.8-1.7-1.9L23.5%2C32.8L23.5%2C32.8z%22/%3E%0A%3C/svg%3E%0A',
                    'SIZE'       => '100%',
                    'POSITION'   => 'center',
                    'COLOR'      => '#ffb3a3',
                ],
                'PLACEMENT_HANDLER' => $this->app_source . '/handler.php',
            ];

            $this->call($method, $params);


            $this->call(
                'event.bind',
                [
                    'event'   => 'OnImConnectorMessageAdd',
                    'handler' => $this->app_source . '/handler.php',
                ]
            );

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function uninstall()
    {
        try {

            $method = 'event.unbind';
            $params = [
                'event'   => 'OnImConnectorMessageAdd',
                'handler' => $this->app_source . '/handler.php',
            ];

            $this->call($method, $params);


            $method = 'imconnector.unregister';
            $params = [
                'ID' => self::$connector_name
            ];

            $this->call($method, $params);

            return [];

        } catch (Exception $ex) {
            echo $ex->getMessage() . '<br/>';
            file_put_contents(__DIR__ . '/../logs/logs.log', $ex->getMessage() . PHP_EOL);
            return [];
        }
    }






    public function sendMessage2OL($chatID)

    {

        try {
            $line_id   = $this->getLine();
            $arMessage = [
                'user'    => [
                    'id'   => $chatID,
                    'name' => htmlspecialchars($_POST['name']),
                ],
                'message' => [
                    'id'   => false,
                    'date' => time(),
                    'text' => htmlspecialchars($_POST['message']),
                ],
                'chat'    => [
                    'id'  => $chatID,
                    'url' => htmlspecialchars($_SERVER['HTTP_REFERER']),
                ],
            ];

            $id              = self::saveMessage($chatID, $arMessage);
            $result['error'] = 'error_save';
            if ($id !== false) {
                $arMessage['message']['id'] = $id;
                $result                     = $this->call(
                    'imconnector.send.messages',
                    [
                        'CONNECTOR' => self::$connector_name,
                        'LINE'      => $line_id,
                        'MESSAGES'  => [$arMessage],
                    ]
                );
            }

            return $result;

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function activateConnector($options) //
    {
        $widgetUri  = '';//detail page chat "https://example.com/index.php"
        $widgetName = 'nbnabokovkz';//name connector in widget

        try {
            $method = 'imconnector.activate';
            $params = [
                'CONNECTOR' => self::$connector_name,
                'LINE'      => intVal($options['LINE']),
                'ACTIVE'    => intVal($options['ACTIVE_STATUS']),
            ];

            $result = $this->call($method, $params);

            var_dump($params);
            var_dump($result);

            if (!empty($result['result'])) {
                //add data widget
                if (!empty($widgetUri) && !empty($widgetName)) {
                    $resultWidgetData = CRest::call(
                        'imconnector.connector.data.set',
                        [
                            'CONNECTOR' => self::$connector_name,
                            'LINE'      => intVal($options['LINE']),
                            'DATA'      => [
                                'id'     => self::$connector_name . 'line' . intVal($options['LINE']),
                                'url_im' => $widgetUri,
                                'name'   => $widgetName
                            ],
                        ]
                    );
                    if (!empty($resultWidgetData['result'])) {
                        $this->setLine($options['LINE']);
                        return 'Установка с виджетом прошла успешно';
                    }
                } else {
                    $this->setLine($options['LINE']);
                    return 'Установка без виджета прошла успешно';
                }
            }

            return 'Установка не успешна';

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function setLine($line_id)
    {
        return file_put_contents(__DIR__ . '/../configs/line_id.txt', intVal($line_id));
    }

    private function getLine()
    {
        return file_get_contents(__DIR__ . '/../configs/line_id.txt');
    }

    public static function convertBB($var)
    {

        $search = array(
            '/\[b\](.*?)\[\/b\]/is',
            '/\[br\]/is',
            '/\[i\](.*?)\[\/i\]/is',
            '/\[u\](.*?)\[\/u\]/is',
            '/\[img\](.*?)\[\/img\]/is',
            '/\[url\](.*?)\[\/url\]/is',
            '/\[url\=(.*?)\](.*?)\[\/url\]/is'
        );

        $replace = array(
            '<strong>$1</strong>',
            '<br>',
            '<em>$1</em>',
            '<u>$1</u>',
            '<img src="$1" />',
            '<a href="$1">$1</a>',
            '<a href="$1">$2</a>'
        );

        $var = preg_replace($search, $replace, $var);

        return $var;
    }


    public static function getChat($chatID)
    {
        $result = [];
        if (file_exists(__DIR__ . '/../chats/' . $chatID . '.txt')) {
            $result = json_decode(file_get_contents(__DIR__ . '/../chats/' . $chatID . '.txt'), 1);
        }
        return $result;
    }




    public static function saveMessage($chatID, $arMessage)
    {
        file_put_contents(__DIR__ . '/../logs/arMessage.log', print_r($arMessage, true));
        $arMessages = self::getChat($chatID);
        $count = count($arMessages);
        $arMessages['message' . $count] = $arMessage;
        if (file_put_contents(__DIR__ . '/../chats/' . $chatID . '.txt', json_encode($arMessages, JSON_UNESCAPED_UNICODE))) {
            $return = $count;
        } else {
            $return = false;
        }
        return $return;
    }



    public function sendDelivery()
    {
        // Логирование содержимого $_REQUEST['data']['MESSAGES']
        file_put_contents(__DIR__ . '/../logs/messages.log', print_r($_REQUEST, true));

        foreach ($_REQUEST['data']['MESSAGES'] as $arMessage) {
            try {
                $idMess         = self::saveMessage($arMessage['chat']['id'], $arMessage);
                $resultDelivery = $this->call(
                    'imconnector.send.status.delivery',
                    [
                        'CONNECTOR' => self::$connector_name,
                        'LINE'      => self::getLine(),
                        'MESSAGES'  => [
                            [
                                'im'      => $arMessage['im'],
                                'message' => ['user_id' => [$idMess]],
                                'chat'    => ['id' => $arMessage['chat']['id']

                            ],
                            ],
                        ]
                    ]
                );
                file_put_contents(__DIR__ . '/../logs/resultDelivery.log', print_r($resultDelivery, true));
            } catch (Exception $ex) {
                file_put_contents(__DIR__ . '/../logs/delivery.log', $ex->getMessage());
            }
        }
    }
}