<?php

try {
            $this->bx24call('bizproc.activity.add', $params);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $message = ' Install error: ';
            $message .= $ex->getMessage() . PHP_EOL;
            $message .= 'TRACE: ' . $ex->getTraceAsString() . PHP_EOL;
            $this->log($message);
        }
    }

    public function uninstall()
    : void
    {
        try {
            $this->bx24call('bizproc.activity.delete', ['code' => self::ba_name]);
        } catch (Exception $ex) {
            return;
        }
    }

    private function runApp($id, array $p)
    : array
    {
        try {
            $ret = [
                'deals_ids' => [],
                'count' => 0
            ];

            if (empty($p['phone']) || empty($p['entity'])) {
                return $ret;
            }

            mb_internal_encoding("UTF-8");
            mb_regex_encoding("UTF-8");
            $params = [
                'entity_type' => $p['entity'],
                'type' => 'PHONE',
                'values' => [$p['phone']]
            ];
            $result = $this->bx24call('crm.duplicate.findbycomm', $params);

            $close_filter = ($p['is_check_closed'] === 'N') ? ['CLOSED' => 'N'] : [];
            $batch = [
                'halt' => 0,
                'cmd'  => []
            ];

            $entetyIdName = $p['entity'] . '_ID';
            foreach ($result['result'][$p['entity']] as $contact) {
                $batch['cmd'][$contact] = 'crm.deal.list?' . http_build_query([
                        'filter' => array_merge([$entetyIdName => $contact], $close_filter),
                        'select' => ['ID']
                ]);
            }
            $find = $this->bx24call('batch', $batch);
            foreach ($find['result']['result'] as $deals) {
                foreach ($deals as $deal) {
                    array_push($ret['deals_ids'], $deal['ID']);
                }
            }
            $ret['count'] = count($ret['deals_ids']);
            return $ret;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function bx24call($method, array $params)
    {
        try {
            usleep(500000);

            $curl = curl_init('https://' . $this->auth['domain'] . '/rest/' . $method . '.json');

            $params["auth"] = $this->auth["access_token"];

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));

            $resultRaw = curl_exec($curl);

            if (curl_errno($curl)) {
                throw new Exception('curl curl_errno ' . curl_errno($curl) . '. Ответ от curl:  ' . curl_error($curl));
            }

            $result = json_decode($resultRaw, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new Exception("curl json_error: " . $resultRaw);
            }

            curl_close($curl);

            if (!empty($result['error'])) {

                $messageArr = [];
                if (!empty($result['error'])) {
                    $messageArr[] = $result['error'];
                }
                if (!empty($result['error_description'])) {
                    $messageArr[] = $result['error_description'];
                }
                $message = implode(': ', $messageArr);
                throw new Exception($message);
            }

            return $result;

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function log($message, $data = null)
    {
        $objDateTime = new DateTime("now");

        $logfile = __DIR__ . '/logs/' . $this->auth['member_id'] . '_' . $objDateTime->format('Ymd') . '.log';
        $message = $objDateTime->format('c') . PHP_EOL . $message . PHP_EOL;
        if (!empty($data)) {
            $message .= print_r($data, true) . PHP_EOL;
        }
        file_put_contents($logfile, $message, FILE_APPEND);
    }

}










//////////////////////////////////

        if (!isset($_REQUEST['auth'])) {
            die();
        }  // может нас пытаются тупо взломать, не будем на них тратить логи

        $this->auth = $_REQUEST['auth'];


<= проверить, приходит ли в requet авторизация (есть ли auth при инсталяции)
<!-- если да ->bx24call -->



